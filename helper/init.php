<?php
error_reporting(E_ALL);
session_set_cookie_params(3600 * 24 * 365);
session_start();
require __DIR__ . '/../vendor/autoload.php';
include __DIR__ . '/../helper/config.php';
require __DIR__ . '/../helper/Practical.php';
include __DIR__ . '/../helper/DbConnection.php';

define("STYLE_VERSION","5");
use  GuzzleHttp\Client as GuzzClient;
$client = new GuzzClient(['base_uri' => BASE_API]);

?>