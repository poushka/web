<?php
namespace Src\helper ;

class  Practical {

   public  static function thousandsCurrencyFormat($num) {

        if($num>1000) {
            $x = round($num);
            $x_number_format = number_format($x);
            $x_array = explode(',', $x_number_format);
            $x_parts = array('k', 'm', 'b', 't');
            $x_count_parts = count($x_array) - 1;
            $x_display = $x;
            $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
            $x_display .= $x_parts[$x_count_parts - 1];

            return $x_display;

        }

        return $num;
    }

      public static function setCookies($social_position,$socials) {
          setcookie(SOCIAL_ID_SOCIAL, $socials[$social_position][SOCIAL_ID_SOCIAL],2147483647);
          setcookie(P_NAME_SOCIAL, $socials[$social_position][P_NAME_SOCIAL],2147483647);
          setcookie(URI_SOCIAL, $socials[$social_position][URI_SOCIAL],2147483647);
          setcookie(ICON_SOCIAL, $socials[$social_position][ICON_SOCIAL],2147483647);
          setcookie(PREFIX_SOCIAL, $socials[$social_position][PREFIX_SOCIAL],2147483647);
          setcookie(MEMBER_PREFIX_SOCIAL, $socials[$social_position][MEMBER_PREFIX_SOCIAL],2147483647);
      }
      public static function currentUrl($server){
        //Figure out whether we are using http or https.
        $http = 'http';
        //If HTTPS is present in our $_SERVER array, the URL should
        //start with https:// instead of http://
        if(isset($server['HTTPS'])){
            $http = 'https';
        }
        //Get the HTTP_HOST.
        $host = $server['HTTP_HOST'];
        //Get the REQUEST_URI. i.e. The Uniform Resource Identifier.
        $requestUri = $server['REQUEST_URI'];
        //Finally, construct the full URL.
        //Use the function htmlentities to prevent XSS attacks.
        return $http . '://' . htmlentities($host) . '/' . htmlentities($requestUri);
    }

}


?>