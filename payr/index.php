<?php
error_reporting(E_ALL);

session_set_cookie_params(3600 * 24 * 365);
session_start();

//session_destroy();

require '../vendor/autoload.php';
include '../helper/config.php';
include '../helper/MCrypt.php';



/*if (!isset($_GET["message"]) || !isset($_GET["titile"]) || !isset($_GET["aow"]) || !isset($_GET["condition"])) {
    header('Location:'.BASE_URL);
}*/
$crypto = new \Src\Inc\MCrypt();

$result = $crypto->decrypt2($_GET["data"]);

$result=json_decode($result,true);


$message = $result["message"];
$title = $result["title"];
$state=$result["state"];
$aow = $result["aow"];


if ($state==1) {
   $pay_border = "success-pay-shadow";
   $icon = "text-success text-center fa fa-check-circle fa-4x";
   $message_text_color = "text-success";


}else {
    $pay_border= "failure-pay-shadow";
    $icon =  "text-danger text-center fas fa-exclamation-triangle fa-4x";
    $message_text_color = "text-danger";
}

if ($aow=="web") {

    $back_url = BASE_URL."dashbord";

    $back = " <a class=\"align-self-center d-flex align-items-center back-pay mt-3\" href=\"$back_url\"> بازگشت به حساب کاربری
             <i class=\"fa fa-arrow-alt-circle-left mr-2\">
             </i>
         </a>";
} else {
    $back_url = "intent:#Intent;scheme=pooshka;package=com.developer.hrg.socialbase;end";
    $back = " <a class=\"align-self-center d-flex align-items-center back-pay mt-3\" href=\"$back_url\"> بازگشت به اپلیکیشن
             <i class=\"fa fa-arrow-alt-circle-left mr-2\">
             </i>
         </a>";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <base href=<?php echo BASE_URL?>>
    <title><?php echo $title?></title>

    <meta charset="utf-8">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!--    <link rel="stylesheet" href="css/bootstrap.css">-->

    <link rel="stylesheet" href="awsome/css/all.min.css">

    <link rel="stylesheet" href="css/style.css">



</head>
<body>

     <div class="container rtl">
     <div class="mt-3 mt-md-4 mt-lg-5 d-flex flex-column min-height-70 justify-content-center <?php echo $pay_border?>"
         >
         <img class="align-self-center" src="assets/poushka.jpg" width="80" height="80">
         <h1 class="myh1  text-center mt-2 mb-4">پوشکا مرجع شبکه های اجتماعی</h1>
         <h2 class="text-center myh1 pay-color mt-2"><?php echo $title ?> </h2>
         <hr class="my_background w-75 "/>
         <i class="<?php echo $icon?>"></i>
         <p class="text-center mt-2 <?php echo $message_text_color?> text-bold"><?php echo $message?></p>
         <?php
         echo $back;
         ?>

     </div>

 </div>


<?php include '../footer.php'?>


</body>
</html>

