<?php
//todo search
//todo footer !!!!
//todo list pagination redesign
use Src\helper\Practical as MyPractical;
include '../helper/init.php';

$social_id = 1;
$cat_id = 0;
$main = 1;
$social_position = 0;
$current_social = null;
$response = $client->request('GET', 'getSC');
$socials_categories = json_decode($response->getBody(), true);
$socials = $socials_categories["socials"];
$categories= $socials_categories["categories"];
$new_page_modal = false ;  // it define that modal is opening from new page or user panel
$category_en = null;
$category_fa = null;

if (isset($_GET["social"]) & isset($_GET["category"])) {
    $social = $_GET["social"];
    $category_en = $_GET["category"];
    $stmt = $conn->prepare("select social_id from sociall where e_name like ?");
    $stmt->bind_param("s", $social);
    $stmt->execute();
    $result_social = $stmt->get_result();
    if ($result_social->num_rows ==0) {
        header('Location:'.BASE_URL."404");
    }
    $social_db = $result_social->fetch_assoc();
    $social_id = $social_db["social_id"];

    $stmt = $conn->prepare("select cat_id,name from category where en_name like ?");
    $stmt->bind_param("s", $category_en);
    $stmt->execute();
    $result_cat = $stmt->get_result();
    if ($result_cat->num_rows ==0) {
        header('Location:'.BASE_URL."404");
    }

    $cat_db = $result_cat->fetch_assoc();
    $cat_id = $cat_db["cat_id"];
    $category_fa=$cat_db["name"];

    $main = 0;

    for ($i = 0; $i < count($socials); $i++) {
        if ($social_id == $socials[$i][SOCIAL_ID_SOCIAL]) {
            $social_position = $i;
            setcookie(SOCIAL_POSITION, $social_position, 2147483647, "/");
        }
    }
    $current_social = $socials[$social_position];

} else if (isset($_GET["social"])) {

    $social = $_GET["social"];
    $stmt = $conn->prepare("select social_id from sociall where e_name like ?");
    $stmt->bind_param("s", $social);
    $stmt->execute();
    $result_social = $stmt->get_result();
    if ($result_social->num_rows==0) {
        header('Location:'.BASE_URL."404");
    }
    $social_db = $result_social->fetch_assoc();
    $social_id = $social_db["social_id"];

    for ($i = 0; $i < count($socials); $i++) {
        if ($social_id == $socials[$i][SOCIAL_ID_SOCIAL]) {
            $social_position = $i;
            setcookie(SOCIAL_POSITION, $social_position, 2147483647, "/");
        }
    }
    $current_social = $socials[$social_position];

} else {
    header('Location:'.BASE_URL."404");
}


$body = 1;


$sections_request = $client->request('GET', "PSections/" . $current_social[SOCIAL_ID_SOCIAL], [
    'query' => ["main" => $main, 'cat_id' => $cat_id, 'prefix' => $current_social[PREFIX_SOCIAL]
    ]
], $body);
$sections = json_decode($sections_request->getBody(), true);




$og_image=  BASE_URL."assets/site.jpg";


if ($main==1) {

    $og_url =   BASE_URL."m/".$social;

    $title =  "معرفی ". $current_social["prefix"] . " " . $current_social[P_NAME_SOCIAL] . " - " . "پوشکا";
    $header = "معرفی " .  $current_social["prefix_p"] . " " . $current_social[P_NAME_SOCIAL] . " - صفحه اصلی" ;
    $description=  "معرفی " . $current_social["prefix"] ." " . $current_social[P_NAME_SOCIAL] . "، مرجع "
                         .  $current_social["prefix_p"] ." " . $current_social[P_NAME_SOCIAL] . " پر بازدیدترین و پرمخاطب ترین " . $current_social["prefix_p"] . " " . $current_social[P_NAME_SOCIAL]
        . " را در پوشکا مشاهده و جستجو نمایید";

     $keywords = " معرفی " .$current_social["prefix"] . " " . $current_social[P_NAME_SOCIAL];

}else {
    $og_url =   BASE_URL."m/".$social."/".$category_en;
    $header = "معرفی ". $current_social["prefix_p"] . " " . $category_fa . " ". $current_social[P_NAME_SOCIAL ] ;
    $title =  $current_social["prefix"] . " " . $category_fa . " ". $current_social[P_NAME_SOCIAL ] ." - " . "پوشکا";

    $description=  "معرفی " . $current_social["prefix"] ." ".$category_fa. " "  . $current_social[P_NAME_SOCIAL] . "، مرجع "
        .  $current_social["prefix"] ." " . $category_fa . " "  . $current_social[P_NAME_SOCIAL] . " پربازدید ترین و پر مخاطب ترین " . $current_social["prefix_p"] . " ".$category_fa. " "  . $current_social[P_NAME_SOCIAL]
        . " را در پوشکا مشاهده نمایید";
    $keywords = "معرفی ". $current_social["prefix"] . " " . $category_fa . " ". $current_social[P_NAME_SOCIAL ];


}

include 'header.php';



?>




<div class="container min-height-70 rtl"> <!--banners-->

    <div class="d-flex flex-column pr-xl-5 pr-md-2 ">


    <div class="d-flex mt-3 rtl socials-top  ">

        <?php

        $current_href = $category_fa==null ? "m/".$current_social["e_name"] : "m/".$current_social["e_name"]."/".$category_en;
        $current_text = $category_fa==null ? " /  اصلی" :" / $category_fa";
        $current_imgg = IMG_URL . 'social_cat/' . $current_social[ICON_SOCIAL];

        ?>

        <a href="<?php echo $current_href?>">
        <div class="d-flex font-size-13 social-active ">
        <div class='d-flex social_icon align-items-center justify-content-center' >
            <img src=<?php echo $current_imgg?> width='16px' height='16px'>
        </div>
           <span class="align-self-center mr-1 ml-1">
               <?php echo $current_social[P_NAME_SOCIAL] . $current_text
               ?>
           </span>
        </div>
        </a>

        <?php
         foreach ($socials as $social) {
             if ($social[SOCIAL_ID_SOCIAL]!=$current_social[SOCIAL_ID_SOCIAL]) {
                 $current_text = $category_fa==null ? " / اصلی" :" / $category_fa";
                 $social_href = $category_fa==null ? "m/".$social["e_name"] : "m/".$social["e_name"]."/".$category_en;
                 $img_src= IMG_URL . 'social_cat/' . $social[ICON_SOCIAL];
                 echo "
                   <a  href='$social_href'>
                   <div class='d-flex  social-inactive mr-1 font-size-13'>
                   <div class='d-flex social_icon align-items-center justify-content-center' '>
                   <img src=$img_src width='16px' height='16px'>
                   </div>
                   <span class='align-self-center mr-2 ml-2'>$social[p_name] $current_text</span>
                   </div> 
                   </a> 
                   ";
             }
         }
      ?>
<!--        echo "<a href='$social_href' class='social-inactive'>$social[p_name] $current_text</a>";-->
    </div>

    <div class="d-flex justify-content-start mt-2 ">
        <h1 class="main-h1 mt-3 text-right"><?php echo $header ?></h1>
    </div>

    </div>


    <div id="banner-owl" class="owl-carousel mt-3 owl-theme p-0">

        <?php

        $banners = $sections[0]["pages"];


        foreach ($banners as $banner) {


            $banner_pic = IMG_URL . "banner/" . $banner["banner"];
            $banner_name = htmlspecialchars($banner["name"],ENT_QUOTES,"UTF-8");
            $short_des = htmlspecialchars($banner["short_des"],ENT_QUOTES,"UTF-8");
         //   $banner_alt = $_COOKIE[PREFIX_SOCIAL] . " " . $_COOKIE[P_NAME_SOCIAL] . " " . $banner_name;
            $banner_alt = $current_social[PREFIX_SOCIAL] . " " . $current_social[P_NAME_SOCIAL] . " " . $banner_name;

            $social_address = BASE_URL . "social/" . $current_social[E_NAME_SOCIAL] . "/" . $banner["id"];

            echo "
               <div class=\"item_banner mr-xl-5 ml-xl-5 pr-1 pl-1\">
               <a title='$banner_alt' href=$social_address>
               <img src=\"$banner_pic\" class='fit-cover'  alt=\"$banner_alt\">
               <span class='text-center d-block mt-3 '> $short_des </span>
           </a> 
           </div>
            ";
        }

        ?>


    </div>



<?php
foreach ($sections as $section) {
     $is_special = false ;

    $title = $section["title"];

     if ($section["key"]=="specials") {
         $is_special=true;

     }

    if ($title != "بنرها") {
        if ($main==1) {
            $list_url = BASE_URL ."all/". $current_social[E_NAME_SOCIAL] ."/".$section["key"];
        }else {
            $list_url =  BASE_URL ."all/".   $current_social[E_NAME_SOCIAL] ."/". $category_en  ."/" .$section["key"];
        }


        echo "<main class=\"grid-item container main mt-4 \"  >
    <div class=\"d-flex list-top justify-content-between align-items-center rtl\" >
        <h2 class=\"mr-2  mt-2 text-right section_title\">$title</h2>";

            if (!$is_special) {
                echo "<a title='$title' class='ml-1 ml-md-2 d-flex align-items-center section_all'  
                href=$list_url> همه <i class='fa fa-angle-left'></i>  </a>
               
               ";
            }
           echo"
    </div>";

       if (count($section["pages"])>0) {
           echo "           
    <div  class=\" mt-2 text-right owl-carousel pt-1 myowl\"> ";
           $pages = $section["pages"];
           foreach ($pages as $single_page) {
               $name = $single_page["prefix"] . " " . $single_page["p_name"]. " " . htmlspecialchars($single_page["name"],ENT_QUOTES,"UTF-8");
               $member = MyPractical::thousandsCurrencyFormat($single_page["member"]);
               $member_prefix = $single_page["member_prefix"];
               $page_icon = IMG_URL . "chanel_pic/" . $single_page["thumb"];
               // $page_alt = $_COOKIE[PREFIX_SOCIAL] . " " . $_COOKIE[P_NAME_SOCIAL] . " " . $single_page["name"];
               $page_alt =  $name;
               $social_address = BASE_URL . "social/" . $current_social[E_NAME_SOCIAL] . "/" . htmlspecialchars($single_page["id"],ENT_QUOTES,"UTF-8");
               $short_des=htmlspecialchars($single_page["short_des"],ENT_QUOTES,"UTF-8");

               echo "
        <div  class=\"item \">
                <a title='$page_alt' href=$social_address>
               <img  src=$page_icon class=\"item-img fit-cover\" alt=\"$page_alt\">
               </a> " ;

               if ($is_special) {
                   echo '<img src="assets/special.png" alt="" class="special_img">';
               };
               echo "  
               <div class='d-flex flex-column'>
               <a title='$page_alt' href=$social_address 
               class=\"item-title mt-1\" >$name</a>
               <span >$short_des</span>
               <p  class=\"item-member align-self-end mb-0\">$member <span class='small-font text-light-font'>$member_prefix</span></p>
              </div>
              
           </div> <!--end of item-->
          
        ";
           }
           echo "
            </div> ";
       }else {

           echo "
           <div class='d-flex flex-column text-regular align-items-center  justify-content-center mt-1 no-item-color pt-5 pb-5'>
            <i class='fal fa-empty-set fa-3x'></i>
            <span>هیچ آیتمی دراین بخش یافت نشده</span>
           </div>
           ";
       }


          echo "
              <hr class='hr-bg'>
              </main>
             ";
    }


}
?>
</div> <!--banners ends-->


<?php
include '../footer.php'
?>
</body>


<script src="js/owl.carousel.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<!--<script src="js/bootstrap.min.js"></script>-->
<script src="js/script.js"></script>
<script src="js/register.js"></script>

<script>

    $('#home').click(function () {
        var search = $('#et_search').val();
        window.location = web_url+"search/"+current_social_script+"/"+search;
    });

    $(document).ready(function () {
        $("#banner-owl").owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 300,
            paginationSpeed: 400,
            rtl: true,
            singleItem: true,
            items: 1,
            autoplay: 4000,
            autoPlay: 4000,
            loop: true,

        });

        $(".myowl").owlCarousel({

            loop: false,
            rtl: true,
            /* margin: 0,*/
            nav: true,
            navText: [
                '<div class="d-flex align-items-center justify-content-center navigation"> <i class="fa fa-angle-right " ></i></div>',
                '<div class="d-flex align-items-center justify-content-center navigation"> <i class="fa fa-angle-left " ></i></div>'
            ],

            dots: false,
            margin: 10,
            stagePadding: 10,
            autoPlay: 1000,
            responsive: {
                0: {
                    items: 2
                },
                450: {
                    items: 3
                },

                768: {
                    items: 4
                },
                992: {
                    items: 5
                },

                1000: {
                    items: 6
                }

            }
        });

    });




</script>

</html>