<!DOCTYPE html>
<html lang="fa_IR">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href=<?php echo BASE_URL?>>
    <meta charset="utf-8">

    <title><?php echo $title?></title>

    <meta http-equiv="content-language" content="fa">
    <meta property="og:site_name" content="پوشکا - مرجع تبلیغات شبکه های اجتماعی">
    <meta property="og:title" content="<?php echo $title ?>">
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?php echo $og_url ?>">
    <meta property="og:image" content="<?php echo $og_image ?>">
    <meta property="og:description" content="<?php echo $description?>">
    <meta  name="og:locale" content="fa_IR">
    <meta name="description" content="<?php echo $description ?>">
    <meta name="keywords" content="<?php echo $keywords?>">



    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="<?php echo $title?>">
    <meta name="twitter:description" content="<?php echo $description?>">
    <meta name="twitter:site" content="@PoushkaSite">
    <meta name="twitter:image" content="<?php echo BASE_URL.'assets/site.jpg'?>">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!--    <link rel="stylesheet" href="css/bootstrap.min.css">-->

    <link rel="canonical" href="<?php echo $og_url?>" />
    <link rel="stylesheet" href="awsome/css/all.min.css">


<!--    <link rel="stylesheet" href="css/owl.carousel.css">-->
<!---->
<!--       <link rel="stylesheet" href="css/owl.theme.default.css">-->
<!---->
<!--    <link rel="stylesheet" href="css/owl.theme.default.css">-->


    <link rel="stylesheet" href="css/style.css?<?php echo "version=".STYLE_VERSION?>">

<!--    <link rel="stylesheet" href="css/simple-lightbox.min.css">-->

    <link rel="icon" href="favicon.ico">

<!--    <script-->
<!--            src="https://code.jquery.com/jquery-3.4.1.min.js"-->
<!--            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="-->
<!--            crossorigin="anonymous"></script>-->

    <script src="js/jquery.min.js"></script>

    <script src="js/config.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


    <script>
        var is_open_from_panel  = false;
        var current_social_script= "<?php echo $current_social[E_NAME_SOCIAL]?>"; // to pass to register script
        //for using the current social when new page is open
    </script>

</head>
 <body>

<nav class="navbar sticky-top navbar-expand-lg navbar-light bg-light pr-1 pl-1 pr-md-2 pl-md-2">
        <a href="<?php echo BASE_URL.'new/'.$current_social[E_NAME_SOCIAL] ?>" id="new_page"
           class="align-items-center alertPulse-css bg_new_page brand-background ml-1 ml-md-2 d-flex">
            <?php echo "ثبت  " . $current_social[PREFIX_SOCIAL] . " ". $current_social[P_NAME_SOCIAL] ?>
        </a>
    <div class="d-flex flex-row  order-lg-3">
        <div class="d-flex align-items-center  mr-1">
            <a href="<?php echo BASE_URL?>" class="text-bold brand-color text-bold brand-textsize" >
                <img src="assets/typo.png">
            </a>
        </div>

        <button class="navbar-toggler  navbar-toggler-right " type="button"
                data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <sapn class="navbar-toggler-icon"></sapn>
        </button>
    </div>

        <div class="collapse navbar-collapse rtl" id="navbarSupportedContent">

            <form class="d-flex   my-2 mr-lg-2 ">
                <input id="et_search" class="form-control col-md-5 mr-sm-1 search_input small-font" type="search"
                       placeholder= "<?php echo   "جستوجو در " .$current_social["prefix_p"] . " " . $current_social["p_name"] ." ..."?>" aria-label="Search">
                <button id="btn_search" class="btn btn-outline-success small-font my-sm-0  " >جستوجو</button>
            </form>



            <ul class="navbar-nav pr-1 mr-lg-1 mr-md-1 text-right my-2 my-lg-0 ">
                <li class="nav-item">
                    <a id="home" class="nav-link" href=<?php echo BASE_URL_M . $current_social[E_NAME_SOCIAL] ?>> <i
                                class="fa fa-home"></i> صفحه اصلی <span
                                class="sr-only">(current)</span></a>
                </li>



                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop-socials" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-chart-network"></i>       شبکه ها
                    </a>

                    <div class="dropdown-menu">

                        <?php
                        foreach ($socials as $single_social) {
                           $active_dropdown="";
                           $social_alt = "مرجع " .  $single_social["prefix_p"] . " ".$single_social["p_name"];

                           if ($single_social[SOCIAL_ID_SOCIAL] == $current_social[SOCIAL_ID_SOCIAL]) {
                               $active_dropdown="drop-down-active";
                           }
                                $icon = IMG_URL . 'social_cat/' . $single_social[ICON_SOCIAL];
                                $pname = $single_social[P_NAME_SOCIAL];
                                $social_address = null;
                                $social_address = BASE_URL_M . $single_social[E_NAME_SOCIAL];

                                echo "
                             <a title='$social_alt' class=\"dropdown-item mt-1 mb-1 nav-link $active_dropdown text-right \"  href=$social_address>
                             <img alt='$social_alt' src=\"$icon\" class=\"mr-2\" width=\"25px\" height=\"25px\">
                                 $pname
                                </a>";

                        }
                        ?>

                    </div>

                </li>

                <li class="nav-item dropdown" >
                    <a class="nav-link dropdown-toggle"  href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-list ml-1"></i>
                        دسته بندی ها
                    </a>
                    <div class="dropdown-menu dropdown-menu-category">
                        <div class="container p-0 mt-1 mb-1">
                            <div class="pl-2 pr-2 w-100">
                                <input id="in-search-cat" class="form-control w-100" type="text" autocapitalize="off"
                                       autocomplete="off"
                                       spellcheck="false"
                                       autocorrect="off"  placeholder="جستجو در دسته ها...">
                            </div>
                       <div class="row m-0 ">

                        <?php
                        foreach ($categories as $category) {
                            $active_dropdown="";
                            if ($category_en == $category["en_name"]) {
                                $active_dropdown="drop-down-active";
                            }
                            $src = IMG_URL."cat/".$category["icon"];
                            $cat_address =BASE_URL_M . $current_social[E_NAME_SOCIAL] . "/" . $category["en_name"];
                            $alt_category= $current_social["prefix_p"]. " " .$current_social["p_name"]. " ".$category["name"];

                            echo "
                          
                              

                                <div class='col-6 col-lg-4 mt-3 category nc' data-id='$cat_id' >
                              
                              <a href='$cat_address' type='$alt_category' data-name='$category[name]' >
                              <div class='d-flex align-items-center'>
                             
                             <img alt='$alt_category' src='$src' width='45px' height='45px'>
               
                         <span  class='text-right mr-1 category_selected $active_dropdown'>$category[name]</span>
                     </div>
                     
                     </a>   
                     
                               </div>
              
                             
                                ";
                        }
                        ?>
                       </div>
                        </div>
                    </div>
                </li>

                <li class="nav-item ">

                <?php
                  if (isset($_SESSION[LOGIN]) && $_SESSION[LOGIN]===true) {
                        ?>
                        <a rel="nofollow" class="nav-link" href="<?php echo BASE_URL.'dashbord' ?>" >
                            <i class="fa fa-user  ml-1"></i> حساب کاربری </a>
                        <?php
                   }else { ?>
                        <a rel="nofollow"  class="nav-link" id="user_panel" href="#login_modal" data-toggle="modal" >
                            <i class="fa fa-user  ml-1"></i> ورود/ثبت نام </a>
                        <?php
                    }
                    ?>


              <!--    <a class="nav-link " href="#"> <i class="lofa fa-user ml-1"></i> حساب کاربری</a>-->
                </li>
            </ul>
        </div>


</nav>  <!--nav bar-->



<div class="modal fade" id="login_modal" >
    <div class="modal-dialog modal-dialog-centered " >
        <div class="modal-content modal-content-radius">

            <div class="modal-header login_header d-flex align-items-center  position-relative " >

                    <button id="close_dialog" type="button" class="myclose mt-1 mr-sm-1"  data-dismiss="modal"> x </button>
                    <h5 class="myh4 text-medium text-center w-100 mr-3 mt-2" id="tv_login_header">ورود به حساب کاربری</h5>
            </div>

            <!-- Modal body -->
            <div class="modal-body login_register">
                <div class="d-flex flex-column login_main">

                    <span id="lbl_should_login" class="text-center d-none my-black-color mb-3">برای ثبت باید وارد حساب کاربری شوید </span>

                <div class="d-flex align-items-center rtl form-group mr-2 ml-2 mr-lg-5 ml-lg-5">
                    <i class="fa fa-phone text-gray "></i>
                    <input class="form-control number_input mr-2 w-100" type="number" oninput="if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"    maxlength="11" placeholder="شماره موبایل" id="et_login_mobile"
                          >

                </div>

                <div class="d-flex align-items-center rtl mt-3 mr-3 ml-3 mr-lg-5 ml-lg-5">
                    <i class="fa fa-lock text-gray "></i>
                    <input type="password" placeholder="رمز عبور" id="et_login_password" class="form-control  mr-2">

                </div>

                <p class="mt-3 ml-2 text-bold text-blue pointer" id="forget_div">فراموشی رمز عبور؟</p>


                <p class="text-danger text-center mt-2" id="login_error"> پیام </p>

                <button type="submit" class="btn_login d-flex align-items-center justify-content-center mt-3 mr-2 ml-2 mr-lg-5 ml-lg-5" id="btn_login">
                    <span class="spinner-border spinner-border-sm mr-2" id="loading_login"></span>
                    ورود
                </button>
                </div> <!--login_modal-->

                <div class="d-none flex-column register_main" >
                     <p class="text-center" id="register_message"> شمار خود را برای دریافت پیامک ورود وارد نمایید</p>
                    <div class="d-flex align-items-center rtl form-group mr-3 ml-3 mr-lg-5 ml-lg-5" id="register_mobile_div">
                        <i class="fa fa-phone text-gray " id="et_register_icon"></i>
                        <input type="tel" maxlength="11" placeholder=" شماره موبایل" id="et_register_mobile" class="form-control  mr-2">
                    </div>

                  <div class="flex-column d-none " id="register_final_div">

                    <div class="d-flex align-items-center rtl mt-1 mr-3 ml-3 mr-lg-5 ml-lg-5">
                        <i class="fa fa-lock text-gray "></i>
                        <input type="password" placeholder="رمز عبور " id="et_register_password" class="form-control  mr-2">
                    </div>

                    <div class="d-flex align-items-center rtl mt-3 mr-3 ml-3 mr-lg-5 ml-lg-5">
                        <i class="fa fa-lock text-gray "></i>
                        <input type="password" placeholder="تکرار رمز عبور" id="et_re_register_password" class="form-control  mr-2">
                    </div>

                    <div class="d-flex align-items-center rtl mt-3 mr-3 ml-3 mr-lg-5 ml-lg-5">
                        <i class="fa fa-user text-gray "></i>
                        <input type="password" placeholder="کد معرف(اختیاری)" id="et_moaref_password" class="form-control  mr-2">
                    </div>

                  </div>


                    <p class="text-danger  text-center mt-2" id="register_error"> پیام </p>

                    <button type="submit" class="btn_login mr-lg-5 ml-lg-5 mr-2 ml-2 d-flex align-items-center mt-2 justify-content-center" id="btn_register">
                        <span class="spinner-border spinner-border-sm mr-2" id="loading_register"></span>
                        <i  class="fa fa-arrow-circle-left mr-2" id="icon_register"></i>
                        مرحله بعد
                    </button>
                </div> <!--register modal -->

                <div class="d-none flex-column forget_main" >
                    <p class="text-center" id="message_forget" >شماره خود را برای بازیابی رمز عبور وارد نمایید</p>

                    <div class="d-flex align-items-center rtl form-group mr-3 ml-3 mr-lg-5 ml-lg-5" id="div_top_forget">
                        <i class="fa  fa-phone text-gray " id="icon_top_forget"></i>
                        <input type="tel" maxlength="11" placeholder=" شماره موبایل" id="et_top_forget" class="form-control  mr-2">
                    </div>


                    <div class="d-none align-items-center rtl form-group mr-3 ml-3 mr-lg-5 ml-lg-5" id="div_bottom_forget">
                        <i class="fa fa-lock text-gray "></i>
                        <input type="password"  placeholder="تکرار رمز عبور" id="et_bottom_forget" class="form-control  mr-2">
                    </div>

                    <p class="text-danger d-none text-center mt-2" id="forget_error"> پیام </p>

                    <button type="submit" class="btn_login mr-lg-5 ml-lg-5 mr-2 ml-2 d-flex align-items-center mt-2 justify-content-center" id="btn_forget">
                        <span class="spinner-border spinner-border-sm mr-2" id="loading_forget"></span>
                        <i  class="fa fa-redo mr-2" id="icon_forget"></i>
                        دریافت کد بازیابی
                    </button>
                </div> <!--forget modal -->


            </div>
            <!-- Modal footer -->
            <div class="modal-footer login_footer d-flex flex-row-reverse justify-content-center">
                <span class="ml-1 mt-3 mb-3" id="lbl_login_register"> حساب کاربری ندارید؟ </span>
                <span class="ml-2 mb-3 mt-3 text-bold" id="tv_register"> ثبت نام </span>

            </div>

        </div>
    </div>


</div> <!--modal_categories-->


<script>
$('#btn_search').click(function () {
    var search = $('#et_search').val();
    if (search.toString().length > 0 ) {
        var location = web_url+"search/"+current_social_script+"/"+search;
        window.location.href=location;
        return false;
    } else {
        return true;
    }

})
    $('#in-search-cat').keyup(function () {
        var value = $(this).val();
        $('.nc > a').each(function () {
            if ($(this).attr('data-name').search(value) > -1) {
                $(this).parent().show();
            } else {
                $(this).parent().hide();
            }
        })
    }).blur(function () {
        setTimeout(function () {
            $('#in-search-cat').val('')
            $('.nc').each(function () {
                $(this).show()
            })
        },500)

    })



</script>