<!--footer strat-->
<div class="container-fluid footer mt-2">

    <div class="container footer-container">
        <div class="row text-right rtl ">
            <div class="col-lg-3  col-sm-6 info-footer">
                <p class="text-medium mt-1">درباه ما </p>

                <p class="semi-black-color text-justify font-size-13">
                    پوشکا به عنوان مرجعی برای ثبت و معرفی صفحات و کانال های پیام رسان ها و شبکه های اجتماعی سعی بر این دارد تا بستری شفاف و
                    مستقیم برای نمایش صفحه و کانال شما فراهم نمایید . در پوشکا کاربران قادر به ثبت امتیاز و نظر برای شما میباشند . پس نهایت تلاش خود را برای  تولید محتوای با کیفیت و ارائه خدمات مطلوب به کاربران خود به کار ببندید
                </p>

            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 pr-md-5 info-footer">
                <P class="text-medium">دسترسی سریع</P>


                <ul class="pr-0 mt-2 semi-black-color font-size-13 footer-ul">
                    <li> <a title="صفحه اصلی"  href="<?php echo BASE_URL?>">صفحه اصلی</a></li>
                    <li>        <?php
                        if (isset($_SESSION[LOGIN]) && $_SESSION[LOGIN]===true) {
                            ?>
                            <a rel="nofollow"  href="<?php echo BASE_URL.'dashbord' ?>" >
                                حساب کاربری </a>
                            <?php
                        }else { ?>
                            <a rel="nofollow"  id="user_panel_footer" href="#login_modal" data-toggle="modal" >
                                ورود / ثبت نام</a>
                            <?php
                        }
                        ?>
                    </li>
                    <li> <a title="قوانین ثبت"  href="<?php echo BASE_URL.'social-rules' ?>">قوانین ثبت صفحه و کانال</a></li>
                    <li> <a title="ثبت صفحه و کانال در پوشکا"  href="<?php echo BASE_URL.'new/Instagram'?>"> ثبت صفحه یا کانال</a></li>
                    <li> <a title="سوالات متداول"  href="<?php echo BASE_URL.'faq'?>">سوالات متداول</a></li>
                    <li> <a title="تماس"  href="<?php echo BASE_URL.'support'?>"> تماس با ما</a></li>
<!--                    <li><a href="#">درخواست پشتیبانی</a></li>-->
<!--                    <li><a href="#">گزارش تخلف</a></li>-->
                </ul>

            </div>

            <div class="col-lg-3 col-md-6  d-flex flex-column align-items-start ">
                <p class="text-medium">دانلود اپلیکیشن</p>

                <button class="btn green_download text-white min-width-download py-2 my-2" type="button"><i class="fab fa-android"></i> دانلود مستقیم

                </button>
                <button class="btn btn-dark min-width-download py-2" type="button"><i class="fa fa-play"></i>  google play
                </button>

            </div>



            <div class="col-lg-3  col-md-6 col-sm-6">
                <p class="text-medium">ضمانت پرداخت</p>
                <img style="cursor:pointer" onclick="window.open('https://api.nextpay.org/trust/14159',
                 'Popup','location=no,toolbar=no, statusbar=no, scrollbars=yes,menubar=no, resizable=0,width=700,height=800,top=30')"
                     alt="nextpay_trust_logo" height="120" width="120" src="https://nextpay.ir/trust_seal.png">
            </div>




        </div>

    </div>
</div>


<!--footer_end-->