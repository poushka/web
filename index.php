<?php
//todo footer !!!!
//todo fix make page


include 'helper/init.php';

use  GuzzleHttp\Client as GuzzClient;


$client = new GuzzClient(['base_uri' => BASE_API]);

$response = $client->request('GET', 'getSocialA');



$socials = json_decode($response->getBody(), true);

$current_social = $socials[0];

$all_socials="";
for ($i = 0; $i<count($socials);$i++) {
    if ($i+1<count($socials)) {
        $all_socials.=$socials[$i]["p_name"]."، ";
    }else {
        $all_socials.=$socials[$i]["p_name"];
    }

}

$description = "پوشکا بزرگترین مرجع تبلیغات شبکه های اجتماعی میباشد . با ثبت  رایگان صفحه و کانال خود در پوشکا مخاطبین و کاربران صفحه و کانال خود را افزایش داده در موتورهای جستجو دیده شوید";

$description_body = "پوشکا مرجع کاملی از کانال ها و صفحات " .$all_socials." بر اساس تعداد عضو و دنبال کننده میباشد در پوشکا شما قادرید به راحتی صفحه و کانال مورد نظر را با استفاده از امکانات متنوع پوشکا پیدا کنید و با مشاهده تعداد عضو، توضیحات، امتیاز، تصاویر، نظرات و ویدیو از کیفیت محتوای صفحه یا کانال اطمینان حاصل پیدا نمایید ";
$keywords="تبلیغات صفحه, تبلیغات کانال, افزایش بازدید صفحه,افزایش بازدید کانال"



?>
<!DOCTYPE html>
<html lang="fa-IR">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <base href=<?php echo BASE_URL ?>>
    <meta charset="utf-8">
    <title>پوشکا مرجع معرفی و ثبت صفحه و کانال های شبکه های اجتماعی</title>


    <meta http-equiv="content-language" content="fa">
    <meta property="og:site_name" content="پوشکا - مرجع شبکه های اجتماعی">
    <meta property="og:title" content="پوشکا مرجع شبکه های اجتماعی">
    <meta property="og:type" content="website">
    <meta property="og:image" content="<?php echo BASE_URL.'assets/site.jpg'?>">
    <meta property="og:url" content="<?php echo BASE_URL ?>">
    <meta name="og:locale" content="fa_IR">
    <meta property="og:description" content="<?php echo $description?>">


    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="پوشکا مرجع شبکه های اجتماعی">
    <meta name="twitter:description" content="<?php echo $description?>">
    <meta name="twitter:site" content="@PoushkaSite">
    <meta name="twitter:image" content="<?php echo BASE_URL.'assets/site.jpg'?>">

    <meta name="description" content="<?php echo $description?>">
    <meta name="keywords" content="<?php echo $keywords?>">



    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!--    <link rel="stylesheet" href="css/bootstrap.css">-->

    <link rel="stylesheet" href="awsome/css/all.min.css">
    <link rel="stylesheet" href="css/style.css?<?php echo "version=".STYLE_VERSION?>">
    <link rel="icon" href="favicon.ico">

<!--    <script-->
<!--            src="https://code.jquery.com/jquery-3.4.1.min.js"-->
<!--            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="-->
<!--            crossorigin="anonymous"></script>-->



   <script src="js/jquery.js"></script>

    <script src="js/config.js"></script>
    <link rel="canonical" href="https://www.poushka.com" />

    <script>
        var is_open_from_panel = false;
        var current_social_script = "Instagram"; // to pass to register script
        //for using the current social when new page is open
    </script>
</head>

<body>

<nav class="navbar sticky-top navbar-expand-lg navbar-light bg-white pr-1 pl-1 pr-md-2 pl-md-2">

        <a href="<?php echo BASE_URL . 'new/' . $current_social[E_NAME_SOCIAL] ?>" id="new_page"
           class="navbar-brand alertPulse-css bg_new_page brand-background ml-1 ml-md-2 ">
           ثبت صفحه یا کانال
        </a>
    
    <div class="d-flex flex-row  order-lg-3">
        <div class="d-flex align-items-center mr-lg-5 ">
            <a href="<?php echo BASE_URL ?>" class="text-bold brand-color text-bold brand-textsize">
                <img src="assets/typo.png">
            </a>
        </div>
        <button class="navbar-toggler  mr-2  navbar-toggler-right " type="button"
                data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <sapn class="navbar-toggler-icon"></sapn>
        </button>
    </div>

    <div class="collapse navbar-collapse rtl" id="navbarSupportedContent">


        <ul class="navbar-nav pr-1 mr-lg-2 mr-md-2 text-right my-2 my-lg-0 ">

            <li class="nav-item mr-3 bottom-border active ">
                <a data-nv="1" href="#about" class="nav-link text-medium">

                    درباره پوشکا
                </a>
            </li>

            <li class="nav-item   active mr-3">
                <a data-nv="1" href="#networks" class="nav-link text-medium ">
                    شبکه ها
                </a>
            </li>

            <li class="nav-item   active mr-3">
                <a data-nv="1" href="#register" class="nav-link text-medium">
                    افزایش بازدید
                </a>
            </li>


            <li class="nav-item  active mr-3">
                <a data-nv="1" href="#services" class="nav-link text-medium ">
                    ویژگی ها
                </a>
            </li>
            <li class="active mr-3">
                <?php
                if (isset($_SESSION[LOGIN]) && $_SESSION[LOGIN] === true) {
                    ?>
                    <a rel="nofollow" class="nav-link text-medium" href="<?php echo BASE_URL . 'dashbord' ?>">
                        <i class="fa fa-user  ml-1"></i> حساب کاربری </a>

                    <?php
                } else { ?>
                    <a rel="nofollow" class="nav-link text-medium" id="user_panel" href="#login_modal" data-toggle="modal">
                        <i class="fa fa-user  ml-1"></i> ورود/ثبت نام </a>
                    <?php

                }


                ?>

            </li>


        </ul>


    </div>


</nav>  <!--nav bar-->




<div id="about" class="section-padding  mt-2 mt-lg-3">
    <div class="container">


          <div class="about-container row m-1 rtl "  >

              <div class="col-md-8 order-2 order-md-1 pl-md-5 pl-sm-2 d-flex flex-column text-justify" >
                  <h1  class="mt-5 mr-md-4 mr-2 myh1 text-bold text-white">پوشــــکا مرجع معرفی و ثبت صفحه و کانال های شبکه های اجتماعی</h1>
                  <p class="text-white  font-size-13 mr-md-4 mr-2 mt-4 ">پوشکا مرجع کاملی از کانال ها و صفحات اینستاگرام، تلگرام، آپارات، سروش، روبیکا بر اساس تعداد عضو و دنبال کننده میباشد در پوشکا شما قادرید صفحه یا کانال خود با به صورت رایگان ثبن نمایید و یا  به راحتی صفحه و کانال مورد نظر را با استفاده از امکانات متنوع پوشکا پیدا کنید و با مشاهده تعداد عضو، توضیحات، امتیاز، تصاویر، نظرات و ویدیو از کیفیت محتوای صفحه یا کانال اطمینان حاصل پیدا نمایید</p>
                  <a class="mr-md-4 mr-2 mt-4 mb-4 text-medium btn_new_inside align-self-start"
                     href="<?php echo BASE_URL . 'new/' . $current_social[E_NAME_SOCIAL] ?>">ثبت رایگان صفحه یا کانال</a>
              </div>

              <div class="col-md-4 order-1 about-img order-md-2 p-md-0">
                  <img src="assets/jingili.png" height="250px"  >

              </div>




          </div>


    </div>

</div> <!-- about div-->




<div id="networks" class="container max-container" >

    <h2 class="myh3 d-block text-bold text-center mt-md-5 mt-3">محبوب ترین صفحات و کانال های شبکه های اجتماعی </h2>
    <p class="text-center semi-black-color d-block mt-3">شبکه مورد نظر را انتخاب و صفحات و کانال های برتر و پرمخاطب  در دسته بندی های فیلم <br>و سینما ، فروشگاه ، موسیقی و ... را به راحتی پیدا نمایید </p>

    <div class="row rtl mt-2 mb-md-5 mb-3">
        <?php
        foreach ($socials as $social) {
            $src_social = IMG_URL . "/social_cat/" . $social["pic"];
            $social_href = BASE_URL_M . $social["e_name"];
            $enter_text = " ورود به " . $social["prefix_p"] . " " . $social["p_name"];
            $alt = "مرجع ".$social["prefix_p"]." ".$social["p_name"];
            $description = "محبوبترین " . $social["prefix_p"] . " " . $social["p_name"] . " را مشاهده کنید" ;

            echo "<div class='col-lg-2dot4 col-md-3 col-sm-4 col-6 text-center pr-2 pl-2 mt-5'>
                  
                   <div class='d-flex flex-column networks-item'
                            >
                   
                     <img class='align-self-center networks-img' src='$src_social'  width='75'  height='75' >
                   
               <span class='text-medium mt-4 my-black-color'>$alt</span>
               
               <span class='mt-4 mr-2 font-size-13 ml-2 text-gray'>$description</span>
   
                          
                  <a  class='mt-4 mb-4 btn_watch align-self-center' href='$social_href'>مشاهده</a>            

                    </div>
                     

                   
                 </div>";


        }

        ?>


    </div>
</div> <!--socials div-->


<div id="register" class="container-fluid  mt-3 pt-3 pb-3 bg-main-gray"  >

    <div class="container">
    <div class="row rtl">
        <div class="col-sm-6 d-flex justify-content-center">
            <img class="img-static" src="assets/view_static.svg" height="340" width="360">
        </div>

        <div class="col-sm-6 d-flex flex-column justify-content-center mt-2 text-right ">
           <p class="text-medium mb-3"> افزایش رایگان بازدید و جذب مخاطب برای صفحه و کانال شما</p>
            <p class="text-justify semi-black-color mt-2">یکی از روش های ساده برای افزایش بازید صفحه و کانال شما ثبت آن در مراجع معرفی صفحات و کانال های شبکه های اجتماعی میباشد . شما میتوانید به صورت رایگان صفحه یا کانال خود را در پوشکا ثبت نمایید
                و با ثبت توضیحات مناسب و کامل در موتور های جستجو و همچنین در معرض دید کاربران پوشکا قرار گیرید</p>
        </div>

    </div>
    </div>




</div> <!--socials div-->


<div id="services"> <!--features div-->

    <div class="container mt-5">

        <div>

            <div class="page-title text-center ">
                <h2 class="mt-3 text-bold myh3">ویژگی هایی که پوشکا را متمایز میکند </h2>

            </div>

            <div class="row feauters mt-4 p-1 rtl">

                <div class="col-md-4 col-sm-6">
                    <div class="d-flex flex-column align-items-start justify-content-center text-justify " >

                        <img  src="assets/feauters/support.svg" width="45" height="45">


                            <h3 class="myh3 font-size-14 mt-4 text-medium">پشتیبانی از پراستفاده ترین شبکه های اجتماعی کشور </h3>
                            <p class="font-size-13 semi-black-color mt-4"> در حال حاضر پوشکا با پشتیبانی از 5 شبکه
                                اجتماعی و پیام رسان پرکاربرد کشور مکان مناسبی برای ثبت صفحات و کانال های شما کاربران گر
                                امی فراهم نموده است</p>



                    </div> <!--box-service text-center-->

                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="d-flex flex-column align-items-start justify-content-center text-justify " >

                        <img  src="assets/feauters/video.svg" width="45" height="45">

                        <h3 class="myh3 font-size-14 mt-4 text-medium">ثبت توضیحات ، تصاویر و ویدیو از صفحه یا کانال شما</h3>
                        <p class="font-size-13 semi-black-color mt-4">در حال حاضر پوشکا با پشتیبانی از 5 شبکه اجتماعی و پیام رسان پرکاربرد کشور مکان
                            مناسبی برای ثبت صفحات و کانال های شما کاربران گر امی فراهم نموده است
                        </p>

                    </div> <!--box-service text-center-->

                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="d-flex flex-column align-items-start justify-content-center text-justify " >

                        <img  src="assets/feauters/plans.svg" width="45" height="45">

                        <h3 class="myh3 font-size-14 mt-4 text-medium">پلن های مختلف جهت بازدید بیشتر</h3>
                        <p class="font-size-13 semi-black-color mt-4">در اپلیکیشن پوشکا پلن های ویژه ای برای افزایش بازدید با کمترین هزینه نسبت به تبلیغات در کانال ها
                            و صفحات دیگر برای صاحبان صفحات و کانال ها در نظر گرفته شده است
                        </p>

                    </div> <!--box-service text-center-->

                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="d-flex flex-column align-items-start justify-content-center text-justify " >

                        <img  src="assets/feauters/estelam.svg" width="45" height="45">


                        <h3 class="myh3 font-size-14 mt-4 text-medium">استعلام تعداد کاربران صفحه و کانال توسط پوشکا </h3>
                        <p class="font-size-13 semi-black-color mt-4">
                            در اپلیکشن پوشکا تعداد اعضای صفحه یا کانال توسط سرورهای اپلیکیشن انجام شده به همین جهت آماری دقیق برای کاربرانی که به دنبال صفحه یا کانال خاصی میباشند به همراه دارد
                        </p>



                    </div> <!--box-service text-center-->

                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="d-flex flex-column align-items-start justify-content-center text-justify " >

                        <img  src="assets/feauters/simple.svg" width="45" height="45">

                        <h3 class="myh3 font-size-14 mt-4 text-medium">دسترسی اسان</h3>
                        <p class="font-size-13 semi-black-color mt-4">
                            دنبال صفحه یا کانال خاصی میگردید ؟ با استفاده از امکانات جستجو ، دسته بندی و برچسب ها سریعتر از هر زمان چیزی که میخوای رو پیدا کن
                        </p>

                    </div> <!--box-service text-center-->

                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="d-flex flex-column align-items-start justify-content-center text-justify " >

                        <img  src="assets/feauters/replay.svg" width="45" height="45">

                        <h3 class="myh3 font-size-14 mt-4 text-medium">ارسال پاسخ توسط مدیر کانال برای نظرات کاربران </h3>

                        <p class="font-size-13 semi-black-color mt-4">
                            در اپلیکیشن پوشکا صاحبان صفحات و کانال ها قادر هستند تا پاسخی برای نظرات کاربران ارسال نمایید و ارتباط مستقیم با کاربران داشته باشند
                        </p>

                    </div> <!--box-service text-center-->

                </div>


        </div>
    </div> <!--container-->
</div> <!--services-->


<?php include 'footer.php' ?>


<div class="modal fade" id="login_modal">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content modal-content-radius">

            <div class="modal-header login_header d-flex align-items-center  position-relative ">

                <button type="button" class="myclose mt-1  mr-sm-1" data-dismiss="modal">&times;</button>
                <h5 class="myh3 text-center text-medium w-100 mr-5 mt-2" id="tv_login_header">ورود به حساب کاربری</h5>
            </div>

            <!-- Modal body -->
            <div class="modal-body login_register">
                <div class="d-flex flex-column login_main">

                    <div class="d-flex align-items-center rtl form-group mr-2 ml-2 mr-lg-5 ml-lg-5">
                        <i class="fa fa-phone text-gray "></i>
                        <input class="form-control number_input mr-2 w-100" type="number"
                               oninput="if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                               maxlength="11" placeholder="شماره موبایل" id="et_login_mobile"
                        >

                    </div>

                    <div class="d-flex align-items-center rtl mt-3 mr-3 ml-3 mr-lg-5 ml-lg-5">
                        <i class="fa fa-lock text-gray "></i>
                        <input type="password" placeholder="رمز عبور" id="et_login_password" class="form-control  mr-2">

                    </div>

                    <p class="mt-3 ml-2 text-bold text-blue pointer" id="forget_div">فراموشی رمز عبور؟</p>


                    <p class="text-danger text-center mt-2" id="login_error"> پیام </p>

                    <button type="submit"
                            class="btn_login d-flex align-items-center justify-content-center mt-3 mr-2 ml-2 mr-lg-5 ml-lg-5"
                            id="btn_login">
                        <span class="spinner-border spinner-border-sm mr-2" id="loading_login"></span>
                        ورود
                    </button>
                </div> <!--login_modal-->

                <div class="d-none flex-column register_main">
                    <p class="text-center" id="register_message"> شمار خود را برای دریافت پیامک ورود وارد نمایید</p>
                    <div class="d-flex align-items-center rtl form-group mr-3 ml-3 mr-lg-5 ml-lg-5"
                         id="register_mobile_div">
                        <i class="fa fa-phone text-gray " id="et_register_icon"></i>
                        <input type="tel" maxlength="11" placeholder=" شماره موبایل" id="et_register_mobile"
                               class="form-control  mr-2">
                    </div>

                    <div class="flex-column d-none " id="register_final_div">

                        <div class="d-flex align-items-center rtl mt-1 mr-3 ml-3 mr-lg-5 ml-lg-5">
                            <i class="fa fa-lock text-gray "></i>
                            <input type="password" placeholder="رمز عبور " id="et_register_password"
                                   class="form-control  mr-2">
                        </div>

                        <div class="d-flex align-items-center rtl mt-3 mr-3 ml-3 mr-lg-5 ml-lg-5">
                            <i class="fa fa-lock text-gray "></i>
                            <input type="password" placeholder="تکرار رمز عبور" id="et_re_register_password"
                                   class="form-control  mr-2">
                        </div>

                        <div class="d-flex align-items-center rtl mt-3 mr-3 ml-3 mr-lg-5 ml-lg-5">
                            <i class="fa fa-user text-gray "></i>
                            <input type="password" placeholder="کد معرف(اختیاری)" id="et_moaref_password"
                                   class="form-control  mr-2">
                        </div>

                    </div>


                    <p class="text-danger  text-center mt-2" id="register_error"> پیام </p>

                    <button type="submit"
                            class="btn_login mr-lg-5 ml-lg-5 mr-2 ml-2 d-flex align-items-center mt-2 justify-content-center"
                            id="btn_register">
                        <span class="spinner-border spinner-border-sm mr-2" id="loading_register"></span>
                        <i class="fa fa-arrow-circle-left mr-2" id="icon_register"></i>
                        مرحله بعد
                    </button>
                </div> <!--register modal -->

                <div class="d-none flex-column forget_main">
                    <p class="text-center" id="message_forget">شماره خود را برای بازیابی رمز عبور وارد نمایید</p>

                    <div class="d-flex align-items-center rtl form-group mr-3 ml-3 mr-lg-5 ml-lg-5" id="div_top_forget">
                        <i class="fa  fa-phone text-gray " id="icon_top_forget"></i>
                        <input type="tel" maxlength="11" placeholder=" شماره موبایل" id="et_top_forget"
                               class="form-control  mr-2">
                    </div>


                    <div class="d-none align-items-center rtl form-group mr-3 ml-3 mr-lg-5 ml-lg-5"
                         id="div_bottom_forget">
                        <i class="fa fa-lock text-gray "></i>
                        <input type="password" placeholder="تکرار رمز عبور" id="et_bottom_forget"
                               class="form-control  mr-2">
                    </div>

                    <p class="text-danger d-none text-center mt-2" id="forget_error"> پیام </p>

                    <button type="submit"
                            class="btn_login mr-lg-5 ml-lg-5 mr-2 ml-2 d-flex align-items-center mt-2 justify-content-center"
                            id="btn_forget">
                        <span class="spinner-border spinner-border-sm mr-2" id="loading_forget"></span>
                        <i class="fa fa-redo mr-2" id="icon_forget"></i>
                        دریافت کد بازیابی
                    </button>
                </div> <!--forget modal -->


            </div>
            <!-- Modal footer -->
            <div class="modal-footer login_footer d-flex justify-content-center flex-row-reverse ">
                <span class="ml-1 mt-3 mb-3" id="lbl_login_register"> حساب کاربری ندارید؟ </span>
                <span class="ml-2 mb-3 mt-3 text-bold" id="tv_register"> ثبت نام </span>

            </div>

        </div>
    </div>


</div> <!--modal_categories-->


<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<!--<script src="js/bootstrap.min.js"></script>-->
<script src="js/owl.carousel.min.js"></script>
<script src="js/register.js"></script>

<script>

    $(document).on('click', 'a[href^="#"]', function (e) {
        var navigate = $(this).attr("data-nv");

        if (navigate === "1") {
            var id = $(this).attr('href');
            // target element
            var $id = $(id);
            if ($id.length === 0) {
                return;
            }

            // prevent standard hash navigation (avoid blinking in IE)
            e.preventDefault();

            // top position relative to the document
            var pos = $id.offset().top + -80;

            // animated top scrolling
            $('body, html').animate({scrollTop: pos});
        }

        // target element id

    });


    $('.nav-item').click(function () {

        $('.nav-item').removeClass('bottom-border')
        $(this).addClass('bottom-border')
    })


</script>



</body>
</html>