<?php

use GuzzleHttp\Exception\ClientException;
use Src\helper\Practical as MyPractical;
include '../helper/init.php';

$main = 1 ;
$key = null;
$page=1 ;
$address  = "";

$social_id = 1;
$cat_id = 0;

$social_position = 0;
$current_social = null;

$response = $client->request('GET', 'getSC');
$socials_categories = json_decode($response->getBody(), true);
$socials = $socials_categories["socials"];
$categories= $socials_categories["categories"];
$new_page_modal = false ;  // it define that modal is opening from new page or user panel
$category_en = null;
$category_fa = null;

$key_description  = "";

if (isset($_GET["social"]) && isset($_GET["category"]) && isset($_GET["key"]))
{

    $social = $_GET["social"];
    $category_en = $_GET["category"];
    $stmt = $conn->prepare("select social_id from sociall where e_name like ?");
    $stmt->bind_param("s", $social);
    $stmt->execute();
    $result_social = $stmt->get_result();
    if ($result_social->num_rows ==0) {
        header('Location:'.BASE_URL."404");
    }
    $social_db = $result_social->fetch_assoc();
    $social_id = $social_db["social_id"];

    $stmt = $conn->prepare("select cat_id,name from category where en_name like ?");
    $stmt->bind_param("s", $category_en);
    $stmt->execute();
    $result_cat = $stmt->get_result();
    if ($result_cat->num_rows ==0) {
        header('Location:'.BASE_URL."404");
    }

    $cat_db = $result_cat->fetch_assoc();
    $cat_id = $cat_db["cat_id"];
    $category_fa=$cat_db["name"];

    $main = 0;

    for ($i = 0; $i < count($socials); $i++) {
        if ($social_id == $socials[$i][SOCIAL_ID_SOCIAL]) {
            $social_position = $i;
            setcookie(SOCIAL_POSITION, $social_position, 2147483647, "/");
        }
    }
    $current_social = $socials[$social_position];
    $main = 0;
    $key=$_GET["key"];

    $address=BASE_URL."all/".$social."/".$category_en."/".$key;


}else if (isset($_GET["social"]) && isset($_GET["key"]) ){

    $social = $_GET["social"];
    $stmt = $conn->prepare("select social_id from sociall where e_name like ?");
    $stmt->bind_param("s", $social);
    $stmt->execute();
    $result_social = $stmt->get_result();
    if ($result_social->num_rows==0) {
        header('Location:'.BASE_URL."404");
    }
    $social_db = $result_social->fetch_assoc();
    $social_id = $social_db["social_id"];

    for ($i = 0; $i < count($socials); $i++) {
        if ($social_id == $socials[$i][SOCIAL_ID_SOCIAL]) {
            $social_position = $i;
            setcookie(SOCIAL_POSITION, $social_position, 2147483647, "/");
        }
    }
    $current_social = $socials[$social_position];


    $main = 1;
    $key=$_GET["key"];

    $address=BASE_URL."all/".$social."/".$key;



}else {
    header('Location:'.BASE_URL."404");
}
if ($key=="newest") {
    $key_description=" جدیترین و به روز ترین " ;
}else if ($key="maxMember") {
    $key_description=" پر مخاطب ترین و پرجمعیت ترین " ;
}else {
    $key_description="پر بازدیدترین";
}


try {
    $list_request = $client->request('GET', "PList/" . $current_social[SOCIAL_ID_SOCIAL], [
        'query' => ["main" => $main, 'cat_id' => $cat_id, 'key' => $key ,'page' => $page
        ]
    ]);
    $list_request_json = json_decode($list_request->getBody(),true);


} catch (GuzzleHttp\Exception\ClientException $e) {
    $response_tttt = $e->getResponse();
    $responseBodyAsString = $response_tttt->getBody()->getContents();
}

$title =  $list_request_json["title"];
$og_image=  BASE_URL."assets/site.jpg";


if ($main==1) {
    $og_url = BASE_URL."all/".$social."/".$key;

    $description = "لیست " . $list_request_json["title"]. " | " . $key_description . $current_social["prefix"]  . " و ".$current_social["prefix_p"] . " " . $current_social['p_name'] . " را در پوشا مرجع معرفی "
    . $current_social["prefix"]." " . $current_social["p_name"] . " مشاهده نمایید";
    $keywords = $list_request_json["title"];



}else {
    $og_url = BASE_URL."all/".$social."/".$category_en."/".$key;
    $description = "لیست " . $list_request_json["title"]. " | " . $key_description. $current_social["prefix"]  . " و ".$current_social["prefix_p"] . " ".$category_fa . " " .$current_social['p_name'] . " را در پوشکا مرجع معرفی "
     . $current_social["prefix"]." " . $current_social["p_name"] . " مشاهده نمایید";
    $keywords = $list_request_json["title"];

}

include '../m/header.php';



?>

<div class="container min-height-70 d-flex flex-column">

    <div class="d-flex justify-content-end mt-3 mr-xl-4 mr-md-2 mr-1">
        <h1 class="main-h1 text-center"><?php echo $list_request_json["title"]  ?></h1>
    </div>


<div class="row mt-2" id="main_row">
    <?php    foreach ($list_request_json["plist"] as $sinlge_item) {
     $alt =  $current_social[PREFIX_SOCIAL] . " "  . $current_social["p_name"] . " "  . htmlspecialchars($sinlge_item["name"],ENT_QUOTES,"UTF-8")
      . " - ".$sinlge_item["page_id"];
     ;
     $social_address = BASE_URL . "social/" . $current_social[E_NAME_SOCIAL] . "/" . htmlspecialchars( $sinlge_item["id"],ENT_QUOTES,"UTF-8") ;
     ?>

    <div class=" col-12 d-flex flex-row-reverse mt-2 pr-xl-5 pl-xl-5 pr-1 pl-1 lis-item" data-id=<?php echo $sinlge_item["page_id"] ?>>
        <a
           href=<?php echo $social_address ?>>
        <img  class="fit-cover" src='<?php echo IMG_URL . "chanel_pic/". $sinlge_item["thumb"] ?>' height="93px"
             width="85px" alt="<?php echo $alt ?>" style="border-radius: 6px" >
        </a>

        <div class="d-flex flex-column  w-100">

            <div class="d-flex flex-row-reverse justify-content-between mr-1 list_item">
                <a title="<?php echo $alt ?>"  class="text-bold text-dark" href=<?php echo $social_address ?>><?php echo htmlspecialchars($sinlge_item["name"],ENT_QUOTES,"UTF-8")  ?></a>
                 <a   href="<?php echo $current_social[WEB] .htmlspecialchars($sinlge_item['id'],ENT_QUOTES,"UTF-8") ?>" rel="nofollow" target="_blank"
                      class="list_a text-center mr-auto small-font d-flex align-items-center mt-0"> <?php
                       echo "ورود به " . $current_social["prefix"]?></php> </a>
            </div>

            <span class="mt-1 text-right mr-1 "><?php echo htmlspecialchars($sinlge_item["short_des"],ENT_QUOTES,"UTF-8")  ?> </span>
            <div class="d-flex flex-row-reverse align-items-center">
                <span class="mt-1 text-right text-success text-bold  mr-1"><?php echo  MyPractical::thousandsCurrencyFormat($sinlge_item["member"]) ?> <i class="text-dark fa fa-users"> </i> </span>
                <span class="text-success text-bold mr-1"> <?php echo   $current_social[MEMBER_PREFIX_SOCIAL] ?></span>

            </div>

            <hr class="w-100 hr-bg" >
        </div>
    </div>
    <?php

    }?>



    </div> <!--list row -->


    <?php
    if (count($list_request_json["plist"])>14) {
        echo "   
       
           <span id='load_more' class=\"align-self-center mb-2 d-flex pointer align-items-center more_items\">
            <div class=\"spinner-border d-none spinner-border-sm mr-2\">
            </div> نمایش بیشتر</span>
            
            "

        ;
    }

    ?>



</div><!-- container_list-->

<?php include "../footer.php";?>


</body>

<script src="js/owl.carousel.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<!--<script src="js/bootstrap.min.js"></script>-->
<script src="js/script.js"></script>
<script src="js/config.js"></script>
<script src="js/register.js"></script>
<script>
    var page = 2;
    var main = <?php echo $main?>;
    var cat_id = <?php echo $cat_id?>;
    var social_id = <?php echo $current_social["social_id"]?>;
    var key = "<?php echo $key?>";
    var loading = false ;


    $('#load_more').click(function () {

        if (loading==false) {
            loading=true;
            $('#load_more .spinner-border').removeClass('d-none').addClass('d-flex');
            $.ajax({
                type:"get",
                url:baseURl+"PList/"+social_id,
                data :{
                    'main':main,
                    'cat_id':cat_id,
                    'key':key,
                    'page':page
                },
                complete : function () {
                    $('#load_more .spinner-border').removeClass('d-flex').addClass('d-none');
                    page++;
                    loading=false;
                },
                success : function (result, status, xhr) {
                    if (!result["hasNext"]) {
                        $('#load_more').removeClass('d-flex').addClass('d-none')
                    }



                    $.each(result['plist'],function (index,element) {


                        var alt_item = "<?php echo $current_social[PREFIX_SOCIAL]. " " . $current_social["p_name"] ?>"+ " " +escapeHtml(element.name)+" - "+element.page_id;
                        var social_address = web_url + "social/"+"<?php echo $current_social[E_NAME_SOCIAL]?>"+"/"+escapeHtml(element.id);
                        var image_url_item = IMAGE_URL + "chanel_pic/"+element.thumb;
                        var join_href = "<?php $current_social[WEB] ?>" + escapeHtml(element.id);
                        var enter_text = "ورود به " + "<?php echo $current_social[PREFIX_SOCIAL]?>";
                        var member_item = nFormatter(element.member);
                        var member_prefix = "<?php echo   $current_social[MEMBER_PREFIX_SOCIAL] ?>";


                        console.log(alt_item);

                        var div = "<div class='col-12 d-flex flex-row-reverse mt-2 pr-xl-5 pl-xl-5 pr-1 pl-1 lis-item' data-id='"+element.page_id+"'>" +
                            "<a href='"+social_address+"'> <img src='"+image_url_item+"' class='fit-cover' height='93px' width='85px' alt='"+alt_item+"' style='border-radius: 6px' ></a>" +
                            "<div class='d-flex flex-column w-100'>" +
                            "<div class='d-flex flex-row-reverse justify-content-between mr-1 list_item'>" +
                            "<a title='"+alt_item+"' class='text-bold text-dark' href='"+social_address+"'>"+escapeHtml(element.name)+"</a>" +
                            "<a href='"+join_href+"' rel='nofollow' target='_blank' class='list_a text-center mr-auto small-font d-flex align-items-center mt-0'>"+enter_text+"</a>" +
                            "</div>" +
                            "<span class='mt-1 text-right mr-1'>"+escapeHtml(element.short_des)+"</span>" +
                            "<div class='d-flex flex-row-reverse align-items-center'>" +
                            "<span class='mt-1 text-right text-success text-bold  mr-1'>"+member_item+" <i class='text-dark fa fa-users'></i></span>" +
                            "<span class='text-success text-bold mr-1'>"+member_prefix+"</span>"+
                            "</div>" +
                            "<hr class='w-100 hr-bg'>"+
                            "</div>" +
                            "" +
                            "</div>";

                        $("#main_row").append(div);




                    });



                },
                error : function (xhr, status, error) {
                    console.log()
                    alert(xhr.responseText)
                }

            })
        }




    })


</script>





</html>
