<?php
//todo footer !!!!
//todo fix make page
include "helper/init.php";

$user=null;
if (isset($_SESSION["user"])) {
    $user = json_decode($_SESSION["user"],true);
}

$response_supports = $client->request('GET', 'supportTypes');
$supports = json_decode($response_supports->getBody(), true);
$description="بخش پشتیبانی و انتقادات و پیشنهادات در پوشکا ";



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href=<?php echo BASE_URL ?>>
    <meta charset="utf-8">
    <title> بخش پشتیبانی - پوشکا</title>
    <meta http-equiv="content-language" content="fa">
    <meta property="og:site_name" content="پوشکا - مرجع شبکه های اجتماعی">
    <meta name="description" content="<?php echo $description?>">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!--    <link rel="stylesheet" href="css/bootstrap.css">-->
    <link rel="stylesheet" href="awsome/css/all.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
<!--    <script src="js/jquery.js"></script>-->
    <script src="js/config.js"></script>

</head>

<body>

<nav class="navbar sticky-top d-flex justify-content-between navbar-light bg-white pr-1 pl-1 pr-md-2 pl-md-2">
    <a href="<?php echo BASE_URL . 'new/Instagram' ?>" id="new_page"
       class="navbar-brand alertPulse-css bg_new_page brand-background ml-1 ml-md-2 ">
        ثبت صفحه یا کانال
    </a>
    <a href="<?php echo BASE_URL ?>" class="text-bold brand-color mr-2 text-bold brand-textsize">پوشکا</a>

</nav>  <!--nav bar-->


<div class="min-height-70 container rtl d-flex flex-column">
    <h1 class="main-h1 mt-3 align-self-center"> ارتباط با بخش پشتیبانی پوشکا</h1>

    <div class="row text-right">
        <div class="col-lg-10 mr-auto ml-auto">
            <div class="d-flex flex-column">

                <span>گزینه پشتیبانی خود را انتخاب نمایید </span>
                <select id="select_types" class="mt-2" name="گزینه" style="padding: 7px 4px; border-radius: 4px; background: #f0eff3">

                    <?php
                    foreach ($supports as $support) {
                        echo " <option id='$support[st_id]' value=\"volvo\">$support[text]</option>";
                    }

                    ?>

                </select>


                <span class="mt-3">شماره موبایل </span>
                <div class="form-group  position-relative mt-1">
                    <div>
                        <i class="fa fa-phone position-absolute drawable_rigth_support" ></i>
                        <input type="text"   class="form-control pr-4"  id="et_mobile">
                    </div>
                </div>


                <span class="mt-1"> عنوان تیکت پشیتیبانی </span>
                <div class="form-group  position-relative mt-1">
                    <div>
                        <i class="fa fa-header position-absolute drawable_rigth_support" ></i>
                        <input type="text"  class="form-control pr-4"  id="et_title">
                    </div>
                </div>


                <span class="mt-1 mb-0"> توضیحات  </span>
                <div class="form-group   mt-1">
                    <textarea id="et_des" rows="4" class="form-control" ></textarea>
                </div>



                <span > تصویر ضمیمه  </span>

                <div class="d-flex flex-column image-upload">
                    <img src="assets/gallery.png" class="mt-1 pointer" id="support-img" width="135" height="135">
                    <input id="img-input" type="file" accept="image/x-png,image/gif,image/jpeg"
                           data-type='image'/>

                </div>


                <div id="div-message" class="d-none mt-2">ارسال درخواست با موفقیت انجام شد</div>

                <button id="btn_save" class="btn btn-success align-self-end mt-2">ارسال درخواست </button>

            </div>

         <hr />





        </div>


    </div>


    <div style="height: 200px;" class="out mt-1 d-flex flex-column justify-content-center">

        <span class="text-center mr-1 text-bold mb-4">ارتباط مستقیم با پشتیبانی</span>
        <ul class="text-center pl-4" id="ul-support">
            <i><a><i class="fab fa-instagram"></i></a></i>
            <i><a><i class="fab fa-telegram"></i></a></i>
            <i><a><i class="fa fa-envelope"></i></a></i>
        </ul>

</div>
</div>


<?php include 'footer.php' ?>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<!--<script src="js/bootstrap.min.js"></script>-->
<script src="js/compress.js"></script>

<script>
    $(document).ready(function () {

        var apikey = "<?php
          if (is_null($user)) {
              echo "";
          }else {
              echo $user["apikey"];
          }

        ?>";


        var myFormData = new FormData();

        $('#btn_save').click(function () {

            var st_id = $('#select_types').find(":selected").attr('id');
            var title = $('#et_title').val();
            var des = $('#et_des').val();
            var mobile = $('#et_mobile').val();

            var yourObject = {
                st_id: st_id,
                title: title,
                message: des,
                mobile: mobile

            }
            myFormData.append('object', JSON.stringify(yourObject));
            showLoading(true)
            showError(false,false,"")
            $.ajax({
                type: "post",
                processData: false, // important
                contentType: false, // important
                dataType: 'json',
                url: baseURl + "support",
                data: myFormData,
                headers: {
                    "Authorization": apikey
                },
                oncomplete: function () {

                    showLoading(false)
                },

                success: function (result, status, xhr) {
                    showLoading(false)
                    showError(true, true, result["message"]);

                },

                error: function (xhr, status, error) {
                    console.log(xhr.responseText)
                    var json = JSON.parse(xhr.responseText);
                    showLoading(false)
                    showError(true, false, json["message"])

                }


            })


        })

        $('#support-img').click(function () {
            $('#img-input').trigger('click');
        })


        //handling what happens after img-input is selected
        $("#img-input").change(function () {
            //  var formData = new FormData(this);
            readURL(this);
        });



        function readURL(input) {
            if (input.files && input.files[0]) {
                var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(input.files[0]['type'], validImageTypes) > 0) {
                    // invalid file type code goes here.

                    new Compressor(input.files[0], {
                        quality: .8,
                        convertSize:50000,
                        success(result) {
                            $('#support-img').attr('src', window.URL.createObjectURL(result));
                            myFormData.append('pic', result, result.name)

                        },
                        error(e) {
                            console.log(e.message);
                        },
                    });


                } else {
                    swal({
                        title: "خطا !",
                        text: "فرمت انتخاب شده مجاز نمیباشد لطفا یک تصویر انتخاب نمایید",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }


            }
        }

        function isEmpty(value) {
            return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
        }



        function showLoading(show) {
            if (show) {
                $('#btn_save').html("  درحال ارسال\n" +
                    "                         <span class=\"spinner-border spinner-border-sm mr-2\"></span>\n")
            } else {
                $('#btn_save').html("ارسال درخواست")
            }
        }

        function showError(show, success, message) {
            if (show) {
                if (success) {
                    $('#div-message').removeClass('d-none').addClass('success-div').html(message)
                } else {
                    $('#div-message').removeClass('d-none').addClass('error-div').html(message)
                }

            } else {
                $('#div-message').addClass('d-none')
            }
        }


    })
</script>

</body>
</html>