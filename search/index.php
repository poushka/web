<?php
use Src\helper\Practical as MyPractical;
include '../helper/init.php';

$search  = null;

$main = 1 ;
$key = null;
$page=1 ;
$address  = "";

$social_id = 1;
$cat_id = 0;

$social_position = 0;
$current_social = null;


$response = $client->request('GET', 'getSC');
$socials_categories = json_decode($response->getBody(), true);
$socials = $socials_categories["socials"];
$categories= $socials_categories["categories"];
$new_page_modal = false ;  // it define that modal is opening from new page or user panel
$category_en = null;
$category_fa = null;



if (isset($_GET["social"]) && isset($_GET["search"])){
    $social = $_GET["social"];
    $stmt = $conn->prepare("select social_id from sociall where e_name like ?");
    $stmt->bind_param("s", $social);
    $stmt->execute();
    $result_social = $stmt->get_result();
    if ($result_social->num_rows==0) {
        header('Location:'.BASE_URL."404");
    }
    $social_db = $result_social->fetch_assoc();
    $social_id = $social_db["social_id"];

    for ($i = 0; $i < count($socials); $i++) {
        if ($social_id == $socials[$i][SOCIAL_ID_SOCIAL]) {
            $social_position = $i;
            setcookie(SOCIAL_POSITION, $social_position, 2147483647, "/");
        }
    }
    $current_social = $socials[$social_position];


    $main = 1;
    $search=$_GET["search"];


}else {
    die("we should go 404");
}

$list_request = $client->request('GET', "Psearch/" . $current_social[SOCIAL_ID_SOCIAL], [
    'query' => [ 'search' => $search ,'page' => 1
    ]
]);

$list_request_json = json_decode($list_request->getBody(),true);

$og_url = BASE_URL."search/".$social."/".$search;
$og_image=  BASE_URL."assets/site.jpg";

$title =  "نتیجه جستجو " . htmlspecialchars($search,ENT_QUOTES,"UTF-8")  . " در " . $current_social["prefix_p"] . " " . $current_social[P_NAME_SOCIAL];

$description = "نتیجه جستجو " . htmlspecialchars($search,ENT_QUOTES,"UTF-8")  . " در " . $current_social["prefix_p"] . " " . $current_social[P_NAME_SOCIAL] . " در پوشکا";

$keywords = $search;

include '../m/header.php';







?>





<div class="container min-height-70 d-flex flex-column">



    <div class="d-flex justify-content-end mt-3 mr-xl-4 mr-md-2 mr-1">
        <h1 class="main-h1 text-center"><?php echo "نتیجه جستجو " . htmlspecialchars($search,ENT_QUOTES,"UTF-8")  . " در " . $current_social["prefix_p"] . " " . $current_social[P_NAME_SOCIAL] ?></h1>
    </div>

    <?php if (count($list_request_json)==0) {

        echo "<div class='d-flex flex-column no-item align-items-center'>
           <i class='fal fa-3x fa-search'></i>
           <p class='mt-2'>هیچ نتیجه ای یافت نشد</p>
            </div>";

    } ?>


    <div class="row" id="main_row">
        <?php    foreach ($list_request_json as $sinlge_item) {
            $alt = $current_social[PREFIX_SOCIAL] . " " . htmlspecialchars($sinlge_item["name"] ,ENT_QUOTES,"UTF-8");
            $social_address = BASE_URL . "social/" . $current_social[E_NAME_SOCIAL] . "/" . htmlspecialchars($sinlge_item["id"] ,ENT_QUOTES,"UTF-8")  ;
            ?>

            <div class="col-lg-12 col-12 d-flex flex-row-reverse mt-2 pr-xl-5 pl-xl-5 pr-1 pl-1 lis-item" data-id=<?php echo $sinlge_item["page_id"] ?>>
                <a  class="position-relative"  href=<?php echo $social_address ?>>
                    <?php
                    if ($sinlge_item["ad"]==1) {
                        echo "<img class='position-absolute ad-search' src='assets/ic_reserve.png' 
                           width='44px' height='44px'>";
                    }

                    ?>
                    <img  src= <?php echo IMG_URL . "chanel_pic/". $sinlge_item["thumb"]  ?> height="93px"
                          width="85px" alt=<?php echo $alt ?> style="border-radius: 6px" >
                </a>

                <div class="d-flex flex-column  w-100">

                    <div class="d-flex flex-row-reverse justify-content-between mr-1 list_item">
                        <a  class="text-bold text-dark" href=<?php echo $social_address ?>><?php echo htmlspecialchars($sinlge_item["name"] ,ENT_QUOTES,"UTF-8")  ?></a>
                        <a href='<?php echo $current_social[WEB] . htmlspecialchars($sinlge_item["id"],ENT_QUOTES,"UTF-8")  ?>'   class="list_a text-center mr-auto small-font d-flex align-items-center mt-0"> ورود به کانال </a>
                    </div>
                    <span class="mt-1 text-right mr-1 "><?php echo htmlspecialchars($sinlge_item["short_des"],ENT_QUOTES,"UTF-8")  ?> </span>
                    <div class="d-flex flex-row-reverse align-items-center">
                        <span class="mt-1 text-right text-success text-bold  mr-1"><?php echo  MyPractical::thousandsCurrencyFormat($sinlge_item["member"]) ?> <i class="text-dark fa fa-users"> </i> </span>
                        <span class="text-success text-bold mr-1"> <?php echo   $current_social[MEMBER_PREFIX_SOCIAL] ?></span>

                    </div>

                    <hr class="w-100 hr-bg" >
                </div>
            </div>
            <?php

        }?>



    </div> <!--list row -->

    <div>

    </div>

    <?php
    if (count($list_request_json)>7) {
        echo "     <span id='load_more' class=\"align-self-center mb-2 d-flex pointer align-items-center more_items\">
            <div class=\"spinner-border d-none spinner-border-sm mr-2\">
            </div> نمایش بیشتر</span>";
    }

    ?>





</div><!-- container_list-->

<?php include '../footer.php'?>

</body>

<script src="js/owl.carousel.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<!--<script src="js/bootstrap.min.js"></script>-->
<script src="js/script.js"></script>
<script src="js/register.js"></script>
<script>

    var page = 2 ;
    var loading = false ;
    var finished = false ;
    var search = "<?php echo $search ?>";


    $("#load_more").click(function () {

        if (!loading && !finished) {
            var key =   <?php echo  "'$key'"?>;
            loading=true;
            showLoading(true)
            $.ajax({
                type : "get",
                url : baseURl +  "Psearch/"+<?php echo $current_social[SOCIAL_ID_SOCIAL]?>,
                data :  {

                    'search' : search ,
                    'page' : page
                },

                complete: function () {
                    showLoading(false)
                    loading=false;


                },

                success: function (result, status, xhr) {

                       page++;
                       if (result.length<8) {
                           $('#load_more').removeClass('d-flex').addClass('d-none');
                       }

                        $.each(result, function(index, element) {
                            var alt = " <?php echo $current_social[PREFIX_SOCIAL] ?> " + element.name ;
                            var social_address = " <?php echo BASE_URL . "social/" . $current_social[E_NAME_SOCIAL] . "/"  ?>"  + escapeHtml(element.id);
                            var src = "<?php echo  IMG_URL . "chanel_pic/"?>" + element.thumb ;
                            var join = "<?php echo $current_social[WEB] ?>"+element.id;
                            var member_prefix = "<?php echo $current_social[MEMBER_PREFIX_SOCIAL] ?>";
                            var prefix = "<?php echo $current_social[PREFIX_SOCIAL] ?>";
                            var member = nFormatter(element.member,1);


                            var div = "<div class='col-lg-12 col-12 d-flex flex-row-reverse mt-2 pr-xl-5 pl-xl-5 pr-1 pl-1 lis-item' data-id='"+element.page_id+"'>" +
                                "<a href='"+social_address+"'><img src='"+src+"' width='93px' height='93px' style='border-radius: 6px'> </a>" +
                                "<div class='d-flex flex-column w-100'>" +
                                "<div class='d-flex flex-row-reverse justify-content-between mr-1 list_item'>" +
                                "<a class='text-bold text-dark' href='"+social_address+"'>"+ escapeHtml(element.name)+"</a>" +
                                "<a href='"+join+"' class='list_a text-center mr-auto small-font d-flex align-items-center mt-0'> ورود به  "+prefix+"</a>" +
                                "</div> " +
                                "<span class='mt-1 text-right mr-1'> "+escapeHtml(element.short_des)+"</span>" +
                                "<div class='d-flex flex-row-reverse align-items-center'>" +
                                "<span class='mt-1 text-right text-success text-bold mr-1'> "+member+" <i class='fa fa-users my-black-color'></i></span>" +
                                "<span class='mt-1 text-right text-success text-bold mr-1'> "+member_prefix+" </span>" +
                                "</div>" +
                                "<hr class='w-100 hr-bg'>" +
                                "</div>" +
                                "" +
                                "</div>"
                            $("#main_row").append(div);


                        });




                },


                error: function (xhr, status, error) {

                    var json = JSON.parse(xhr.responseText);
                    if (xhr.status===404) {
                        finished=true;
                    }


                    //   setErrorRegister(true, json["message"])
                }




            });
        }





    })



    function showLoading( show) {
        if(show) {
            $('#load_more .spinner-border').removeClass('d-none').addClass('d-flex');
        }else {
            $('#load_more .spinner-border').removeClass('d-flex').addClass('d-none');
        }
    }




</script>


</html>
