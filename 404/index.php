<?php
error_reporting(E_ALL);

//session_destroy();
require '../vendor/autoload.php';
require '../helper/config.php'

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <base href=<?php echo BASE_URL?>>
    <title><?php echo "صفحه مورد نظر یافت نشد !"?></title>

    <meta charset="utf-8">

    <link rel="stylesheet" href="css/bootstrap.css">

    <link rel="stylesheet" href="awsome/css/all.min.css">

    <link rel="stylesheet" href="css/boot.css">

    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" href="css/common.css">

</head>
<body>

<div class="container rtl">
    <div class="mt-3 mt-md-4 mt-lg-5 d-flex flex-column justify-content-center align-items-center"
         style="min-height: 70vh">
        <img src="assets/error-404.svg" width="125" height="125">
        <p class="text-bold mt-2">صفحه مورد نظر یافت نشد</p>
        <a href="<?php echo BASE_URL?>" class="next-page mt-2 "> بازگشت به صفحه اصلی <i class="fa fa-angle-left"></i> </a>

    </div>

</div>


<?php include '../footer.php'?>


</body>
</html>

