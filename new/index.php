<?php

use GuzzleHttp\Exception\ClientException;
use Src\helper\Practical as MyPractical;
include '../helper/init.php';

if (!isset($_SESSION["login"])) {


 //   echo "<script> location.replace(web_url); </script>";
}



$social_id = 1;
$cat_id = 0;
$main = 1;
$social_position = 0;
$current_social = null;

$response = $client->request('GET', 'getSC');
$socials_categories = json_decode($response->getBody(), true);
$socials = $socials_categories["socials"];
$categories= $socials_categories["categories"];
$new_page_modal = false ;  // it define that modal is opening from new page or user panel
$category_en = null;
$category_fa = null;

$has_screen = false; // to initilazie gallery if there is any picture
if (isset($_GET["social"])) {

    $social = $_GET["social"];
    $stmt = $conn->prepare("select social_id from sociall where e_name like ?");
    $stmt->bind_param("s", $social);
    $stmt->execute();
    $result_social = $stmt->get_result();
    if ($result_social->num_rows==0) {
        header('Location:'.BASE_URL."404");
    }
    $social_db = $result_social->fetch_assoc();
    $social_id = $social_db["social_id"];

    for ($i = 0; $i < count($socials); $i++) {
        if ($social_id == $socials[$i][SOCIAL_ID_SOCIAL]) {
            $social_position = $i;
            setcookie(SOCIAL_POSITION, $social_position, 2147483647, "/");
        }
    }
    $current_social = $socials[$social_position];

    $og_url = BASE_URL."new/".$social;
    $og_image=  BASE_URL."assets/site.jpg";
    $title = "ثبت رایگان" . " " . $current_social["prefix"] .  " ".$current_social["p_name"] . "در گوگل - پوشکا";
    $header = "ثبت رایگان" . " " . $current_social["prefix"] .  " ".$current_social["p_name"] ;
    $description = "ثبت رایگان". " ".  $current_social["prefix"] .  " ".$current_social["p_name"] . " جهت تبلیغات و افزایش بازدید و جذب مخاطب در پوشکا مرجع شبکه های اجتماعی";
    $keywords = "معرفی کانال، معرفی صفحه، ثبت کانال، افزایش بازدید";
}




include '../m/header.php';
//we cant use php location becase we cant set header after outputing any thing

$cat_id = 0;
$tags_request = $client->request('GET', 'gettags');
$tags = json_decode($tags_request->getBody(), true);


?>

<script src="js/compress.js"></script>

<script>
    $('#new_page').removeClass('d-flex').addClass('d-none');
</script>



<!--<div class="d-flex flex-column justify-content-center align-items-center new-root" id="sosials_center">
    <div class="rounded_container">-->


 <div class="container position-relative d-flex justify-content-center align-items-center" style="min-height: 70vh">

            <div class="row d-flex justify-content-center w-100">


                <div class=" col-lg-6 col-md-8 col-sm-12 d-flex flex-column register-col justify-content-center align-items-center pr-0 pl-0 pt-2 pb-2">
                    <!--register col -->
                    <h1 class="mb-2 text-medium myh4" id="newpage-heaer" ><?php echo $header ?></h1>

                    <span class="text-center rtl text-gray " id="tv_message_new"> لطفا گزینه مورد نظر خود را انتخاب نمایید</span>

                    <div class="pt-1 pb-1 pl-1 pr-1 mt-2 w-100 socials_list_container d-flex">
                        <!--list constainer div -->


                        <div class="d-flex flex-column w-100">
                            <?php
                            foreach ($socials as $single_social) {
                                if($single_social["e_name"] == $social) {
                                    $current_social_id = $single_social["social_id"];
                                    $active = "socials_item_active";
                                }else {
                                    $active = "socials_item_inactive";
                                }

                                ?>

                                <div  class="d-flex align-items-center pt-3 pb-3 mt-2 rtl  social_item <?php echo $active?>"
                                     data-id=<?php echo $single_social["social_id"] ?>>
                                    <img class="mr-3"
                                         src="<?php echo IMG_URL . '/social_cat/' . $single_social['icon'] ?>"
                                         width="15px" height="15px">
                                    <span class="mr-2  text-medium"><?php echo "ثبت " . $single_social["prefix"] . " " . $single_social["p_name"] ?></span>
                                </div>

                                <?php
                            }

                            ?>
                        </div>
                    </div> <!--list container div-->

                    <div class="wrap-input100 d-none  w-100 mt-2" id="page_enterance_div"> <!--page entrance div-->
                        <input class="input100" type="text" id="et_page" placeholder="Telegram ID">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
							<i class="fa fa-id-card" aria-hidden="true"></i>
						</span>

                    </div> <!--page enterance div -->


                    <div class="align-self-center d-none align-items-center mt-1 mb-1" id="rules-div">
                        <span><a target="_blank" href="<?php echo BASE_URL . 'social-rules'?>">قوانین ثبت</a> را مطالعه و قبول میکنم </span>
                        <input id="rules-checkbox" type="checkbox" class="ml-2" name="vehicle1" value="Bike">
                    </div>



                    <div class="d-none flex-column rtl w-100 sumbit_form register" id="final_step">
                        <!--page register div-->

                        <span class="text-center text-bold" id="lbl_complete">  تکمیل اطلاعات کانال </span>


                        <div class="d-flex  page_info mt-2">
                            <span class="small-font  text-center w-50 page_info" id="tv_id"> poushka <i class="fa fa-id-card"></i> </span>
                            <span class="small-font  text-center w-50 page_info " id="tv_members"> <i class="fa fa-users"></i> دنبال کننده 253564  </span>
                        </div>


                        <span id="lbl_page_picture" class="text-right  small-font align-self-start mt-4">  تصویر کانال </span>


                        <div class="image-upload align-self-start mt-1 mr-1">
                                <!--     <label for="img-input">
                                        </label>-->
                            <img src="assets/gallery.png" class="fit-cover" id="img_page" width="110" height="110""/>
                            <input id="img-input" type="file" accept="image/*"
                                   data-type='image'/>
                        </div>




                        <span class="d-none  text-right text-danger small-font mr-1" id="lbl_error_pic">تصویر انتخاب نشده است ! </span>




                        <div class="d-flex flex-column text-right rtl mt-3 btn_category">
                            <span id="lbl_category">دسته بندی </span>
                            <button class="d-flex align-items-center pl-1 pr-1 pt-2 pb-2 mt-1 w-100 small-font"
                                    id="btn_category">
                                انتخاب دسته بندی <i class="mr-auto fa fa-arrow-left"></i></button>
                            <span class="d-none text-right text-danger small-font mt-1 mr-1" id="lbl_error_cat">دسته بندی انتخاب نشده است ! </span>
                        </div>


                        <div class="d-flex flex-column  rtl mt-4 w-100">
                            <!-- <i class="fa fa-not-equal text-gray "></i>-->
                            <span id="lbl_name" class="text-right">نام کانال (فقط فارسی)</span>
                            <input type="text" maxlength="60" placeholder="نام کانال" id="et_page_name"
                                   class="form-control w-100  ">
                            <span class="d-none text-right text-danger small-font mt-1 mr-1" id="lbl_error_name">نام نمیتواند خالی بماند ! </span>
                        </div>

                        <span class="small mt-4 text-right">توضیحات مختصر خود را حداکثر در 60 حرف وارد نموده و تلاش کنید از کلمات کلیدی و چشمگیر استفاده نمایید</span>
                        <div class="d-flex flex-column rtl mt-1 w-100">
                            <input maxlength="60" type="text" placeholder="توضیحات مختصر " id="et_page_short"
                                   class="form-control w-100">
                            <span class="d-none text-right text-danger small-font mt-1 mr-1" id="lbl_error_shor_des">توضیحات مختصر باید دارای حداقل 15 حرف باشد</span>
                        </div>


                        <span id="lbl_des"
                              class="small mt-4 text-right">به صورت کامل شرح فعالت کانال خود را وارد نمایید</span>
                        <div class="d-flex align-items-center rtl mt-1 w-100">
                            <textarea class="form-control  small-font w-100"  rows="8" id="et_description"
                                      placeholder="توضیحات کامل"></textarea>
                        </div>


                        <span id="lbl_tags"
                              class="small mt-4 text-right">برچسب مورد نظر خود را وارد نمایید</span>
                        <div class="d-flex align-items-center rtl mt-1 w-100">
                            <input maxlength="60" type="text" placeholder="برچسب" id="et_tags"
                                   class="form-control w-100">
                        </div>

                        <ul class="rtl text-right  p-0 tags-ul d-none" id="ul_tags">
                            <?php foreach ($tags as $tag) {
                                ?>
                                <li data-id="<?php echo $tag["tag_id"] ?>" id="<?php echo $tag["tag_id"] ?>"

                                    class="align-items-center pt-2 pb-2 pr-1 li-tag "><?php echo $tag["name"] ?>
                               
                                </li>

                                <?php
                            } ?>
                        </ul>

                        <div class="d-flex flex-wrap rtl text-right small-font mt-1 tags-div">
                            <!--div to add new tags -->

                        </div> <!--div to add new tags -->


                        <span id="lbl_screends"
                              class="small mt-4 text-right">انتخاب اسکرین شات از پست های موجود  </span>

                        <div class="image-upload align-self-start mt-2 d-flex w-100 flex-wrap " id="screen-imgs">

                            <div class="screen_div mr-1 mt-1" id="screen_1" data-state="1">
                                <img class="img-not-selected"
                                     src="assets/screen.svg" width="100%" height="100%"/>

                                <div class="d-none align-items-center justify-content-center">
                                    <span class="spinner-border text-white text-center spinner-border mr-1">
                                    </span>
                                </div>
                                    <i class="d-none trash-bg small-font fa fa-trash text-white text-black">
                                </i>
                            </div>

                            <input id="screen-input" multiple="multiple" type="file"  accept="image/*"
                                   data-type='image'/>
                        </div>

                        <span class="small mt-4 text-right">در صورت داشتن ویدیو تبلیغاتی لینک آپارات ویدیو را وارد نمایید <br /> مثال : https://www.aparat.com/v/bEu3w </span>
                        <div class="d-flex align-items-center rtl mt-2 w-100">
                            <input maxlength="60" type="text" placeholder="لینک ویدیو آپارات " id="et_aparat"
                                   class="form-control w-100  ">
                        </div>

                        <div class="d-none flex-column mt-3 " id="success-newpage">
                         <span class="text-center flex-grow-1"><i class="fa fa-badge-check"></i></span>
                            <a  href="<?php echo BASE_URL."dashbord/social"?>"> مشاهده<i class="fa fa-eye mr-2"></i> </a>
                        </div>


                    </div> <!--page register div-->


                    <span class="text-center new_error w-100 small-font d-none pt-1 pb-1 mt-1" id="new_error"> <!--error div-->

                    </span> <!--error div-->


                    <button class="mt-3 btn_next_active  pt-3 pb-3 d-flex w-100 align-items-center justify-content-center"
                            type="button" id="btn_next">
                        <span class="spinner-border spinner-border-sm mr-1 d-none"></span>
                        <i class="fa fa-arrow-circle-left mr-1"></i>
                        مرحله بعد
                    </button>

                </div> <!--register col-->


            </div>




        </div>

<?php include '../footer.php'?>




<div class="modal fade" id="cats_modal_new">

    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">

            <div class="modal-header d-flex justify-content-between align-items-center">


                <button type="button" class="myclose  mr-sm-1" data-dismiss="modal">&times;</button>
                <h5> دسته بندی ها</h5>

            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="container p-0">

                    <div class="row p-0 text-right rtl">

                        <?php
                        $response = $client->request('GET', 'categories');
                        $categories = json_decode($response->getBody(), true);
                        foreach ($categories as $single) {
                            $catname = $single["name"];
                            $cat_id = $single["cat_id"];
                            $icon = $single["icon"];
                            $img_address = IMG_URL . 'cat/' . $icon;
                            echo " 
                       <div class='col-6 mt-3 d-flex align-items-center category pointer nc' data-id='$cat_id' data-text='$catname'>
                        <div>
                          <a  class='category_selected' data-id='$cat_id'>
                        <img src='$img_address' width='45px' height='45px'>
                          </a>
                        </div>
                        <div class='mr-1'>
                         <a class='category_selected' data-id='$cat_id'>$catname</a>
                         </div>
                       
                   </div>";
                        }
                        ?>

                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer d-flex">
                <button type="button" class="btn btn-danger" data-dismiss="modal">بستن</button>
            </div>

        </div>
    </div>


</div> <!--modal_categories-->


<script src="js/config.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<!--<script src="js/bootstrap.min.js"></script>-->
<script src="js/script.js"></script>
<script src="js/register.js"></script>


<script>





    $(document).ready(function () {

        var is_login = <?php
               if ((isset($_SESSION[LOGIN]) && $_SESSION[LOGIN] === true)) {echo "true"; }else { echo "false";  }?>;
        //show login dialog if the user is not login
        if (!is_login) {
              //if it is not login we show back button and take him back
            $('#close_dialog').html('<i class="fas font-size-24 fa-arrow-left"></i>').click(function (){

                if (document.referrer != "") {

                    window.history.back()
                }else {
                    location.href=web_url
                }





            })
            $('#lbl_should_login').removeClass('d-none');
            $('#login_modal').modal({backdrop: 'static', keyboard: false})
            $('#login_modal').modal('show');

        }



        var social_id = <?php echo $current_social_id ?>;
        var cat_id = 0;
        var socials = JSON.parse(<?php echo "'" . json_encode($socials) . "'"?>);
        var level = 1;
        var page_id = "";
        var myFormData = new FormData();
        var members = 0;
        var tags = [];
        var screen_numbers = 0;  // to store number of current screens for limit it to 6
        var screen_i = 1; //to send the id of current screen upload to hide loading when it is finished
        var screen_id;
        var screen_ids = [];
        var current_social ;
        $.each(socials,function (index,value) {
            if (social_id==value.social_id){
                current_social=value;
            }

        })


        $('#new_page').removeClass('d-none').addClass('d-flex').css('visibility','hidden');


        $("#screen-imgs").on('click', '.screen_div', function () {

            if ($(this).attr('data-state') === "1") {
                $('#screen-input').trigger('click')

            } else {
                // to check if we have img avilable to upload new pic
                var has_upload_image = false;
                var index = screen_ids.indexOf(parseInt($(this).attr("data-screen")));
                if (index !== -1) screen_ids.splice(index, 1);
                $(this).remove();
                screen_numbers--;
                //here we check if we have add list one image to upload
                $("#screen-imgs > div").each(function () {
                    if ($(this).attr('data-state')==1)  {
                        has_upload_image =true;
                    }
                });


             //   .if we dont have any aviable img we add more
                if (!has_upload_image) {
                    screen_i++;
                  //  screen_numbers++;
                    $('#screen-imgs').append("<div class=\"screen_div mr-1 mt-1\"  id=\"screen_" + screen_i + "\" data-state=\"1\" >\n" +
                        " <img class=\"img-not-selected\"\n" +
                        "src=\"assets/screen.svg\" width=\"100%\"  height=\"100%\"/>\n" +
                        "\n" +
                        "<div class=\"d-none align-items-center justify-content-center\">\n" +
                        "<span class=\"spinner-border text-white text-center spinner-border mr-1\">\n" +
                        "</span>\n" +
                        "</div>\n" +
                        "\n" +
                        "<i class=\"d-none trash-bg small-font fa fa-trash text-white text-black\">\n" +
                        "</i>\n" +
                        "</div>")

                }

            }
        })

        // $("#screen-input").change(function () {
        //
        //     readURLS(this)
        // });

        $("#screen-input").on("change paste keyup", function() {
            readURLS(this)
        });


        //handle screen images selected
        function readURLS(input) {

            var ok_size = false;
            var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
            for (var i = 0; i < input.files.length; i++) {
                var file = input.files[i];
                if (input.files && file) {


                    if ($.inArray(input.files[i]['type'], validImageTypes) > 0) {
                        // console.log(input.files[i])
                        if (screen_numbers < 6) {

                            // invalid file type code goes here
                       //     var reader = new FileReader();
                            // reader.onload = function (e) {
                            //
                            //     var image = new Image();
                            //     image.src = reader.result;
                                new Compressor(file, {
                                    quality: .8,
                                    maxWidth : 1200,
                                    maxHeight :1200,
                                    convertSize:50000,
                                    success(result) {

                                        $('#screen-imgs .screen_div img:last').attr('src', window.URL.createObjectURL(result))
                                            .removeClass('img-not-selected');
                                        $('#screen-imgs .screen_div').addClass('screen_hover')
                                        $('#screen-imgs .screen_div div:last').removeClass("d-none").addClass("d-flex")
                                        $('#screen-imgs .screen_div i:last').removeClass("d-none").css("padding", "#66434343")
                                        $('#screen-imgs .screen_div:last').attr('data-state', "2")
                                        screen_id = $('#screen-imgs .screen_div:last').attr("id")
                                        screen_numbers++;
                                        screen_i++;
                                        if (screen_numbers < 6) {
                                            $('#screen-imgs').append("<div class=\"screen_div mr-1 mt-1\"  id=\"screen_" + screen_i + "\" data-state=\"1\" >\n" +
                                                " <img class=\"img-not-selected\"\n" +
                                                "src=\"assets/screen.svg\" width=\"100%\"  height=\"100%\"/>\n" +
                                                "\n" +
                                                "<div class=\"d-none align-items-center justify-content-center\">\n" +
                                                "<span class=\"spinner-border text-white text-center spinner-border mr-1\">\n" +
                                                "</span>\n" +
                                                "</div>\n" +
                                                "\n" +
                                                "<i class=\"d-none trash-bg small-font fa fa-trash text-white text-black\">\n" +
                                                "</i>\n" +
                                                "</div>")
                                        }

                                        var screen_data = new FormData();
                                        screen_data.append('pic', result, result.name)
                                        screen_data.append('name', screen_id)
                                        $.ajax({
                                            type: "post",
                                            processData: false, // important
                                            contentType: false, // important
                                            dataType: 'json',
                                            url: baseURl + "screenUpload",
                                            data: screen_data,

                                            oncomplete: function () {
                                                showProgress(false, "پایات")
                                            },
                                            success: function (result, status, xhr) {
                                                console.log(JSON.stringify(result))
                                                $("#" + result["name"] + " div").removeClass("d-flex").addClass("d-none")
                                                $("#" + result["name"]).attr('data-screen', result["message"])
                                                screen_ids.push(result["message"])

                                            },

                                            error: function (xhr, status, error) {
                                                // showProgress(false, "برسی " + socials[social_id - 1]["prefix"])
                                                console.log(xhr.responseText);
                                                var json = JSON.parse(xhr.responseText);
                                            //    showError(true, json["message"])
                                            }
                                        })

                                        myFormData.append('screen[]', result, result.name)


                                        //    Send the compressed image file to server with XMLHttpRequest.
                                    },
                                    error(e) {
                                        console.log(e.message);
                                    },
                                });


                                // image.onload = function(e) {
                                //    if (image.width > 300 && image.height > 300) {
                                //
                                //
                                //
                                //
                                //
                                //    }
                                //
                                //    else {
                                //
                                //        swal({
                                //            title: "! خطا",
                                //            text: "حداقل ارتفاع و عرض تصویر انتخاب شده باید 300 پیکسل باشد ",
                                //            icon: "warning",
                                //            button: "بسیار خوب ",
                                //        });
                                //    }
                                //
                                //
                                // }



                    //        }

                      //      reader.readAsDataURL(file);


                        }

                    } else {
                        swal({
                            title: "خطا !",
                            text: "فرمت انتخاب شده مجاز نمیباشد لطفا یک تصویر انتخاب نمایید",
                            icon: "warning",
                            button: "بسیار خوب ",
                        });
                    }


                }
            }


        }


        $('.social_item').click(function () {

            $('.social_item').removeClass('socials_item_active').addClass('socials_item_inactive');
            $(this).addClass('socials_item_active');
            showError(false, "");
            social_id = $(this).attr('data-id');
            $.each(socials,function (index,value) {
                 if (social_id==value.social_id){
                     current_social=value;
                     $('#newpage-heaer').text('ثبت رایگان '+current_social.prefix+ " "+current_social.p_name)
                 }

            })

            
        })



        $('#btn_next').click(function () {
      
                if (level == 1) {
                    $('.socials_list_container').slideUp(300, function () {
                        window.scrollTo({ top: 0, behavior: 'smooth' });
                        $('.socials_list_container').removeClass('d-flex').addClass('d-none');
                        $('#page_enterance_div').removeClass('d-none').addClass('d-flex').hide().slideDown(300);
                        $('#tv_message_new').html("آی دی " + current_social.prefix+ " " + current_social.p_name
                            + " خود را وارد نمایید " + "<br /> ")
                        $('#et_page').attr('placeholder', current_social.e_name + " ID")
                        $('#btn_next').html("<i class=\"fa fa-barcode mr-1\"></i>  بررسی  \n" +
                            current_social.prefix)
                        level = 2;


                    })
                } else if (level == 2) {
                    page_id = $('#et_page').val();
                    if (page_id.length == 0) {
                        showError(true, "لطفا آی دی " + current_social.prefix + " را وارد نمایید ")
                    } else {
                        showProgress(true, "در حال بررسی")
                        showError(false, "")
                        $.ajax({

                            type: "post",
                            url: baseURl + "estelam/" + social_id,
                            data: {
                                'id': page_id,
                                'prefix': current_social.prefix
                            },
                            oncomplete: function () {

                            },
                            success: function (result, status, xhr) {

                                level = 3;
                                members = result;
                                showProgress(false, "مرحله بعد ")
                                $('#rules-div').removeClass('d-none').addClass('d-flex');
                                $('#tv_message_new').html("<i class='fa fa-id-card ml-2'></i>" + "آی دی  " + current_social.prefix + " : " + page_id + "<br />"
                                    + "<i class='fa fa-users ml-2'></i>" + current_social.member_prefix   + " : " + result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                                ).css({
                                    color: "rgba(60,60,61,0.92)",

                                }).removeClass("text-center").addClass("text-justify")

                            },

                            error: function (xhr, status, error) {
                                showProgress(false, "برسی " + current_social.prefix,"fa fa-barcode")
                                var json = JSON.parse(xhr.responseText);
                                showError(true, json["message"])
                            }


                        })
                    }
                } else if (level == 3) {
                    var rule_state =  $('#rules-checkbox').is(":checked")
                    if (rule_state) {
                        window.scrollTo({ top: 0, behavior: 'smooth' });
                        $('#rules-div').removeClass('d-flex').addClass('d-none');
                        $('#tv_message_new').addClass("d-none");
                        $('#newpage-heaer').addClass("d-none")
                        $('#page_enterance_div').slideUp(300, function () {
                            $('#page_enterance_div').removeClass('d-flex').addClass('d-none');
                            $('#final_step').removeClass('d-none').addClass('d-flex').hide().slideDown(300);
                            $('#tv_members').html("<i class='fa fa-users'> </i>" + " "+   members.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " " + current_social.member_prefix )
                            $('#tv_id').html(" <i class='fa fa-id-card'></i> " + " " + page_id ) ;
                            $('#et_page_name').attr('placeholder','نام '+ current_social.prefix)
                            $('#lbl_complete').html("تکمیل اطلاعات " + current_social.prefix)
                            $('#lbl_page_picture').text(" تصویر "+ current_social.prefix)
                            $('#lbl_name').text('نام ' + current_social.prefix + " ( فقط فارسی ) ")
                            $('#lbl_des').text("شرح فعالیت "+ current_social.prefix + " خود را وارد نمایید")
                            $('#lbl_tags').text("برچسب های مرتبط با "+ current_social.prefix + " خود را انتخاب نمایید انتخاب برچسب های صحیح به شما در افزایش بازدید کمک خواهد نمود(حداکثر 10 برچسب)")
                            $('#lbl_screends').text("تصاویر مرتبط با نمونه پست های " + current_social.prefix + " را وارد نمایید (حداکثر 6 تصویر)")

                            $('#btn_next').html("<i class=\"fa fa-check mr-1\"></i>  ثبت  \n" +
                                socials[social_id - 1]['prefix'])
                            level = 4;


                        })
                    } else {
                        swal({
                            title: "خطا !",
                            text: "ابتدا باید قوانین ثبت صفحه و کانال را تایید فرمایید",
                            icon: "warning",
                            button: "بسیار خوب ",
                        });

                    }




                } else if (level == 4) {


                    var name = $('#et_page_name').val();
                     //hide error lable if t
                    $('#et_page_name').on('keyup',function(){
                        if($(this).val().length > 0) {
                            $('#lbl_error_name').addClass("d-none");
                        }
                    });

                    var short_des = $('#et_page_short').val();
                    $('#et_page_short').on('keyup',function(){
                        if($(this).val().length > 14) {
                            $('#lbl_error_shor_des').addClass("d-none");
                        }
                    });

                    var des = $('#et_description').val();
                    var prefix = socials[social_id - 1]["prefix"];
                    var aparat = $('#et_aparat').val();
                    var apikey = "<?php echo isset($_SESSION['apikey']) ? $_SESSION['apikey'] : ''; ?>";

                    if (final_check(name, short_des)) {
                        var yourObject = {
                            id: page_id,
                            name: name,
                            short_des: short_des,
                            des: des,
                            prefix: prefix,
                            cat_id: cat_id,
                            aparat: aparat
                        };
                        myFormData.append('object', JSON.stringify(yourObject));
                        myFormData.append('tags', JSON.stringify(tags))
                        myFormData.append('screens', JSON.stringify(screen_ids))

                        showProgress(true, "در حال ثبت " + socials[social_id - 1]["prefix"])
                        showError(false, "")

                        $.ajax({
                            type: "post",
                            processData: false, // important
                            contentType: false, // important
                            dataType: 'json',
                            url: baseURl + "makePage/" + social_id,
                            data: myFormData,
                            headers: {
                                "Authorization": apikey
                            },
                            oncomplete: function () {
                                showProgress(false, "پایان")
                            },
                            uploadProgress: function (event, position, total, percentageComplete) {
                                $('.progress-bar').animate({
                                    width: percentageComplete + '%'
                                }, {
                                    duration: 1000
                                });
                            },

                            success: function (result, status, xhr) {
                                 $('#btn_next').removeClass('d-flex').addClass('d-none');
                                 $('#success-newpage').removeClass('d-none').addClass('d-flex').find('span').html("<i class='fa fa-badge-check ml-2'></i>"+  result["message"]);
                            },

                            error: function (xhr, status, error) {
                                var json = JSON.parse(xhr.responseText);
                                showError(true, json["message"])
                                showProgress(false,"ذخیره " + socials[social_id - 1]["prefix"],"fa fa-check")
                            }


                        })
                    }


                }



        });


        //open category modal
        $('#btn_category').click(function () {
            $('#cats_modal_new').modal('show');
        });

        //handle click on category
        $('.nc').click(function () {
            var cat_name = $(this).attr("data-text");
            cat_id = $(this).attr("data-id");
            $('#btn_category').html(cat_name + " <i class=\"mr-auto fa fa-arrow-circle-left\"></i>")
                .css('borderColor', '#53a929');
            $('#cats_modal_new').modal('hide');
            $('#lbl_error_cat').addClass("d-none");

        })

        //to handle show tag list  on click events of
        $('#et_tags').focus(function () {
            $('.tags-ul').removeClass("d-none")
        }).blur(function () {
            setTimeout(function () {
                $('.tags-ul').addClass("d-none")
            }, 200)

        }).keyup(function () {
            var value = $(this).val();
            $('.tags-ul').removeClass("d-none")
            $("#ul_tags > li").each(function () {
                if ($(this).text().search(value) > -1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });

        //click event for li in tags
        $('.tags-ul li').click(function () {
            var tag = $(this).html();
            var tag_id = $(this).attr('data-id')

            //first check if it has no selected class so we select it and add its id to our array an append span to selected tags
            if (!$(this).hasClass('li-tag-selected')) {

                if (tags.length>9) {
                    swal({
                        title: "خطا !",
                        text: "حداکثر 10 برچسب قابل انتخاب است",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }else {
                    $(this).addClass('li-tag-selected')
                    tags.push(tag_id)
                    $('.tags-div').append("<span data-id=" + tag_id + " id=" + tag_id + " class=\"d-flex inline align-items-center justify-content-center selected-tag \">" + tag + "<i class=\"fa fa-times-circle mr-2\"></i></span>")
                    $('#et_tags').val('')
                    $('#ul_tags > li').each(function () {
                        $(this).show()
                    })
                }



            } else {
                //else we unselect it and remove from our array and remove span from selected tags
                $(this).removeClass('li-tag-selected')
                var index = tags.indexOf(tag_id);
                if (index !== -1) tags.splice(index, 1);
                $('.tags-div').find("#" + tag_id).remove();
            }


        })




        //click event for  selected tags and we should use this way becuase they are added dynamically
        $('.tags-div').on('click', '.selected-tag', function () {
            var tag_id = $(this).attr('data-id')
            $('.tags-ul').find("#" + tag_id).removeClass("li-tag-selected")
            var index = tags.indexOf($(this).attr('data-id'));
            if (index !== -1)
                tags.splice(index, 1);
            $(this).remove();

        })


        //open file choser when image is clicked
        $('#img_page').click(function () {
            $('#img-input').trigger('click');
        })

        //handling what happens after img-input is selected
        $("#img-input").change(function () {
            //  var formData = new FormData(this);
            readURL(this);
        });

        function readURL(input) {

            if (input.files && input.files[0]) {
                var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(input.files[0]['type'], validImageTypes) > 0) {
                    // invalid file type code goes here.


                    // var reader = new FileReader();
                    //
                    // reader.onload = function (e) {
                    //     $('#img_page').attr('src', e.target.result);
                    //
                    // }
                    //
                    //
                    //
                    //
                    //
                    // reader.readAsDataURL(input.files[0]);
                    // form_data = input.files[0];



                    //  myFormData.append('pic', input.files[0])

                    new Compressor(input.files[0], {
                        quality: .8,
                        maxWidth : 1200,
                        maxHeight :1200,
                        convertSize : 50000,
                        success(result) {
                            $('#img_page').attr('src', window.URL.createObjectURL(result));
                             myFormData.append('pic', result, result.name)
                             $('#lbl_error_pic').addClass("d-none")

                            // Send the compressed image file to server with XMLHttpRequest.
                        },
                        error(e) {
                            console.log(e.message);
                        },
                    });


                } else {
                    swal({
                        title: "خطا !",
                        text: "فرمت انتخاب شده مجاز نمیباشد لطفا یک تصویر انتخاب نمایید",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }


            }
        }




        function showProgress(show, message, icon) {
            if (show) {
                $('#btn_next').html("<span class=\"spinner-border spinner-border-sm mr-1 \"></span>\n" +
                    "                        <i class=\"fa fa-arrow-circle-left d-none mr-1\"></i>\n" +
                    message +
                    "                    </button>\n")
            } else {
                $('#btn_next').html("<span class=\"spinner-border spinner-border-md mr-1 d-none\"></span>\n" +
                    "                        <i class=\""+icon+"   mr-1\"></i>\n" +
                    message +
                    "                    </button>\n")
            }
        }

        function showError(show, meessage) {
            if (show) {
                $('#new_error').removeClass('d-none').addClass('d-block').html(meessage)
            } else {
                $('#new_error').removeClass('d-block').addClass('d-none').html(meessage)
                /*     $('#new_error').slideUp(200, function () {
                         $('#new_error').removeClass('d-block').addClass('d-none')
                     });*/
            }
        }

        function final_check(name, short_des) {

            var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
            var passed = true;
            var scrolled = false ;

            if ($("#img-input").val() == "" || $.inArray($("#img-input").prop('files')[0]["type"], validImageTypes) < 0) {
                passed = false;
                $('#lbl_error_pic').removeClass("d-none")
                scrollToElement("#img_page")
                scrolled=true
            }else {
                $('#lbl_error_pic').addClass("d-none")
            }

            if (cat_id === 0) {
                passed = false;
                $('#lbl_error_cat').removeClass("d-none");
                if (!scrolled) {
                    scrolled=true
                    scrollToElement("#btn_category")
                }

            }else {
                $('#lbl_error_cat').addClass("d-none");
            }

            if ((jQuery.trim(name)).length == 0) {
                passed = false;
                $('#lbl_error_name').removeClass("d-none");
                if (!scrolled) {
                    scrolled=true
                    scrollToElement("#et_page_name")
                }
            }else {
                $('#lbl_error_name').addClass("d-none");
            }

            var p = /^[\u0600-\u06FF0-9\s]+$/;
            if (!p.test(name)) {
                passed = false;
                $('#lbl_error_name').removeClass("d-none").text('نام فقط باید به صورت فارسی وارد شود');
                if (!scrolled) {
                    scrolled=true
                    scrollToElement("#et_page_name")
                }
            }else {
                $('#lbl_error_name').addClass("d-none");
            }



            if (short_des.length < 15) {
                passed = false;
                $('#lbl_error_shor_des').removeClass("d-none");
                if (!scrolled) {
                    scrolled=true
                    scrollToElement("#et_page_short")
                }
            }else {
                $('#lbl_error_shor_des').addClass("d-none");
            }

            return passed;

        }

        function scrollToElement(id) {
            $('html, body').animate({
                scrollTop: $(id).offset().top-70
            }, 600);
        }

    })
</script>
</body>
</html>

