<?php
require __DIR__.'/../helper/init.php';

$user = json_decode($_SESSION["user"],true);
if (!isset($_SESSION["login"])) {
    header('Location:'.BASE_URL);
}

$response = $client->request('GET', 'getDashbord'    ,[
    'headers' =>[
        'Authorization' => $user['apikey']
    ],
]);
$dashbord = json_decode($response->getBody(), true);


$current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$current = substr($current_url, strrpos($current_url, '/') + 1);

$current_fa = "داشبورد" ;
      if ($current==""){
          $current_fa= "داشبورد";
      }else if ($current=="coin") {
          $current_fa= "خرید سکه";
      }else if ($current=="inbox") {
          $current_fa= "صندوق پیام";
      }else if($current=="profile") {
          $current_fa= "حساب کاربری";
      }else if ($current=="social") {
          $current_fa="صفحات و کانال ها";
      }else if ($current=="specials") {
          $current_fa="ویژه شده ها";
      }else if ($current=="support") {
          $current_fa="پشتیبانی";
      }

?>

<!DOCTYPE html>
<html>
<head>
    <base href=<?php echo BASE_URL?>>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>پنل مدیریت |  <?php echo $current_fa?> </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <!-- Ionicons -->

    <link rel="stylesheet" href="awsome/css/all.min.css">


    <!-- Theme style -->
    <link rel="stylesheet" href="dashbord/dist/css/adminlte.min.css">

    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- iCheck -->
    <!-- bootstrap rtl -->
    <link rel="stylesheet" href="dashbord/dist/css/bootstrap-rtl.min.css">
    <!-- template rtl version -->
    <link rel="stylesheet" href="dashbord/dist/css/custom-style.css">

    <link rel="stylesheet" href="css/style.css">



    <script src="js/config.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



</head>

<body class="hold-transition sidebar-mini" style="overflow-x: hidden">

<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand  navbar-light light-gray border-bottom"
         style="position: -webkit-sticky; position: sticky ;  top: 0">
        <!-- Left navbar links -->
        <ul class="navbar-nav d-flex pr-0">

            <li class="nav-item ">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>


            <li class="nav-item ">
                <a href="<?php echo BASE_URL?>" class="nav-link"> صفحه اصلی </a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">تماس</a>
            </li>

            <li class="nav-item mt-2 register-position">
                <a class="bg_new_page brand-background ml-md-2" href="<?php echo BASE_URL . "new/Instagram" ?>">ثبت صفحه یا کانال</a>
            </li>



        </ul>


    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">

        <div class="sidebar" style="direction: ltr">
            <div style="direction: rtl">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel  mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img id="img_top" class="img-circle elevation-2" alt="User Image"
                             src=<?php echo $user["thumb"]==null? BASE_URL."assets/profile.svg" : IMG_URL. 'profile/'.$user["thumb"]?>>
                    </div>
                    <div >
                        <span  class="d-block mt-2 text-white"><?php echo empty($user["username"]) ? "بدون نام کاربری "  : htmlspecialchars($user["username"]) ?></span>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column pr-0" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                             with font-awesome or any other icon font library -->


                        <li class="nav-item ">
                            <a href="dashbord" class="nav-link <?php if ($current=="") {echo "active";} ?>">
                                <i class="nav-icon  fas fa-tachometer-alt"></i>
                                <p>
                                    داشبورد
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="dashbord/social" id="social-menu" class="nav-link <?php if ($current=="social") {echo "active";} ?>">
                                <i class="nav-icon fa fa-chart-network"></i>
                                <p>
                                    صفحات و کانال ها
                                </p>
                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="dashbord/specials" class="nav-link <?php if ($current=="specials") {echo "active";} ?>">
                                <i class="nav-icon fal fa-star-shooting"></i>
                                <p>
                                    ویژه شده ها
                                </p>
                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="dashbord/comments" id="comment_menu" class="nav-link <?php if ($current=="comments") {echo "active";} ?>">
                                <i class="nav-icon fal fa-comments"></i>
                                <p>
                                    نظرات
                                    <span class="right badge badge-danger"> <?php echo $dashbord["comments"]
                                            - $dashbord["read_comment"]
                                        ?> </span>
                                </p>
                            </a>
                        </li>



                        <li class="nav-item">
                            <a href="dashbord/inbox" class="nav-link <?php if ($current=="inbox") {echo "active";} ?>">
                                <i class="nav-icon fal fa-inbox"></i>
                                <p>
                                    صندوق پیام
                                    <span class="right badge badge-danger"> <?php echo $dashbord["messages"]
                                            - $dashbord["read"]
                                        ?> </span>
                                </p>
                            </a>
                        </li>









                        <li class="nav-item">
                            <a href="dashbord/coin" class="nav-link <?php if ($current=="coin") {echo "active";}?>">
                                <i class="nav-icon fa fa-coins"></i>
                                <p>
                                    افزایش موجودی سکه
                                </p>
                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="dashbord/profile" class="nav-link <?php if ($current=="profile"){echo "active";} ?>">
                                <i class="nav-icon fal fa-user"></i>
                                <p>
                                    حساب کاربری
                                </p>
                            </a>
                        </li>



                        <li class="nav-item">
                            <a href="dashbord/support" class="nav-link <?php if ($current=="support") {echo "active";} ?>">
                                <i class="nav-icon fas fa-user-headset"></i>
                                <p>ارتباط با پشتیبانی
                                </p>
                            </a>
                        </li>


                        <li class="nav-item" data-function="exit">
                            <a href="#" class="nav-link d-flex align-items-center">
                                <i class="nav-icon fa fa-sign-out "></i>
                                <p>خروج از حساب </p>
                            </a>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
        </div>
        <!-- /.sidebar -->
    </aside>