<?php

use Src\helper\Practical as MyPractical;

include 'header.php';

$response_pages = $client->request('GET', 'getPagesWithComments', [
    "headers" => [
        "Authorization" => $user["apikey"]
    ]
]);
$pages = json_decode($response_pages->getBody(), true);


?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <?php

    if (count($pages)==0) {
        echo "    <div class=\"no_message_div d-flex flex-column align-items-center justify-content-center\">
        <i class=\"text-gray fal fa-times-circle  mr-lg-5\" > </i>
        <p class=\"mt-1 mr-lg-5\">هیچ صفحه یا کانالی فعالی  یافت نشد</p>
        
    </div>";
    }
    ?>

    <div class="container-fluid d-flex flex-wrap justify-content-between  align-items-center p-xl-3 p-2">
        <h1 class="my-h1 mr-md-2">پاسخ دهی به نظرات</h1>
    </div>


    <div class="container-fluid d-flex flex-column" id="div_messages">

        <div class="row pages-container">


            <?php

            foreach ($pages as $page) {
                  $src = IMG_URL."chanel_pic/".$page["thumb"];
                 // $href=BASE_URL."social/".$page["e_name"]."/".$page["id"];




                echo " <div class=\"col-lg-2 col-md-4 col-6 pt-2 pb-2 p-2\" >
              <div id='$page[page_id]' class=\"d-flex rtl flex-column rounded page-card-comment \">

              <img src='$src' height='150'/>
              <span class='mt-3 mr-2 text-medium'>$page[name]</span>
              <span class='mt-3 mr-2'>$page[count] نظر</span>
              <a  href='dashbord/comment/$page[page_id]' class='align-self-center social btn btn_watch mt-3 mb-3'>مشاهده و پاسخ</a> 
             

         

          </div>
        </div>
        
           ";
            }

            ?>



        </div>

        <div class="mt-2  mr-md-4 ml-md-4 d-none justify-content-center align-items-center" id="loading_list"
             style="height: 60px ; background: #e2e2e2 ; border-radius: 12px">
            <span> <div class="spinner-border spinner-border-sm text-muted"></div> در حال دریافت لطفا صبر کنید </span>
        </div>


        <?php
        if (count($pages)>11) {
            echo "     <span id='load_more' class=\"mb-2 d-flex  align-self-center pointer justify-content-center align-items-center more_items_social\">
            <div class=\"spinner-border d-none spinner-border-sm mr-2\"></div>
             نمایش بیشتر <i class='fa fa-arrow-to-bottom mr-2'> </i></span>";
        }

        ?>



    </div><!-- /.container-fluid -->

</div>




<!-- jQuery -->
<script src="dashbord/plugins/jquery/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="dashbord/dist/js/exit.js"></script>
<script src="js/compress.js"></script>

<script>

    $(document).ready(function () {



    })

</script>
<!-- jQuery UI 1.11.4 -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!- AdminLTE App -->
<script src="dashbord/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

</body>
</html>
