<?php

use Src\helper\Practical as MyPractical;

include '../header.php';



$id = $_GET["id"];


try {
    $page_response = $client->request('GET', "getPage/$id", [

        "headers" => [
            "Authorization" => $user["apikey"]
        ]
    ]);

    $page = json_decode($page_response->getBody(), true);
    $page = $page["page"];


    $response_comments = $client->request('GET', "getComments/$id?last_id=0", [
        "headers" => [
            "Authorization" => $user["apikey"]
        ]
    ]);
    $comments = json_decode($response_comments->getBody(), true);
}catch (GuzzleHttp\Exception\ClientException $e) {
    $response_tttt = $e->getResponse();
    $responseBodyAsString = $response_tttt->getBody()->getContents();
    $status_code  = $response_tttt->getStatusCode();


}



?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <?php

    if (count($comments)==0) {
        echo "    <div class=\"no_message_div d-flex flex-column align-items-center justify-content-center\">
        <i class=\"text-gray fal fa-times-circle  mr-lg-5\" > </i>
        <p class=\"mt-1 mr-lg-5\">هیچ نظری ثبت نشده است</p>
        
    </div>";
    }
    ?>

    <div class="container-fluid d-flex flex-wrap justify-content-between  align-items-center p-xl-3 p-2">
        <h1 class="my-h1 mr-md-2"><?php echo "نظرات " . $page['prefix']. " " . $page['name']?> </h1>
    </div>


    <div class="container-fluid d-flex flex-column" id="div_messages">

        <div class="row pages-container mb-3">

            <?php
            foreach ($comments as $single_comment) {
                $profile = $single_comment["thumb"] == null ? BASE_URL . "assets/profile.svg" :
                    $profile = IMG_URL . "profile/" . $single_comment["thumb"];
                $replay_date = substr($single_comment["created_at"], 0, 10);

                $display_replay_btn = $single_comment["reply"] == null ? "d-flex" : "d-none";
                $display = $single_comment["reply"] == null ? "d-none" : "d-flex";
                $no_profile = "assets/comment.svg";
                $comment_text = htmlspecialchars($single_comment["text"], ENT_QUOTES, "UTF-8");
                $comment_reply = htmlspecialchars($single_comment["reply"], ENT_QUOTES, "UTF-8");
                $username = $single_comment["username"] == null ? "بدون نام " : htmlspecialchars($single_comment["username"]);
                $page_thumb = IMG_URL . "chanel_pic/".$page["thumb"];


                echo "
            <div class='parent-comment  w-100 mt-2 pl-2 pr-2 pl-md-4 pr-md-4'  data-id='$single_comment[comment_id]'>
            <div class='d-flex justify-content-start  rtl comment norma-line-height' >
                <a href='#'>
                    <img src='$profile' width='60px' height='60px' class='img-com' >
                </a>
                <div class=' w-100 d-flex flex-column'>
                    <div class='d-flex rtl'>
                        <span class='text-bold text-medium text-right'>$username</span>
                        <span class='mr-auto ml-2'>$single_comment[persian_time]</span>
                    </div>
                    <span class='text-right mr-2 mt-2 mb-3'> $comment_text </span>
                    <span   data-id='$single_comment[comment_id]' data-admin='0' class='$display_replay_btn ml-2 pointer align-self-end font-size-13  align-items-center mb-2 replay_cm btn_watch'> 
                    ارسال پاسخ  <i class='fa fa-reply mr-1'></i> </span>
               
                </div>
            </div>
            
            <div class='$display justify-content-start rtl replay mr-5 mt-2  pr-lg-5  pr-xl-5 pr-md-3 pr-sm-2 pr-1 norma-line-height' >
                <a href='#'>
                    <img src=$page_thumb width='60px' height='60px' class='img-com' >
                </a>
                <div class=' w-100 d-flex flex-column'>
                    <div class='d-flex  rtl'>
                        <span class='text-bold text-medium'>مدیر کانال </span>
                        <span class='ml-2 mr-auto'>$single_comment[persian_time_reply]</span>
                    </div>
                    <span class='text-right mr-2 mt-2'> $comment_reply </span>
                    <span  data-id='$single_comment[comment_id]' data-admin='1' class='pointer ml-2 align-self-end text-danger mt-2 '> 
       
                </div>
            </div>
            
        </div> ";
            }
            ?>



        </div>

        <div class="mt-2  mr-md-4 ml-md-4 d-none justify-content-center align-items-center" id="loading_list"
             style="height: 60px ; background: #e2e2e2 ; border-radius: 12px">
            <span> <div class="spinner-border spinner-border-sm text-muted"></div> در حال دریافت لطفا صبر کنید </span>
        </div>


        <?php
        if (count($comments)==15) {
            echo "     <span id='load_more' class=\"mb-2 mt-3 d-flex  align-self-center pointer justify-content-center align-items-center more_items_social\">
            <div class=\"spinner-border d-none spinner-border-sm mr-2\"></div>
             نمایش بیشتر <i class='fa fa-arrow-to-bottom mr-2'> </i></span>";
        }
        ?>



    </div><!-- /.container-fluid -->

</div>

<div class="modal fade" id="replay_modal">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header d-flex  justify-content-center align-items-center">
              <span class="text-medium"> ارسال پاسخ </span>
            </div>

            <!-- Modal body -->
            <div class="modal-body d-flex flex-column">
                <div class="d-flex  rtl form-group login_register">
                    <textarea class="form-control " rows="3" id="et_comment"
                              placeholder="پاسخ شما به این نظر"></textarea>
                </div>
                <span class="text-center d-none align-self-center" id="replay_message"> نظر شما ثبت شد</span>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer my_background d-flex comment-body">
                <a class="text-danger mr-3 text-bold small-font pr-2 pl-2" data-dismiss="modal"> لغو</a>
                <div class="mr-2 comment-footer" ></div>
                <a class="text-success mr-3 text-bold small-font" id="tv_send_replay">ارسال کردن </a>
                <div class="spinner-border spinner-border-sm myspinner d-none" id="comment_spiiner"></div>
            </div>

        </div>
    </div>


</div> <!--comment_modal-->




<!-- jQuery -->
<script src="dashbord/plugins/jquery/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="dashbord/dist/js/exit.js"></script>
<script src="js/compress.js"></script>
<script src="js/script.js"></script>

<script>

    $(document).ready(function () {
        $('#comment_menu').addClass('active');


        var apikey = "<?php echo $user['apikey']?>";
        var page_thumb = "<?php echo $page["thumb"]?>"



        var myFormData = new FormData();
        var page_id = <?php echo $id ?> ;
        var replay_id  = 0 ;
        var loading = false;
        var finished = false;
        var count = <?php  echo count($comments)?>;


        if (count < 5) {
            finished=true;
        }


        $('#load_more').click(function () {
            if (!loading && !finished) {
                $("#loading_list").removeClass("d-none").addClass("d-flex");
                var last_id = $('.parent-comment:last').attr("data-id")
                loading=true;
                $.ajax({
                    type: "get",
                    url: baseURl + "getComments/"+page_id,
                    headers: {
                        'Authorization': apikey

                    },
                    data: {
                        'last_id': last_id
                    },

                    complete: function () {
                        loading = false;
                        $("#loading_list").removeClass("d-flex").addClass("d-none");
                    },

                    success: function (result, status, xhr) {

                        if ( result.length < 15) {
                            $('#load_more').removeClass('d-flex').addClass('d-none');
                        }
                        $.each(result,function (index,element) {

                            var profile = element.thumb==null ? baseURl+"assets/profile.svg" : IMAGE_URL+"profile/"+element.thumb
                            var username = element.username == null ? "بدون نام " : escapeHtml(element.username)
                            var comment_text = escapeHtml(element.text)
                            var display_replay_btn = element.reply == null ? "d-flex" : "d-none"
                            var display = element.reply == null ? "d-none" : "d-flex";


                            var thumb = IMAGE_URL + "chanel_pic/"+page_thumb


                            var comment_replay  = escapeHtml(element.reply==null ? " " : element.reply)

                         var div = "<div class='parent-comment  w-100 mt-2 pl-2 pr-2 pl-md-4 pr-md-4' data-id='"+element.comment_id+"' >" +
                             "<div class='d-flex justify-content-start rtl comment norma-line-height'>" +
                             "<a href='#'>" +
                             "<img width='60' height='60' src='"+profile+"'>"+
                             "</a>" +
                             "<div class='w-100 d-flex flex-column'>" +
                             "<div class='d-flex rtl'>" +
                             "<span class='text-bold text-medium'>"+username+"</span>" +
                             "<span class='mr-auto ml-2'>"+element.persian_time+"</span>" +
                             "</div>" +
                             "<span class='text-right mr-2 mt-2 mb-2'>"+comment_text+"</span>" +
                             "<span data-id='"+element.comment_id+"' class='"+display_replay_btn+" font-size-13 ml-2 pointer align-self-end  align-items-center mb-2 replay_cm btn_watch'>" +
                              "ارسال پاسخ <i class='fa fa-reply mr-1'></i>"+
                             "</span>" +
                             "</div>" +
                             "</div>"+
                             "<div class='"+display+" justify-content-start rtl replay mr-5 mt-2  pr-lg-5  pr-xl-5 pr-md-3 pr-sm-2 pr-1 norma-line-height'>" +
                             "<a href='#'><img src='"+thumb+"' width='60px' height='60px' class='img-com'></a> " +
                             "<div class=' w-100 d-flex flex-column '>" +
                             "<div class='d-flex rtl'>" +
                             "<span class='text-bold text-medium'>مدیر کانال </span>" +
                             "<span class='ml-2 mr-auto'>"+element.persian_time_reply+"</span>" +
                             "</div>" +
                             "<span class='text-right mr-2 mt-2'> "+comment_replay+" </span>" +
                             "<span  data-id='"+element.comment_id+"' data-admin='1' class='pointer ml-2 align-self-end text-danger mt-2 '>"+
                             "</div>" +
                             "</div>"
                             "</div>"

                            $('.pages-container').append(div);



                        })

                    },


                    error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                        var json = JSON.parse(xhr.responseText);

                    }


                });
            }
        })

        $('.pages-container').on('click','.replay_cm',function () {
            replay_id = $(this).attr('data-id')
            $('#replay_modal').modal('show');

        })



        $('#replay_modal').on('hidden.bs.modal',function (){
            $('#replay_message').addClass('d-none').removeClass('text-danger text-success')
            showLoading(false)
        })

        $('#tv_send_replay').click(function (){
            var replay = $('#et_comment').val()
            if (isEmpty(replay)) {
                $('#replay_message').text('متن پاسخ نمیتواند خالی بماند').removeClass('d-none').addClass('text-danger')
            }else {
                $('#replay_message').addClass('d-none')
                showLoading(true)
                $.ajax({
                    type: "PUT",
                    url: baseURl + "replyComment/"+replay_id,
                    headers: {
                        'Authorization': apikey
                    },
                    data: {
                        'reply': replay
                    },

                    complete: function () {
                        showLoading(false)
                    },

                    success: function (result, status, xhr) {
                        $('#replay_message').removeClass('d-none').text(result["message"]).removeClass("text-danger")
                            .addClass('text-success')

                    },
                    error: function (xhr, status, error) {
                        var json = JSON.parse(xhr.responseText);
                        $('#replay_message').removeClass('d-none').text(json["message"]).removeClass('text-success').
                            addClass('text-danger')

                    }
                })



            }
        })


        function isEmpty(value) {
            return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
        }


        function showLoading(show) {
            if (show) {
                $('#tv_send_replay').text("در حال ارسال ")
                $('#comment_spiiner').removeClass('d-none');

            } else {
                $('#tv_send_replay').html("ارسال کردن")
                $('#comment_spiiner').addClass('d-none');
            }
        }




    })

</script>
<!-- jQuery UI 1.11.4 -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!- AdminLTE App -->
<script src="dashbord/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

</body>
</html>
