<?php
include 'header.php';
$response_supports = $client->request('GET', 'supportTypes');
$supports = json_decode($response_supports->getBody(), true);


?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >


    <!-- Content Header (Page header) -->

    <!--    <div class="d-flex justify-content-between align-items-center light-gray  mt-0"
             style="position: -webkit-sticky; top :0px; position: sticky;">
            <h1 class="text-dark mr-md-2 top-h1 pt-2 pb-2">افزایش سکه</h1>
            <span class="ml-2" >موجودی فعلی : 200</span>

        </div>-->

    <div class="container-fluid mt-4">





        <div class="row">
            <div class="col-lg-10 mr-auto ml-auto">
            <div class="d-flex flex-column">

                <span>گزینه پشتیبانی خود را انتخاب نمایید </span>
                <select id="select_types" class="mt-2" name="گزینه" style="padding: 7px 4px; border-radius: 4px; background: #f0eff3">

                    <?php
                    foreach ($supports as $support) {

                        echo " <option id='$support[st_id]' value=\"volvo\">$support[text]</option>";
                    }

                    ?>


                </select>


                <span class="mt-3">شماره موبایل </span>
                <div class="form-group  position-relative mt-1">
                    <div>
                        <i class="fa fa-phone position-absolute drawable_rigth_support" ></i>
                        <input type="text" value="<?php echo $user["mobile"]?>"  class="form-control pr-4 "  id="et_mobile">
                    </div>
                </div>


                <span class="mt-1"> عنوان تیکت پشیتیبانی </span>
                <div class="form-group  position-relative mt-1">
                    <div>
                        <i class="fa fa-header position-absolute drawable_rigth_support" ></i>
                        <input type="text"  class="form-control pr-4  "  id="et_title">
                    </div>
                </div>


                <span class="mt-1 mb-0"> توضیحات  </span>
                <div class="form-group   mt-1">
                    <textarea id="et_des" rows="4" class="form-control" ></textarea>
                </div>



                    <span > تصویر ضمیمه  </span>

                    <div class="d-flex flex-column image-upload">
                        <img src="assets/gallery.png" class="mt-1 pointer" id="support-img" width="135" height="135">
                        <input id="img-input" type="file" accept="image/x-png,image/gif,image/jpeg"
                               data-type='image'/>

                    </div>


                    <div id="div-message" class="d-none mt-2">ارسال درخواست با موفقیت انجام شد</div>

                    <button id="btn_save" class="btn btn-success align-self-end mt-2">ارسال درخواست </button>

            </div>





            </div>


        </div>


        <div style="height: 200px; background: #d9ddde" class="out mt-2 d-flex flex-column justify-content-center">

          <span class="text-center  mb-4">ارتباط مستقیم با پشتیبانی</span>
            <ul class="text-center" id="ul-support">
                <i><a><i class="fab fa-instagram"></i></a></i>
                <i><a><i class="fab fa-telegram"></i></a></i>
                <i><a><i class="fa fa-envelope"></i></a></i>
            </ul>

        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->

</div>

<!-- /.content -->


<!-- ./wrapper -->


<!-- jQuery -->
<script src="dashbord/plugins/jquery/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="dashbord/dist/js/exit.js"></script>
<script src="js/compress.js"></script>

<script>

    $(document).ready(function () {

        var apikey = "<?php echo $user['apikey']?>";
        var myFormData = new FormData();

        $('#btn_save').click(function () {

            var st_id = $('#select_types').find(":selected").attr('id');
            var title = $('#et_title').val();
            var des = $('#et_des').val();
            var mobile = $('#et_mobile').val();

            var yourObject = {
                st_id: st_id,
                title: title,
                message: des,
                mobile: mobile

            }
            myFormData.append('object', JSON.stringify(yourObject));
            showLoading(true)
            showError(false,false,"")
            $.ajax({
                type: "post",
                processData: false, // important
                contentType: false, // important
                dataType: 'json',
                url: baseURl + "support",
                data: myFormData,
                headers: {
                    "Authorization": apikey
                },
                oncomplete: function () {

                    showLoading(false)
                },

                success: function (result, status, xhr) {
                    showLoading(false)
                    showError(true, true, result["message"]);

                },

                error: function (xhr, status, error) {
                    console.log(xhr.responseText)
                    var json = JSON.parse(xhr.responseText);
                    showLoading(false)
                    showError(true, false, json["message"])

                }


            })


        })

        $('#support-img').click(function () {
            $('#img-input').trigger('click');
        })


        //handling what happens after img-input is selected
        $("#img-input").change(function () {
            //  var formData = new FormData(this);
            readURL(this);
        });



        function readURL(input) {
            if (input.files && input.files[0]) {
                var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(input.files[0]['type'], validImageTypes) > 0) {
                    // invalid file type code goes here.

                    new Compressor(input.files[0], {
                        quality: .8,
                        convertSize:50000,
                        success(result) {
                            $('#support-img').attr('src', window.URL.createObjectURL(result));
                            myFormData.append('pic', result, result.name)

                        },
                        error(e) {
                            console.log(e.message);
                        },
                    });


                } else {
                    swal({
                        title: "خطا !",
                        text: "فرمت انتخاب شده مجاز نمیباشد لطفا یک تصویر انتخاب نمایید",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }


            }
        }

        function isEmpty(value) {
            return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
        }



        function showLoading(show) {
            if (show) {
                $('#btn_save').html("  درحال ارسال\n" +
                    "                         <span class=\"spinner-border spinner-border-sm mr-2\"></span>\n")
            } else {
                $('#btn_save').html("ارسال درخواست")
            }
        }

        function showError(show, success, message) {
            if (show) {
                if (success) {
                    $('#div-message').removeClass('d-none').addClass('success-div').html(message)
                } else {
                    $('#div-message').removeClass('d-none').addClass('error-div').html(message)
                }

            } else {
                $('#div-message').addClass('d-none')
            }
        }


    })

</script>
<!-- jQuery UI 1.11.4 -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!- AdminLTE App -->
<script src="dashbord/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

</body>
</html>
