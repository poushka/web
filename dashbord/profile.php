<?php
include 'header.php';

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-fluid  p-xl-3 p-2">
        <h1 class="text-dark my-h1 mr-md-2">حساب کاربری</h1>

        <div class="row mt-lg-4 mt-2">
            <div class="col-md-3 col-sm-12 col-12">
                <div class="d-flex flex-column align-items-center image-upload">
                    <img src="<?php echo $user["thumb"]==null? BASE_URL."assets/profile.svg" : IMG_URL. 'profile/'.$user["thumb"] ?>" class="rounded-circle pointer" id="profile-img" width="135" height="135">
                    <input id="img-input" type="file" accept="image/*"
                           data-type='image'/>
                    <button class="btn btn-primary mt-2">انتخاب تصویر</button>
                </div>
            </div>

            <div class="col-md-9 d-flex flex-column ">
                <p class="mt-2 text-bold">اطلاعات کاربری</p>


                <div class="form-group row d-flex align-items-center">
                    <span  for="et_name" class="col-lg-2 p-0 mr-3 mr-lg-1 text-lg-left small-font col-form-label ">نام و نام خانوادگی: </span>
                    <div class="col-lg-9 ">
                        <i class="fa fa-user position-absolute drawable_rigth" ></i>
                        <input type="text" value="<?php echo $user["name"]?>"  class="form-control pr-4 "  id="et_name">
                    </div>
                </div>

                <div class="form-group row d-flex align-items-center">
                    <span  for="et_username" class="col-lg-2 text-lg-left p-0 mr-3 mr-lg-1  small-font col-form-label "> نام کاربری : </span>
                    <div class="col-lg-9 ">
                        <i class="fa fa-user position-absolute drawable_rigth" ></i>
                        <input type="text" value="<?php echo $user["username"]?>"  class="form-control pr-4"  id="et_username">
                    </div>
                </div>


                <div class="form-group row d-flex align-items-center">
                    <span  for="et_username" class="col-lg-2 mr-3 mr-lg-1  p-0 text-lg-left small-font col-form-label ">  بیوگرافی : </span>
                    <div class="col-lg-9 ">
                     <textarea class="form-control  small-font "  rows="4" id="et_bio"
                              ><?php echo $user["bio"]?></textarea>
                    </div>
                </div>



                <p class="mt-5 text-bold ">راه های ارتباطی</p>

                <div class="form-group row d-flex  align-items-center">
                    <span  for="et_name" class="col-lg-2 p-0 mr-3 mr-lg-1  text-lg-left small-font col-form-label ">  اینستاگرام : </span>
                    <div class="col-lg-9 ltr ">
                        <i class="fab fa-instagram position-absolute drawable_left"></i>
                        <input type="text"  value="<?php echo $user["instagram"]?>" class="form-control pl-4 "  id="et_instagram">
                    </div>
                </div>

                <div class="form-group row d-flex align-items-center p-0">
                    <span  for="et_username" class="col-lg-2 text-lg-left p-0 mr-3 mr-lg-1 small-font col-form-label "> تلگرام  : </span>
                    <div class="col-lg-9 ltr">
                        <i class="fab fa-telegram position-absolute drawable_left"></i>
                        <input type="text"  value="<?php echo $user["t_id"]?>" class="form-control pl-4"  id="et_telegram">
                    </div>
                </div>


                <div class="form-group row d-flex align-items-center ">
                    <span  for="et_username" class="col-lg-2 mr-3 mr-lg-1 p-0 text-lg-left small-font col-form-label ">  ایمیل : </span>
                    <div class="col-lg-9 ltr">
                        <i class="fa fa-envelope position-absolute drawable_left"></i>
                     <input type="text"  value="<?php echo $user["email"]?>" class="form-control pl-4 small-font "  id="et_email">
                    </div>
                </div>

           <div class="row">

           <div class="col-lg-2 mr-1 "></div>
               <div  class="col-lg-9 ">
                   <div id="error_div" class="btn d-none ml-1 w-100"></div>
               </div>
           </div>



                 <div class="col-lg-11 text-left mt-2 p-0">
                     <button class="btn btn-success  d-flex align-items-center justify-content-center mr-auto " id="btn_save">
                         ذخیره تغییرات
                     </button>
                    </div>

        </div>



    </div>


</div>


</div><!-- /.container-fluid -->
</div>





<script src="dashbord/plugins/jquery/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="dashbord/dist/js/exit.js"></script>
<script src="js/compress.js"></script>





<script>

    $(document).ready(function () {
        var initial = "<?php echo $user["username"] ?>";
        var apikey = "<?php echo $user['apikey']?>";
        var myFormData = new FormData();

        $('#profile-img').click(function () {
            $('#img-input').trigger('click');
        })

        $('#btn_save').click(function () {
            var username_changed = false ;
            var name =  isEmpty($('#et_name').val().toString()) ? null : $('#et_name').val()
            var username =isEmpty($('#et_username').val())  ? null : $('#et_username').val()
            var bio = isEmpty($('#et_bio').val())  ? null : $('#et_bio').val()
            var t_id = isEmpty( $('#et_telegram').val()) ? null : $('#et_telegram').val()
            var instagram = isEmpty($('#et_instagram').val()) ? null : $('#et_instagram').val()
            var email = isEmpty($('#et_email').val()) ? null :  $('#et_email').val()
            
            if (username==null) {
                username_changed=false;
            }else {
                if (initial.toUpperCase()===username.toUpperCase()) {
                    username_changed=false
                } else {
                    username_changed =true;
                }

            } 
            
                var yourObject = {
                    huc:username_changed,
                    username: username,
                    name: name,
                    email: email,
                    t_id: t_id,
                    instagram: instagram,
                    bio: bio,
                    sex: 1
                }
                myFormData.append('object', JSON.stringify(yourObject));


          showLoading(true)
       $.ajax({
                type: "post",
                processData: false, // important
                contentType: false, // important
                dataType: 'json',
                url: baseURl + "infoUpdate",
                data: myFormData,
                headers: {
                    "Authorization": apikey
                },
                complete: function () {

                    showLoading(false)
                },

                success: function (result, status, xhr) {

                    showLoading(false)
                    if (result["thumb"]!=null) {
                        $('#img_top').attr('src',IMAGE_URL+"profile/"+result["thumb"])
                    }

                    showError(true,true,"حساب کاربری شما به روز گردید")

                },

                error: function (xhr, status, error) {
                    console.log(xhr.responseText)
                    var json = JSON.parse(xhr.responseText);
                    showLoading(false)
                    showError(true,false,json["message"] )

                }


            })

        })
        //handling what happens after img-input is selected
        $("#img-input").change(function () {
            //  var formData = new FormData(this);
            readURL(this);
        });

        function isEmpty(value) {
            return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(input.files[0]['type'], validImageTypes) > 0) {
                    // invalid file type code goes here.

                    var file = input.files[0];
                    new Compressor(file, {
                        quality: .7,
                        maxWidth : 1200,
                        maxHeight :1200,
                        convertSize:50000,
                        success(result) {
                            console.log(result);
                            myFormData.append('pic', result, result.name)
                            $('#profile-img').attr('src',  window.URL.createObjectURL(result));
                            // Send the compressed image file to server with XMLHttpRequest.
                        },
                        error(e) {
                            console.log(e.message);
                        },
                    });


                } else {
                    swal({
                        title: "خطا !",
                        text: "فرمت انتخاب شده مجاز نمیباشد لطفا یک تصویر انتخاب نمایید",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }


            }
        }








        function showLoading(show) {
            if (show) {
                $('#btn_save').html("  درحال ذخیره سازی\n" +
                    "                         <span class=\"spinner-border spinner-border-sm mr-2\"></span>\n")
            } else {
                $('#btn_save').html("ذخیره تغییرات")
            }
        }

        function showError(show,success,message) {
            if (show) {
             if (success) {
                 $('#error_div').removeClass('d-none btn-danger').addClass(' btn-success').html(message)
             } else {
                 $('#error_div').removeClass('d-none btn-success').addClass(' btn-danger').html(message)
             }

            } else {
                $('#error_div').removeClass('d-none')
            }
        }






    })

</script>
<script src="dashbord/dist/js/adminlte.js"></script>


</body>
</html>
