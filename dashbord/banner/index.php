<?php

include '../header.php';


$id = $_GET["id"];


try {
    $response = $client->request('GET', 'getBanner/' . $id, [
        'headers' => [
            'Authorization' => $user['apikey']
        ],
    ]);

} catch (GuzzleHttp\Exception\ClientException $e) {
    $response_tttt = $e->getResponse();
    $responseBodyAsString = $response_tttt->getBody()->getContents();
    header('Location:' . BASE_URL . "/dashbord");
}
$banner = json_decode($response->getBody(), true);

?>


    <div class="content-wrapper">

            <h1 class='pr-2 text-bold myh1  mr-md-4 ml-md-4  pt-3 pb-3'><?php echo "ثبت بنر " .$banner["prefix"] . " " . $banner["name"]?></h1>
               <div class="item_banner d-flex justify-content-center w-100 pr-1 pl-1 pr-sm-2 pl-sm-2 pr-xl-5 pl-xl-5 ml-1 ">

               <img id="img-banner"   src="<?php echo empty($banner["banner"]) ?  "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" : IMG_URL.'banner/'.$banner["banner"]?>"
                    class="mr-xl-4 ml-xl-4 mr-md-3 ml-md-3 mr-sm-2 ml-sm-2 dashed-border pointer fit-cover" width="100%">


                   <input id="img-input" type="file" accept="image/*"
                          data-type='image'/>
                 </div>
              <div class="d-flex flex-column">
                  <span class="text-center text-bold d-none mt-1" id="banner_message"></span>

                  <div class="d-flex justify-content-center mt-1">
                      <button id="btn_banner" class="bg-banner align-self-center mt-2 "><i class="fa fa-image"></i>
                          <?php echo empty($banner["banner"]) ? "انتخاب بنر" :"به روز رسانی بنر" ?>
                      </button>
                      <button  data-enable="0" id="btn-save-banner" class="bg-save-disable d-flex align-items-center  align-self-center mt-2 mr-2"><i class="fa fa-check ml-1"></i>
                         ذخیره بنر
                          <span id="spinner-banner"  class="spinner-border spinner-border-sm mr-1 d-none"></span>
                      </button>

                  </div>


                  <div class="mr-md-5 mr-sm-2 mr-1 mt-3 ">

                  <span class="text-bold ">قوانین ثبت بنر در پوشکا</span>
                  <ul class="banner-rules" style="list-style-type: circle!important;">
                      <li style="list-style-type: circle!important;">تصویر بنر انتخاب شده باید با قوانین ثبت تصاویر در پوشکا منطبق باشد</li>
                      <li >تصویر انتخاب شده باید به صورت افقی باشد . پیشنهاد پوشکا استفاده از تصویری با عرض 900 و ارتفاع 600 پیکسل میباشد . بنر انتخاب شده در صورت کشیدگی تصویر تایید نخواهد شد </li>
                  </ul>

                  </div>

        </div>

        
    </div>








    <script src="dashbord/plugins/jquery/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="dashbord/dist/js/exit.js"></script>
    <script src="js/compress.js"></script>
    <script src="js/script.js"></script>

    <script>

        $(document).ready(function () {
            $('#social-menu').addClass('active');

            var page_id = <?php echo $id?>;
            var user_id =  <?php echo $user["user_id"]; ?>;
            var apikey = "<?php echo $user['apikey']?>";

            var myFormData = new FormData();


            $('#img-banner').click(function () {
                $('#img-input').trigger('click');
            })
            $('#btn_banner').click(function () {
                $('#img-input').trigger('click');
            })




            $("#img-input").change(function () {
                //  var formData = new FormData(this);
                readURL(this);
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                    if ($.inArray(input.files[0]['type'], validImageTypes) > 0) {
                        new Compressor(input.files[0], {
                            quality: .8,
                            maxWidth : 1300,
                            maxHeight :1200,
                            convertSize:150000,
                            success(result) {
                                console.log(result)
                                myFormData.append('banner', result, result.name)
                                console.log(myFormData)
                                $('#img-banner').attr('src', window.URL.createObjectURL(result));
                                $('#btn-save-banner').attr("data-enable",1).removeClass("bg-save-disable").addClass("bg-save");
                                // Send the compressed image file to server with XMLHttpRequest.
                            },
                            error(e) {
                                console.log(e.message);
                            },
                        });


                    } else {
                        swal({
                            title: "خطا !",
                            text: "فرمت انتخاب شده مجاز نمیباشد لطفا یک تصویر انتخاب نمایید",
                            icon: "warning",
                            button: "بسیار خوب ",
                        });
                    }


                }
            }


            $('#btn-save-banner').click(function () {
                var enable =    $(this).attr('data-enable');
                if (enable==1) {

                    $('#spinner-banner').removeClass('d-none')
                    $.ajax({
                        type: "post",
                        processData: false, // important
                        contentType: false, // important
                        dataType: 'json',
                        url: baseURl + "bannerUpdate/"+page_id,
                        data: myFormData,
                        headers: {
                            "Authorization": apikey
                        },
                        complete: function () {

                            $('#spinner-banner').addClass('d-none')
                        },

                        success: function (result, status, xhr) {

                         showMessage(true,result["message"],true,$('#banner_message'))

                        },

                        error: function (xhr, status, error) {
                            console.log(xhr.responseText)
                            var json = JSON.parse(xhr.responseText);
                            showMessage(true,json["message"],false,$('#banner_message'))

                        }


                    })

                }
            })



        })
        
    </script>

    <script src="dashbord/dist/js/adminlte.js"></script>


</body>
</html>


