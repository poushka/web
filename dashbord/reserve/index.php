<?php

include '../header.php';
use  GuzzleHttp\Client as GuzzClient;




$id = $_GET["id"];

try {
    $response = $client->request('GET', 'getSpecials/' . $id, [
        'headers' => [
            'Authorization' => $user['apikey']
        ],
    ]);

} catch (GuzzleHttp\Exception\ClientException $e) {
    $response_tttt = $e->getResponse();
    $responseBodyAsString = $response_tttt->getBody()->getContents();
    header('Location:' . BASE_URL . "/dashbord");
}

$details = json_decode($response->getBody(), true);


?>


    <div class="content-wrapper">


        <div class="container-fluid d-flex flex-column p-2">
            <h1 class='pr-2 text-bold myh1  mr-md-4 ml-md-4  pt-3 pb-3'><?php echo "ویژه کردن " . $details["prefix"] . " " . $details["name"] ?></h1>


            <div class="  ">
                <p class="gold_btn d-inline-block mr-2 mr-md-4"> موجودی سکه : <?php echo $details["inventory"] ?>
                    عدد </p>
                <a href="<?php echo BASE_URL.'dashbord/banner/'. $id?>" class="bg-banner mr-1">ثبت بنر</a>
            </div>




            <?php


            foreach ($details["specials"] as $special) {
                $reserve_time = $special["day"] * 24 . " ساعت رزرو";
                $avaiable = $special["max"] - $special["reserved"];
                $title = $special["title"];
                $reserved = $special["isReserved"] ? "<span class='reserve_back text-bold  mr-1'>رزرو شده <i class='fa fa-badge-check'></i> </span>" : "";
                $price = "پرداخت آنلاین - " . number_format($special["price"]) . " تومان";
                $gold = "پرداخت با سکه - " . $special["gold"] . " عدد";
                $has_banner = $special["hasBanner"] ? 1 : 0;




                echo "
                         <div class='mt-3 d-flex mr-md-4 ml-md-4  my_card flex-column reserve_div '>
                       <span class='text-center text-bold mt-md-1 mt-'>$special[title]</span>
               
                     <span class='mt-1 mr-2 ml-2'>$special[message]</span>
                      <div class='d-flex align-items-center flex-wrap mt-lg-3 mt-1 mr-1'>
                      <span class='text-bold '> <i class='fa fa-clock'></i>  $reserve_time</span>
                      <span class='text-bold mr-2'> <i class='fa fa-check'></i>  $avaiable   جایگاه باقی مانده   </span>
                       $reserved
              
                  
                     
                   </div>
                 
                
                   
                   <div class='d-flex flex-wrap justify-content-center justify-content-sm-start mt-2 mb-2 ltr'>
                         <span data-banner-need='$special[banner_need]'  data-have-banner='$has_banner' data-available='$avaiable'
                         data-special='$special[special_id]' data-title='$title'  data-gold='$special[price]' data-reserved='$special[isReserved]'
                         data-price='$special[price]' data-time='$special[day]'  class=' pointer text-center mt-1 pay-cash btn_reserve_bc ml-2'><i class='fas fa-shopping-cart'></i> $price</span>
                         
                         <span data-banner-need='$special[banner_need]'  data-have-banner='$has_banner' data-available='$avaiable'
                          data-special='$special[special_id]' data-title='$title'  data-gold='$special[gold]' data-reserved='$special[isReserved]'  
                          data-price='$special[price]' data-time='$special[day]'  class=' btn_reserve_bg pointer mt-1 text-center pay-coin ml-2'><i class='fa fa-coin'></i> $gold </span>
                   
              
                        </div>
                  
           </div>
                 ";
            }
            ?>

            <div class='mt-3 d-flex mr-md-4 ml-md-4  my_card flex-column reserve_div '>
                <span class='text-center text-bold mt-md-1 mt-'>رزرو عبارت جستجو</span>
                <span class='mt-1 mr-2 ml-2'> <?php echo "از طریق این جایگاه شما میتوانید عبارتی را رزرو نمایید تا " .
                        $details["prefix"] . " شما در صدر نتایج جستجوی عبارت رزرو شده به نمایش در آید" . "<br />" . "مثال (دانلود فیلم)" ?>


                </span>


                    <span class='text-bold mr-1 mt-2'> <i class='fa fa-clock'></i> 48 ساعت رزرو </span>
                   <div class="form-group   position-relative mt-2 mr-1 ">
                    <div>
                        <i class="fa fa-file-word position-absolute drawable_rigth_support text-dark"></i>
                        <input type="text" placeholder="کلمه یا عبارت مورد نظر جهت رزرو"
                               class="word-input pr-4  my_background " id="et_word">
                    </div>
                </div>

                <div class='d-flex flex-wrap justify-content-center justify-content-sm-start mt-2 ltr mb-2'>
                         <span id="btn_reserveword_bc" data-special='$special[special_id]' data-title='$title'
                               data-gold='$special[price]'
                               class='pointer mt-1 text-center pay-cash ml-2'><i
                                     class='fa fa-shopping-cart'></i> پرداخت آنلاین - 10,000 تومان </span>

                    <span id="btn_reserveword_bg" data-special='$special[special_id]' data-title='$title'
                          data-gold='$special[gold]'
                          class='pointer text-center mt-1 pay-coin ml-2'><i class='fa fa-coin'></i> پرداخت با سکه - 10 عدد </span>


                </div>


            </div> <!--word_div-->

        </div>

    </div>


    <div class="modal  fade" id="modal_rbg">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content position-relative">

                <div class="modal-header d-flex  justify-content-center align-items-center">
                    <p class="ltr mt-1 mb-1" id="tv_title"> آیا از رزرو این جایگاه مطمعن میباشید ؟</p>
                </div>

                <!-- Modal body -->
                <div class="modal-body m-0 border-none">
                    <p id="tv_gold_reserve" class="text-black text-center"></p>
                    <span class="d-flex mr-2 mt-1 text-regular  align-items-center" id="tv_price"></span>
                    <span class="d-flex mr-2 mt-1 text-regular align-items-center" id="tv_time"></span>
                    <span class="d-none mt-2 text-center" id="tv_going_bank">در حال انتقال به درگاه لطفا صبر کنید ...</span>
                </div>


                <span id="message_rbg" class="reserve_back align-self-center d-none"><i class="fa fa-badge-check"></i> جایگاه مورد نظر با موفقیت رزرو گردید</span>
                <span id="error_rgb" class="align-self-center"></span>

                <div class="modal-footer border-none ltr d-flex  p-0 footer-reserve">

                    <button  id="btn_close_bg" class="btn-cancel mr-2">لغو کردن</button>

                    <button id="btn_reserve_bg_dialog" class="d-flex  mr-4 btn-reserve align-items-center">
                        <span id="spinner_rbg" class="spinner-border spinner-border-sm mr-2 d-none"></span>
                       <i class="fa fa-badge-check mr-1"></i> رزرو جایگاه</button>
                </div>

            </div>
        </div>

    </div> <!--rbg_modal-->


<!--    <div class="modal fade" id="modal_rwbg">-->
<!--        <div class="modal-dialog modal-dialog-centered ">-->
<!--            <div class="modal-content position-relative">-->
<!---->
<!--                <div class="modal-header d-flex my_background justify-content-center align-items-center">-->
<!--                    <p class="text-bold ltr" id="tv_title_rwbg"> آیا از رزرو این جایگاه مطمعن میباشید ؟</p>-->
<!--                </div>-->
<!---->
<!--                <!-- Modal body -->-->
<!--                <div class="modal-body my_background">-->
<!--                    <span class="d-flex text-warning text-bold align-items-center" id="tv_gold_count_rwg"></span>-->
<!--                    <span class="d-flex text-warning text-bold align-items-center" id="tv_gold_count_rwg"></span>-->
<!--                </div>-->
<!---->
<!---->
<!--                <div class="modal-footer d-flex ltr">-->
<!--                    <button id="btn_close_rwbg" class="btn btn-danger">لغو کردن</button>-->
<!--                    <button id="btn_rwbg" class="btn btn-success">رزرو جایگاه</button>-->
<!--                </div>-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--    </div> <!--rbg_modal-->-->


    <div class="modal fade" id="modal_word">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content position-relative">

                <div class="modal-header d-flex justify-content-center align-items-center">
                    <span>رزرو عبارت جستجو</span>
                </div>

                <!-- Modal body -->
                <div class="modal-body d-flex flex-column mt-3 mb-3">
                    <div class="d-flex flex-column flex-wrap">
                        <p class="text-black text-center ltr" id="tv_title_wr"> آیا از رزرو لغت فیلم مطمعن میباشید ؟</p>
                        <span class="d-flex text-regular align-items-center" id="tv_price_wr"></span>
                        <span class="d-flex text-regular align-items-center mt-1" id="tv_time_wr"></span>
                        <span class="d-none mt-2 text-center" id="tv_bank_wr">در حال انتقال به درگاه لطفا صبر کنید ...</span>
                    </div>
                    <span id="message-wr_success" class="reserve_back text-center mt-2 align-self-center d-none">
                        <i class="fa fa-badge-check"></i>  عبارت مورد نظر با موفقیت رزرو گردید برای اطلاع از زمان پایان رزرو به بخش ویژه شده ها مراجعه نمایید</span>

                    <span id="message-wr_failure" class="text-danger text-center mt-2 align-self-center d-none">
                        <i class="fa fa-badge-check"></i></span>

                </div>

                <div class="modal-footer border-none ltr d-flex  p-0 footer-reserve">
                    <button id="btn_close_wr" class="btn mr-2 btn-danger">لغو کردن</button>
                    <button id="btn_wr" class="btn btn-success mr-3 d-flex align-items-center">
                    <i class="fa fa-badge-check mr-1"></i>    رزرو عبارت</button>
                </div>

            </div>
        </div>

    </div> <!--wr_modal-->



    <script src="dashbord/plugins/jquery/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="dashbord/dist/js/exit.js"></script>
    <script src="js/compress.js"></script>

    <script>

        $(document).ready(function () {
            $('#social-menu').addClass('active');
            var special_id = 1;
            var gold = 0;
            var title = "";
            var day_special;
            var page_id = <?php echo $id?>;
            var user_id =  <?php echo $user["user_id"]; ?>;
            var apikey = "<?php echo $user['apikey']?>";
            var word;
            var inventory = <?php echo $details["inventory"] ?>;
            var prefix = "<?php echo $details["prefix"]?>";
            var rbg = true ;
            var w_rbg = true;



            $('#btn_reserveword_bg').click(function () {
                word = $('#et_word').val().trim();
                if (word.toString().length < 2) {
                    swal({
                        title: "خطا !",
                        text: "طول عبارت انتخابی باید دارای حداقل 2 حرف باشد",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                } else if (inventory < 10) {
                    swal({
                        title: "خطا !",
                        text: "موجودی سکه برای رزرو عبارت کافی نمیباشد",
                        icon: "warning",
                        button: "بسیار خوب ",
                    })
                }else  {
                    $('#tv_title_wr').html("آیا از رزرو عبارت "+" <span class='text-danger'>"+word+"</span> "  + " برای نتایج جستجو مطمعن میباشید ؟")
                    $('#tv_price_wr').html("<i class='fa fa-coins ml-1'></i>  10 سکه  ").removeClass('text-success').addClass('text-warning')
                    $('#tv_time_wr').html("<i class='fa fa-clock ml-1'></i>  48 ساعت  ")
                    w_rbg=true;
                    $('#modal_word').modal('show')
                }
            })

            $('#btn_reserveword_bc').click(function () {
                word = $('#et_word').val().trim();
                if (word.toString().length < 2) {
                    swal({
                        title: "خطا !",
                        text: "طول عبارت انتخابی باید دارای حداقل 2 حرف باشد",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                } else if (inventory < 10) {
                    swal({
                        title: "خطا !",
                        text: "موجودی سکه برای رزرو عبارت کافی نمیباشد",
                        icon: "warning",
                        button: "بسیار خوب ",
                    })
                }else  {
                    $('#tv_title_wr').html("آیا از رزرو عبارت "+" <span class='text-danger'>"+word+"</span> "  + " برای نتایج جستجو مطمعن میباشید ؟")
                    $('#tv_price_wr').html("<i class='fa fa-money-bill ml-1'></i>  10000 تومان  ").removeClass('text-warning').addClass('text-success')
                    $('#tv_time_wr').html("<i class='fa fa-clock ml-1'></i>  48 ساعت  ")
                    w_rbg=false;
                    $('#modal_word').modal('show')
                }

            })
            $('#btn_close_wr').click(function () {
                $('#modal_word').modal('hide')
            })
            $('#btn_wr').click(function () {
                if (w_rbg) {
                    $('#btn_wr').html("<span class=\"spinner-border spinner-border-sm mr-2\"></span>\n" +
                        "در حال رزرو")
                    $.ajax({
                        type: "post",
                        url: baseURl + "reserveWord/"+page_id,
                        headers: {
                            'Authorization': apikey
                        },
                        data: {
                            'word': word
                        },

                        complete: function () {
                            $('#btn_wr').html("رزرو عبارت")

                        },

                        success: function (result, status, xhr) {
                            $('#message-wr_success').removeClass('d-none');

                        },


                        error: function (xhr, status, error) {
                            var json = JSON.parse(xhr.responseText);
                           $('#message-wr_failure').removeClass('d-none').text(json["message"])

                        }

                    });

                } else {
                    $('#btn_wr').html("<span class=\"spinner-border spinner-border-sm mr-2\"></span>\n" +
                        "لطفا صبر کنید")
                    $('#tv_bank_wr').removeClass('d-none').addClass('d-block');

                    $.ajax({
                        type: "post",
                        url: baseURl + "reserveWordReq",
                        headers: {
                            'Authorization': apikey
                        },
                        data: {
                            'word': word,
                            'page_id': page_id,
                            'aow': "web"
                        },

                        complete: function () {


                        },

                        success: function (result, status, xhr) {
                            $('#btn_wr').html("رزرو عبارت")
                            var data = result["message"];
                            var url = baseURl + "wordReserveBC/" + user_id + "?data=" + data;
                            window.location = url;

                        },


                        error: function (xhr, status, error) {
                            $('#btn_wr').html("رزرو عبارت")
                            $('#tv_bank_wr').removeClass('d-block').addClass('d-none');
                            var json = JSON.parse(xhr.responseText);
                            $('#message-wr_failure').removeClass('d-none').text(json["message"])

                        }

                    });

                }

            })



            $("#modal_word").on("hidden.bs.modal", function () {

                $('#message-wr_success').removeClass('d-flex').addClass('d-none');
                $('#message-wr_failure').removeClass('d-flex').addClass('d-none');

            }) // call back for function on closing login_modal






            $('.btn_reserve_bg').click(function () {

                special_id = $(this).attr("data-special");
                gold = $(this).attr("data-gold");
                title = $(this).attr("data-title");
                day_special=parseInt($(this).attr("data-time"));

                var isReserved = $(this).attr("data-reserved");
                var banner_need = $(this).attr("data-banner-need");
                var have_banner= $(this).attr("data-have-banner");
                var available = $(this).attr("data-available");

                if (isReserved==1) {
                    swal({
                        title: "خطا !",
                        text: "جایگاه مورد نظر در حال حاضر برای شما رزرو میباشد",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }else if(inventory < gold) {
                    swal({
                        title: "خطا !",
                        text: "موجودی سکه برای رزرو جایگاه کافی نمیباشد",
                        icon: "warning",
                        button: "بسیار خوب ",
                    })
                }else if (banner_need==1 && have_banner==0) {

                    swal({
                        title: "خطا !",
                        text: "برای رزرو این جایگاه ابتدا باید بنر " + prefix +" را ثبت نمایید",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });

                }else if (available==0) {
                    swal({
                        title: "خطا !",
                        text: "ظرفیت این جایگاه در حال حاضر تکمیل میباشد",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }  else {
                    rbg=true;
                    $('#tv_gold_reserve').text(title);
                    $('#tv_price').html("<i class='fa fa-coin ml-1'></i> هزینه : " + gold + " سکه").addClass('text-gold').removeClass('text-green');
                    $('#tv_time').html("<i class='fa fa-clock ml-1'></i> مدت رزرو : " + day_special*24 + " ساعت");
                    $('#modal_rbg').modal('show');
                }

            })
            $('.btn_reserve_bc').click(function () {


                special_id = $(this).attr("data-special");
                title = $(this).attr("data-title");
                var price = $(this).attr("data-price");
                day_special=parseInt($(this).attr("data-time"));

                var isReserved = $(this).attr("data-reserved");
                var banner_need = $(this).attr("data-banner-need");
                var have_banner= $(this).attr("data-have-banner");
                var available = $(this).attr("data-available");


                if (isReserved==1) {
                    swal({
                        title: "خطا !",
                        text: "جایگاه مورد نظر در حال حاضر برای شما رزرو میباشد",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }else if (banner_need==1 && have_banner==0) {

                    swal({
                        title: "خطا !",
                        text: "برای رزرو این جایگاه ابتدا باید بنر " + prefix +" را ثبت نمایید",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });

                }else if (available==0) {
                    swal({
                        title: "خطا !",
                        text: "ظرفیت این جایگاه در حال حاضر تکمیل میباشد",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }  else {
                    rbg=false;
                    $('#tv_gold_reserve').text(title);
                    $('#tv_price').html("<i class='fa fa-money-bill  ml-1'></i> هزینه : " + price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " تومان")
                        .addClass('text-green').removeClass('text-gold');
                    $('#tv_time').html("<i class='fa fa-clock ml-1'></i> مدت رزرو : " + day_special*24 + " ساعت");
                    $('#modal_rbg').modal('show');
                }





            })
            $('#btn_close_bg').click(function () {

                $('#modal_rbg').modal('hide');
            })
            $('#btn_reserve_bg_dialog').click(function () {

                $('#spinner_rbg').removeClass("d-none");
                $('#error_rgb').addClass("d-none");

                if (rbg) {
                    $.ajax({
                        type: "GET",
                        url: baseURl + "reserveBg/" + special_id,
                        headers: {
                            'Authorization': apikey

                        },
                        data: {
                            'page_id': page_id
                        },

                        complete: function () {
                            $('#spinner_rbg').addClass("d-none");

                        },

                        success: function (result, status, xhr) {
                            $('#message_rbg').removeClass("d-none")
                            setTimeout(function () {
                                location.reload();
                            },3000)
                        },


                        error: function (xhr, status, error) {
                            var json = JSON.parse(xhr.responseText);
                            $('#error_rgb').removeClass("d-none").text(json["message"]);

                        }


                    });
                } else {
                    $('#tv_going_bank').removeClass('d-none').addClass('d-block');
                    $.ajax({
                        type: "post",
                        url: baseURl + "reserveReq",
                        headers: {
                            'Authorization': apikey
                        },
                        data: {
                            'special_id': special_id,
                            'page_id': page_id,
                            'aow': "web"
                        },

                        complete: function () {
                            $('#spinner_rbg').removeClass("d-none");

                        },

                        success: function (result, status, xhr) {

                            var data = result["message"];
                            var url = baseURl + "reserveBc/" + user_id + "?data=" + data;
                            window.location = url;

                        },


                        error: function (xhr, status, error) {
                            var json = JSON.parse(xhr.responseText);
                            $('#tv_going_bank').removeClass('d-block').addClass('d-none');
                            $('#error_rgb').removeClass("d-none").text(json["message"]);

                        }

                    });

                }





            })






        })


    </script>

    <script src="dashbord/dist/js/adminlte.js"></script>


</body>
</html>


