<?php


include '../header.php';


$id = $_GET["id"];
try {
    $response = $client->request('GET', 'getPage/' . $id, [
        'headers' => [
            'Authorization' => $user['apikey']
        ],
    ]);

} catch (GuzzleHttp\Exception\ClientException $e) {
    $response_tttt = $e->getResponse();
    $responseBodyAsString = $response_tttt->getBody()->getContents();
    header('Location:' . BASE_URL . "/dashbord");
}

$page_details = json_decode($response->getBody(), true);
$page = $page_details["page"];
$tags_page = $page_details["tags"];
$screens = $page_details["screens"];

$tags_request = $client->request('GET', 'gettags');
$tags = json_decode($tags_request->getBody(), true);

?>




    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">



        <div class="container-fluid  p-xl-3 p-2" id="div_messages">

            <div class="container-fluid d-flex flex-wrap justify-content-between  align-items-center p-xl-3 p-2">
                <h1 class="my-h1 mr-md-2"> به روز رسانی <?php echo $page["prefix"] . " " . $page["name"] ?> </h1>
            </div>

            <div class="row">

                <div class="col-lg-6 col-md-8 offset-lg-2">


                    <div class="d-flex flex-column rtl w-100 sumbit_form register" id="final_step">
                        <!--page register div-->



                        <div class="d-flex  page_info mt-2">
                            <span class="small-font  text-center w-50 page_info"
                                  id="tv_id"> <?php echo $page["id"] ?> <i class="fa fa-id-card"></i> </span>
                            <span class="small-font  text-center w-50 page_info " id="tv_members"> <i
                                        class="fa fa-users"></i>
                              <?php echo number_format($page["member"]) . " " . $page["member_prefix"] ?>
                              </span>
                        </div>

                        <div class="image-upload align-self-start mt-4">
                            <!--     <label for="img-input">

                                         </label>-->
                            <img class="fit-cover" src=<?php echo IMG_URL . "chanel_pic/" . $page["thumb"] ?> id="img_page" width="110"
                                 height="110""/>

                            <input id="img-input" type="file" accept="image/*"
                                   data-type='image'/>
                        </div>

                        <span class="text-right small-font align-self-start mt-1 mr-2 text-bold"><?php echo "تصویر " .$page["prefix"]?></span>


                        <span class="d-none text-center text-danger small-font mr-1" id="lbl_error_pic">تصویر انتخاب نشده است </span>




                        <div class="d-flex flex-column  rtl mt-4 w-100">
                            <span>نام کانال</span>
                            <!-- <i class="fa fa-not-equal text-gray "></i>-->
                            <input type="text" maxlength="60" value="<?php echo $page["name"] ?>" id="et_page_name"
                                   class="form-control w-100  ">
                            <span class="d-none text-right text-danger small-font mt-1 mr-1" id="lbl_error_name">نام کانال وارد نشده است ! </span>
                        </div>

                        <span class="small mt-4 text-right">توضیحات مختصر خود را حداکثر در 60 حرف وارد نموده و تلاش کنید از کلمات کلیدی و چشمگیر استفاده نمایید</span>
                        <div class="d-flex flex-column rtl mt-1 w-100">
                            <input maxlength="60" type="text" value="<?php echo $page["short_des"] ?>"
                                   placeholder="توضیحات مختصر " id="et_page_short"
                                   class="form-control w-100">
                            <span class="d-none text-right text-danger small-font mt-1 mr-1" id="lbl_error_shor_des">توضیحات مختصر باید دارای حداقل 15 حرف باشد</span>
                        </div>


                        <span id="textt"
                              class="small mt-4 text-right"><?php echo "به صورت کامل شرح فعالیت " .$page["prefix"] . " خود را بیان نمایید"?></span>
                        <div class="d-flex align-items-center rtl mt-1 w-100">
                            <textarea class="form-control  small-font w-100" rows="8" id="et_description"
                                      placeholder="توضیحات کامل"><?php echo $page["des"] ?></textarea>
                        </div>


                        <span id="textt"
                              class="small mt-4 text-right"><?php echo "برچسب های مرتبط با " . $page["prefix"]. " خودر را انتخاب نمایید انتخاب برچسب های صحیح به شما در افزایش بازدید کمک خواهند نمود (حداکثر 10 برچسب)"?></span>
                        <div class="d-flex align-items-center rtl mt-1 w-100">
                            <input maxlength="60" type="text" placeholder="برچسب" id="et_tags"
                                   class="form-control w-100">
                        </div>

                        <ul class="rtl text-right  p-0 tags-ul d-none" id="ul_tags">
                            <?php foreach ($tags as $tag) {
                                $exists = false;
                                foreach ($tags_page as $tag_page) {
                                    if ($tag["tag_id"] == $tag_page["tag_id"]) {
                                        $exists = true;
                                    }
                                }

                                if ($exists == false) {
                                    echo "<li data-id='$tag[tag_id]' id='$tag[tag_id]' class='align-items-center pt-2 pb-2 pr-1 li-tag'> $tag[name]</li>";
                                } else {
                                    echo "<li data-id='$tag[tag_id]' id='$tag[tag_id]' class='align-items-center pt-2 pb-2 pr-1 li-tag li-tag-selected'> 
                                      $tag[name]</li>";
                                }

                            }
                            ?>


                        </ul>

                        <div class="d-flex flex-wrap rtl text-right small-font mt-1 tags-div">
                            <!--div to add new tags -->
                            <?php
                            foreach ($tags_page as $tag_page) {
                                echo "<span data-id=$tag_page[tag_id] id=\"$tag_page[tag_id]\" class=\"d-flex inline align-items-center justify-conten -center selected-tag \">$tag_page[name] <i class=\"fa fa-times-circle mr-2\"></i></span>";
                            }

                            ?>

                        </div> <!--div to add new tags -->


                        <span id="textt"
                              class="small mt-4 text-right"><?php echo "تصاویر مرتبط با " .$page["prefix"] . " خود را وارد نمایید . (حداکثر 6 تصویر)"?>  </span>

                        <div class="image-upload align-self-start mt-2 d-flex w-100 flex-wrap " id="screen-imgs">

                            <?php
                            $i = 1;
                            foreach ($screens as $screen) {
                                $src = IMG_URL . "chanel_screen/" . $screen["thumb_name"];

                                echo "<div class=\"screen_div mr-1 mt-1\" id=\"screen_$i\" data-id='$screen[ps_id]' data-state=\"2\" 
                                             data-screen='$screen[ps_id]' >
                                  <img class=\"img-selected\"
                                       src=$src width=\"100%\" height=\"100%\"/>

                                  <div class=\"d-none align-items-center justify-content-center\">
                                    <span class=\"spinner-border text-white text-center spinner-border mr-1\">
                                    </span>
                                  </div>

                                  <i class=\"trash-bg fa fa-trash text-white text-black\">
                                  </i>
                              </div>";
                                $i++;
                            }
                            ?>

                            <?php

                             if (count($screens)<6){
                                 echo "          <div class=\"screen_div mr-1 mt-1\" id=\"screen_$i\" data-state=\"1\">
                                <img class=\"img-not-selected\"
                                     src=\"assets/screen.svg\" width=\"100%\" height=\"100%\"/>

                                <div class=\"d-none align-items-center justify-content-center\">
                                    <span class=\"spinner-border text-white text-center spinner-border mr-1\">
                                    </span>
                                </div>

                                <i class=\"d-none trash-bg fa fa-trash text-white text-black\">
                                </i>
                            </div>";
                             }
                            ?>


                            <input id="screen-input" multiple="multiple"  type="file" accept="image/*"
                                   data-type='image'/>
                        </div>

                        <span class="small mt-4 text-right">در صورت داشتن ویدیو تبلیغاتی لینک آپارات ویدیو را وارد نمایید <br/> مثال : https://www.aparat.com/v/bEu3w </span>
                        <div class="d-flex align-items-center rtl mt-2 w-100">
                            <input maxlength="60" type="text" value="<?php echo $page["aparat"] ?>"
                                   placeholder="لینک ویدیو آپارات " id="et_aparat"
                                   class="form-control w-100  ">
                        </div>

                    </div> <!--page register div-->
                    <span class="text-center new_error w-100 small-font d-none pt-1 pb-1 mt-1" id="new_error"> <!--error div-->

                    </span> <!--error div-->

                    <div class="d-none flex-column mt-3 " id="success-newpage">
                        <span class="text-center flex-grow-1"><i class="fa fa-badge-check"></i></span>
                    </div>

                    <button class="mt-3 btn_next_active pt-3 pb-3 d-flex w-100 align-items-center justify-content-center"
                            type="button" id="btn_next">
                        <span class="spinner-border spinner-border-sm mr-1 d-none"></span>
                        <i class="fa fa-check ml-1"></i>
                        ذخیره تغییرات
                    </button>


                </div>

            </div>


        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->

</div>


<div class="modal fade" id="cats_modal_new">

    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">

            <div class="modal-header d-flex justify-content-between align-items-center">


                <button type="button" class="myclose  mr-sm-1" data-dismiss="modal">&times;</button>
                <h5> دسته بندی ها</h5>

            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="container-fluid">

                    <div class="row text-right rtl">

                        <?php
                        $response = $client->request('GET', 'categories');
                        $categories = json_decode($response->getBody(), true);


                        foreach ($categories as $single) {
                            $catname = $single["name"];
                            $cat_id = $single["cat_id"];
                            $icon = $single["icon"];
                            $img_address = IMG_URL . 'cat/' . $icon;
                            echo " 
                       <div class='col-sm-6 col-lg-6 col-md-6 col-6 mt-3 d-flex align-items-center category nc' data-id='$cat_id' data-text='$catname'>
                        <div>
                          <a  class='category_selected' data-id='$cat_id'>
                        <img src='$img_address' width='45px' height='45px'>
                          </a>
                        </div>
                        <div class='mr-1'>
                      <a class='category_selected' data-id='$cat_id'>$catname</a>
                         </div>
                       
                   </div>";
                        }
                        ?>

                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer d-flex">
                <button type="button" class="btn btn-danger" data-dismiss="modal">بستن</button>
            </div>

        </div>
    </div>


</div> <!--modal_categories-->

<!-- /.content -->


<!-- ./wrapper -->


<!-- jQuery -->
<script src="dashbord/plugins/jquery/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="dashbord/dist/js/exit.js"></script>
<script src="js/compress.js"></script>

<script>

    $(document).ready(function () {

        $('#social-menu').addClass('active');

        var apikey = "<?php echo $user['apikey']?>";
        var myFormData = new FormData();

        //for genereatin alredy added tags to my tags list
        var tags_temp = JSON.parse(<?php echo "'" . json_encode($tags_page) . "'"?>);
        var tags = [];
        $.each(tags_temp, function (index, value) {
            tags.push(value["tag_id"].toString())
        });


        var cat_id = <?php echo $page["cat_id"]?>



        var screens_temp = JSON.parse(<?php echo "'" . json_encode($screens) . "'"?>);
        var screen_ids = [];

        $.each(screens_temp, function (index, value) {
            screen_ids.push(value["ps_id"])
        });

        var screen_numbers = screen_ids.length;
        var screen_i = screen_ids.length;
        var screen_id;


        //handle screens
        $("#screen-imgs").on('click', '.screen_div', function () {

            if ($(this).attr('data-state') === "1") {
                $('#screen-input').trigger('click')
            } else {
                //data is screen is the id of stored image on server
                var index = screen_ids.indexOf(parseInt($(this).attr("data-screen")));
                if (index !== -1) screen_ids.splice(index, 1);
                $(this).remove();
                screen_numbers--;
                //to check if add list there is one image view to selet new pic
                var has_upload_image = false;
                $("#screen-imgs > div").each(function () {

                    if ($(this).attr('data-state') == 1) {
                        has_upload_image = true;
                    }
                });
                //if there is not image view left so we add new one
                if (!has_upload_image) {
                    screen_i++;
                    //  screen_numbers++;
                    $('#screen-imgs').append("<div class=\"screen_div mr-1 mt-1\"  id=\"screen_" + screen_i + "\" data-state=\"1\" >\n" +
                        " <img class=\"img-not-selected\"\n" +
                        "src=\"assets/screen.svg\" width=\"100%\"  height=\"100%\"/>\n" +
                        "\n" +
                        "<div class=\"d-none align-items-center justify-content-center\">\n" +
                        "<span class=\"spinner-border text-white text-center spinner-border mr-1\">\n" +
                        "</span>\n" +
                        "</div>\n" +
                        "\n" +
                        "<i class=\"d-none trash-bg fa fa-trash text-white text-black\">\n" +
                        "</i>\n" +
                        "</div>")
                }

            }
        })

        $("#screen-input").change(function () {
            readURLS(this)
        });

        function readURLS(input) {
            var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
            for (var i = 0; i < input.files.length; i++) {
                var file = input.files[i];
                if (input.files && file) {
                    if ($.inArray(input.files[i]['type'], validImageTypes) > 0) {
                        // console.log(input.files[i])
                        if (screen_numbers < 6) {
                            // invalid file type code goes here
                            // var reader = new FileReader();
                            // reader.onload = function (e) {
                            //     var image = new Image();
                            //     image.src = reader.result;
                            //     image.onload = function () {
                            //         if (image.width > 300 && image.height > 300) {
                                        new Compressor(file, {
                                            quality: .8,
                                            maxWidth : 1200,
                                            maxHeight :1200,
                                            convertSize : 50000 ,
                                            success(result) {

                                                $('#screen-imgs .screen_div img:last').attr('src', window.URL.createObjectURL(result)).removeClass('img-not-selected');
                                                $('#screen-imgs .screen_div').addClass('screen_hover')
                                                $('#screen-imgs .screen_div div:last').removeClass("d-none").addClass("d-flex")
                                                $('#screen-imgs .screen_div i:last').removeClass("d-none")
                                                $('#screen-imgs .screen_div:last').attr('data-state', "2")
                                                screen_id = $('#screen-imgs .screen_div:last').attr("id")
                                                screen_numbers++;
                                                screen_i++;
                                                if (screen_numbers < 6) {
                                                    $('#screen-imgs').append("<div class=\"screen_div mr-1 mt-1\"  id=\"screen_" + screen_i + "\" data-state=\"1\" >\n" +
                                                        " <img class=\"img-not-selected\"\n" +
                                                        "src=\"assets/screen.svg\" width=\"100%\"  height=\"100%\"/>\n" +
                                                        "\n" +
                                                        "<div class=\"d-none align-items-center justify-content-center\">\n" +
                                                        "<span class=\"spinner-border text-white text-center spinner-border mr-1\">\n" +
                                                        "</span>\n" +
                                                        "</div>\n" +
                                                        "\n" +
                                                        "<i class=\"d-none trash-bg fa fa-trash text-white text-black\">\n" +
                                                        "</i>\n" +
                                                        "</div>")
                                                }


                                                    var screen_data = new FormData();
                                                    screen_data.append('pic', result, result.name)
                                                    screen_data.append('name', screen_id)

                                                    $.ajax({
                                                        type: "post",
                                                        processData: false, // important
                                                        contentType: false, // important
                                                        dataType: 'json',
                                                        url: baseURl + "screenUpload",
                                                        data: screen_data,

                                                        oncomplete: function () {
                                                            showProgress(false, "پایات")
                                                        },
                                                        success: function (result, status, xhr) {
                                                            console.log(result);
                                                          //  console.log(JSON.stringify(result))
                                                            $("#" + result["name"] + " div").removeClass("d-flex").addClass("d-none")
                                                            $("#" + result["name"]).attr('data-screen', result["message"])
                                                            screen_ids.push(result["message"])

                                                        },

                                                        error: function (xhr, status, error) {
                                                            // showProgress(false, "برسی " + socials[social_id - 1]["prefix"])
                                                            console.log(xhr.responseText);
                                                            var json = JSON.parse(xhr.responseText);
                                                            showError(true, json["message"])
                                                        }
                                                    })

                                                    myFormData.append('screen[]', result, result.name)


                                                //    Send the compressed image file to server with XMLHttpRequest.
                                            },
                                            error(e) {
                                                console.log(e.message);
                                            },
                                        });


                                    // } else {
                                    //
                                    //     swal({
                                    //         title: "! خطا",
                                    //         text: "حداقل ارتفاع و عرض تصویر انتخاب شده باید 300 پیکسل باشد ",
                                    //         icon: "warning",
                                    //         button: "بسیار خوب ",
                                    //     });
                                    // }


                                }

                            }




                         //   reader.readAsDataURL(file);

                     //   }

                    } else {
                        swal({
                            title: "خطا !",
                            text: "فرمت انتخاب شده مجاز نمیباشد لطفا یک تصویر انتخاب نمایید",
                            icon: "warning",
                            button: "بسیار خوب ",
                        });
                    }

                }
           // }

        }


        //chose image for page and put it for image view
        $('#img_page').click(function () {
            $('#img-input').trigger('click');
        })

        //handling what happens after img-input is selected
        $("#img-input").change(function () {
            //  var formData = new FormData(this);
            readURL(this);
        });

        function readURL(input) {

            if (input.files && input.files[0]) {
                var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(input.files[0]['type'], validImageTypes) > 0) {
                    // invalid file type code goes here.


                    // var reader = new FileReader();
                    //
                    // reader.onload = function (e) {
                    //     $('#img_page').attr('src', e.target.result);
                    // }
                    //
                    // reader.readAsDataURL(input.files[0]);
                    // form_data = input.files[0];
                    //  myFormData.append('pic', input.files[0])

                    new Compressor(input.files[0], {
                        quality: .8,
                        maxWidth : 1200,
                        maxHeight :1200,
                        convertSize : 50000,
                        success(result) {
                            $('#img_page').attr('src', window.URL.createObjectURL(result));
                            myFormData.append('pic', result, result.name)
                            // Send the compressed image file to server with XMLHttpRequest.
                        },
                        error(e) {
                            console.log(e.message);
                        },
                    });


                } else {
                    swal({
                        title: "خطا !",
                        text: "فرمت انتخاب شده مجاز نمیباشد لطفا یک تصویر انتخاب نمایید",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }


            }
        }


        //show modal
        $('#btn_category').click(function () {
            $('#cats_modal_new').modal('show');
        });

        //handle clicing on category item

        $('.nc').click(function () {
            var cat_name = $(this).attr("data-text");
            cat_id = $(this).attr("data-id");

            $('#btn_category').html(cat_name + " <i class=\"mr-auto fa fa-arrow-circle-left\"></i>")
                .css('borderColor', '#53a929');
            $('#cats_modal_new').modal('hide');

        })
        //handle tags
        $('#et_tags').focus(function () {
            $('.tags-ul').removeClass("d-none")
        }).blur(function () {
            setTimeout(function () {
                $('.tags-ul').addClass("d-none")
            }, 200)

        }).keyup(function () {
            var value = $(this).val();
            $('.tags-ul').removeClass("d-none")
            $("#ul_tags > li").each(function () {
                if ($(this).text().search(value) > -1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });

        $('.tags-ul li').click(function () {
            var tag = $(this).html();
            var tag_id = $(this).attr('data-id')

            //first check if it has no selected class so we select it and add its id to our array an append span to selected tags
            if (!$(this).hasClass('li-tag-selected')) {

                if (tags.length>9) {
                    swal({
                        title: "خطا !",
                        text: "حداکثر 10 برچسب قابل انتخاب است",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }else {
                    $(this).addClass('li-tag-selected')
                    tags.push(tag_id)
                    $('.tags-div').append("<span data-id=" + tag_id + " id=" + tag_id + " class=\"d-flex inline align-items-center justify-content-center selected-tag \">" + tag + "<i class=\"fa fa-times-circle mr-2\"></i></span>");
                    $('#et_tags').val('')
                    $('#ul_tags > li').each(function () {
                        $(this).show()
                    })

                }




            } else {
                //else we unselect it and remove from our array and remove span from selected tags
                $(this).removeClass('li-tag-selected')
                var index = tags.indexOf(tag_id);
                console.log(index)

                if (index !== -1) tags.splice(index, 1);
                $('.tags-div').find("#" + tag_id).remove();
            }

        })


        $('.tags-div').on('click', '.selected-tag', function () {

            var tag_id = $(this).attr('data-id')

            $('.tags-ul').find("#" + tag_id).removeClass("li-tag-selected")
            var index = tags.indexOf($(this).attr('data-id'));

            if (index !== -1) tags.splice(index, 1);
            $(this).remove();

        })


        $('#btn_next').click(function () {

            var id = <?php echo $id ; ?>;
            var name = $('#et_page_name').val();
            var short_des = $('#et_page_short').val();
            var des = $('#et_description').val();
            var aparat = $('#et_aparat').val();
            var page_idd ="<?php echo $page["id"]?>";
            var prefix = " <?php echo $page["prefix"]?>";


        if (final_check(name, short_des)) {
                var yourObject = {
                    id:page_idd,
                    prefix:prefix,
                    name: name,
                    short_des: short_des,
                    des: des,
                    cat_id: cat_id,
                    aparat: aparat
                };


                myFormData.append('object', JSON.stringify(yourObject));
                myFormData.append('tags', JSON.stringify(tags))
                myFormData.append('screens', JSON.stringify(screen_ids))
                showProgress(true, "در حال به روز رسانی " )
                showError(false, "")

                $.ajax({
                    type: "post",
                    processData: false, // important
                    contentType: false, // important
                    dataType: 'json',
                    url: baseURl + "updatePage/" + id,
                    data: myFormData,
                    headers: {
                        "Authorization": apikey
                    },
                    oncomplete: function () {
                        showProgress(false, "پایات")
                    },
                    uploadProgress: function (event, position, total, percentageComplete) {
                        $('.progress-bar').animate({
                            width: percentageComplete + '%'
                        }, {
                            duration: 1000
                        });
                    },

                    success: function (result, status, xhr) {
                         $('#btn_next').removeClass('d-flex').addClass('d-none');
                         $('#success-newpage').removeClass('d-none').addClass('d-flex').find('span').html("<i class='fa fa-badge-check ml-2'></i>"+  result["message"]);




                    },

                    error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                        var json = JSON.parse(xhr.responseText);
                        showError(true, json["message"])
                        showProgress(false,"به روز  رسانی " , "fa fa-check")
                    }


                })
            }

        });




        function showProgress(show, message, icon) {
            if (show) {
                $('#btn_next').html("<span class=\"spinner-border spinner-border-sm mr-1 \"></span>\n" +
                    "                        <i class=\"fa fa-arrow-circle-left d-none mr-1\"></i>\n" +
                    message +
                    "                    </button>\n")
            } else {
                $('#btn_next').html("<span class=\"spinner-border spinner-border-md mr-1 d-none\"></span>\n" +
                    "                        <i class=\""+icon+"   mr-1\"></i>\n" +
                    message +
                    "                    </button>\n")
            }
        }

        function showError(show, meessage) {
            if (show) {
                $('#new_error').removeClass('d-none').addClass('d-block').html(meessage)
            } else {
                $('#new_error').removeClass('d-block').addClass('d-none').html(meessage)
                /*     $('#new_error').slideUp(200, function () {
                         $('#new_error').removeClass('d-block').addClass('d-none')
                     });*/
            }
        }

        function final_check(name, short_des) {

            var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
            var passed = true;



            if (cat_id === 0) {
                passed = false;
                $('#lbl_error_cat').removeClass("d-none");
            }else {
                $('#lbl_error_cat').addClass("d-none");
            }

            if ((jQuery.trim(name)).length == 0) {
                passed = false;
                $('#lbl_error_name').removeClass("d-none");
            }else {
                $('#lbl_error_name').addClass("d-none");
            }

            if (short_des.length < 15) {
                passed = false;
                $('#lbl_error_shor_des').removeClass("d-none");
            }else {
                $('#lbl_error_shor_des').addClass("d-none");
            }

            return passed;

        }


    })


</script>
<!-- jQuery UI 1.11.4 -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!- AdminLTE App -->
<script src="dashbord/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

</body>
</html>


