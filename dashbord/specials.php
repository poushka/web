<?php

use function Src\Inc\jdate;

include 'header.php';
include '../helper/jdf.php';

$response_specials = $client->request('GET', 'getuserSpecials', [
    'headers' => [
        'Authorization' => $user['apikey']
    ],
]);
$specials = json_decode($response_specials->getBody(), true);


?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper bg-white">

    <h1 class="my-h1 mt-3 mr-md-4 mr-3 mr-2">ویژه شده ها</h1>

    <div class="container-fluid mt-3 pl-1 pl-md-3">

        <?php
        $iiiii = 0;
        foreach ($specials as $special) {
            echo "<div class=\"d-flex flex-column mt-2 m-0 \">
            
                <div id='h_$special[special_id]' data-id='$special[special_id]' data-open='0'  class='d-flex align-items-center special-header pointer'>
                 <p class='mr-2 mt-2 text-medium d-flex align-items-center'><i class='far  fa-star-shooting text-warning ml-1'></i> $special[title]</p>
                 
                 <div id='c_$special[special_id]' data-id='$special[special_id]' data-open='0' style='width: 40px; height: 40px;' class='bg-expand-speccial d-flex justify-content-center 
                  align-items-center img-circle mr-auto ml-1 expand-special pointer'>
                 <img  src='assets/bottom_white_arrow.svg' width='22' height='22'>
                 </div>
                </div>
                
                <div id='$special[special_id]' class='d-none flex-column mt-1 m-0'> ";
            if (count($special["items"]) == 0) {

                echo "<div class='bg-noitem d-flex align-items-center justify-content-center' style='height: 200px'>

                       <div class='d-flex flex-column align-items-center'>
                           <i  class='fal fa-5x fa-empty-set'></i>
                           <span>هیچ موردی یافت نشد!</span>
                        </div>
                   
                      </div>"; /*empty div*/
            } else {

                if ($special["type"] == 0) {    // 0 means specials so  if it is for specilas (banner and special)
                    foreach ($special["items"] as $item) {
                        $iiiii++;
                        $now = new DateTime(date("Y-m-d H:i:s"));
                        $start_ts = new DateTime($item["start_time"]);
                        $exp_ts = new DateTime($item["exp_time"]);
                        $diff = $exp_ts->getTimestamp() - $now->getTimestamp();
                        $start_jalali = "شروع رزرو : " . jdate("H:i Y/m/d", $start_ts->getTimestamp());
                        $exp_jalali = "پایان رزرو : " . jdate("H:i Y/m/d", $exp_ts->getTimestamp());
                        $des = $special['is_main'] == 1 ? $special['title'] . " " . $item['p_name'] :
                            $special['title'] . " " . $item['cat_name'] . " -> " . $item['p_name'];
                        $img = IMG_URL."chanel_pic/".$item["thumb"];

                        if ($diff < 0) {
                            $finish = "<span class='mt-2 text-danger text-center text-bold'>حذف تا دقایقی دیگر...</span>";
                        } else {
                            $finish = "<div class='d-flex justify-content-center align-items-center'>
                                <span class='d-flex align-items-center' style='color: green'><i  class='m-1 fal fa-hourglass-half'></i> زمان باقی مانده : </span>
                                <div style='direction: ltr' class='mr-2 mt-1 text-bold ml-2 count-down mb-2' data-seconds-left=$diff ></div>
                                </div>
                                ";
                        }

                        echo " <div   class=\"col-xl-8  mt-2 mr-xl-4 d-flex flex-column p-0  my_card card_special \" style=\"border-radius: 8px; \">

                <div class=\"d-flex  align-items-center \">

                    <span class=\"catname_special  mr-2\">$item[p_name]</span>
                      <span class=\"catname_special  mr-1\">$item[cat_name]</span>
                    
                    <img src=$img class=\"m-1 rounded mr-auto\" width=\"45px\" height=\"45px\" >
                </div>
                
                <div class=\"d-flex  justify-content-between align-items-center\">
                    <span class=\"mr-2  mt-1 mb-2 text-bold\">$item[name]</span>
                    <span class=\"ml-1 mt-1 mb-2 text-bold\">$item[id]</span>
                </div>
                <span class='mr-2'>$des</span>
                $finish
                <div class=\"d-flex flex-wrap mt-2\">
                    <span class=\"bg-gray bg-success text-center expire_special-right mt-1 flex-grow-1\">$start_jalali</span>
                    <span class=\"text-center bg-danger expire_special-left mt-1 flex-grow-1\">$exp_jalali</span>
                </div>
            </div>"; /*item_div*/

                    }
                } else if ($special["type"] == 1) {


                    foreach ($special["items"] as $item) {
                        $img = IMG_URL."chanel_pic/".$item["thumb"];

                        echo " <div   class=\"col-xl-8 mt-2 mr-xl-4 d-flex flex-column p-0  my_card card_special \" style=\"border-radius: 8px; \">

                <div class=\"d-flex  align-items-center \">

                    <span class=\"catname_special  mr-2\">$item[p_name]</span>
                      <span class=\"catname_special  mr-1\">$item[cat_name]</span>
                    
                    <img src=$img class=\"m-1 rounded mr-auto\" width=\"45px\" height=\"45px\" >
                </div>
                
                <div class=\"d-flex  justify-content-between align-items-center\">
                    <span class=\"mr-2  mt-1 mb-2 text-bold\">$item[name]</span>
                    <span class=\"ml-1 mt-1 mb-2 text-bold\">$item[id]</span>
                </div>
                <span class='mr-2'>$special[title] $item[prefix] $item[name] </span>";

                  foreach ($item["words"] as $word) {

                      $now = new DateTime(date("Y-m-d H:i:s"));
                      $start_ts_word = new DateTime($word["start_time"]);
                      $exp_ts_word = new DateTime($word["exp_time"]);

                      $start_jalali_word = "شروع رزرو : " . jdate("H:i Y/m/d", $start_ts_word->getTimestamp());
                      $exp_jalali_word = "پایان رزرو : " . jdate("H:i Y/m/d", $exp_ts_word->getTimestamp());

                      echo "<span class='bg-word mr-2 mt-3 align-self-start'>$word[word]</span>
                            <div class=\"d-flex flex-wrap mt-1\">
                    <span class=\"bg-gray bg-success text-center expire_special-right mt-1 flex-grow-1\">$start_jalali_word</span>
                    <span class=\"text-center bg-danger expire_special-left mt-1 flex-grow-1\">$exp_jalali_word</span>
                     </div>" ;


                  }

                        echo " 
             
            </div>"; /*item_div*/

                    }
                }
            }

            echo "
                        </div> <!--all items of a branch div-->
                        
                     </div>";   /* one special with its header div */

        }

        ?>


    </div>  <!--container-fluid-->


</div><!-- /.content-wrapper -->


<!-- jQuery -->
<script src="dashbord/plugins/jquery/jquery.min.js"></script>
<script src="js/jquery.simple.timer.js"></script>
<script src="js/bootstrap.js"></script>


<script>

    $(document).ready(function () {



        $('.special-header').click(function () {

            var open = $(this).attr('data-open');
            var id = $(this).attr('data-id');

            if (open == 1) {
                $(this).attr('data-open', 0)

                $('#c_'+id).children().fadeOut('fast', function () {
                    $(this).attr('src', 'assets/bottom_white_arrow.svg')
                    $(this).fadeIn('fast')
                })
                $('#' + id).hide(function () {
                    $(this).removeClass('d-flex').addClass('d-none')
                    //   $(this).css('visibility','colla')
                })


            } else {
                $(this).attr('data-open', 1)

                $('#' + id).removeClass('d-none').show(function () {
                    $(this).addClass("d-flex")
                });

                $('#c_'+id).children().fadeOut('fast', function () {
                    $(this).attr('src', 'assets/top_white_arrow.svg')
                    $(this).fadeIn('fast')
                })


            }


        })


        $('.count-down').startTimer();

    })

</script>
<!-- jQuery UI 1.11.4 -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!- AdminLTE App -->
<script src="dashbord/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

</body>

</html>
