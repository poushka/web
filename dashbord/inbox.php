<?php
    use function Src\Inc\jdate;
    include 'header.php';
    include '../helper/jdf.php';

    $response_message = $client->request('GET', 'getMessages?last_id=0', [
        'headers' => [
            'Authorization' => $user['apikey']
        ],
    ]);
    $messages = json_decode($response_message->getBody(), true);

    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" >
        <?php
        if (count($messages)==0) {
            echo "    <div class=\"no_message_div d-flex flex-column align-items-center justify-content-center\">
        <i class=\"fa fa-envelope mr-lg-5\"> </i>
        <p class=\"mt-1 mr-lg-5\">صندوق پیام خالی میباشد</p>
    </div>";
        }
        ?>

        <!-- Content Header (Page header) -->

        <div class="container-fluid  p-xl-3 p-2" id="div_messages">
            <h1 class="text-dark my-h1 mr-md-2">صندوق پیام</h1>

            <?php
            foreach ($messages as $message) {
                $thumb=null;
                $sender=null;

                if ($message["active"]>4) {
                    $thumb="assets/profile.svg";
                    $sender="تیم مدیریت پوشکا";
                }else {
                    $thumb=$message["thumb"] ==null ? "assets/profile.svg" : IMG_URL."profile/". $message["thumb"];
                    $sender=$message["username"]==null ? $message["mobile"] : $message["username"];
                }

                $reply_classes = "text-left text-primary pointer mb-1 ml-1 tv_replay ";



             if($message["active"] > 4) {
                    $reply_classes="d-none";
                }
                echo "  
                  <div class=\"d-flex my_card flex-column mt-3\" id=\"$message[message_id]\">
                <div class=\"d-flex align-items-center position-relative\" >
                    <img class=\"rounded-circle mt-2 mr-1\" src=\"$thumb\" width=\"60px\" height=\"60px\">
                    <p class=\"mb-0 mr-2 ltr\">$sender</p>
                    <p class='ltr created_at'>$message[created_at]</p>
                </div>
                <span class=\"text-bold mt-2 mr-2\">$message[title]</span>
                <p class=\"text-justify text-gray m-2\">$message[message]</p>
                <p data-reciver='$message[sender_id]' data-username=$sender  class='$reply_classes'>   پاسخ دادن <i class='fa fa-reply'></i></p>
     
            </div>";
            }
            ?>
            <div class="mt-2  mr-md-4 ml-md-4 d-none justify-content-center align-items-center" id="loading_list"
                 style="height: 60px ; background: #e2e2e2 ; border-radius: 12px">
                <span> <div class="spinner-border  text-muted"></div> در حال دریافت لطفا صبر کنید </span>
            </div>
        </div>
    </div>

<div class="modal fade" id="replay_modal">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">

            <div class="modal-header d-flex my_background justify-content-center align-items-center">
                <p class="text-bold ltr" id="tv_title"></p>

            </div>

            <!-- Modal body -->
            <div class="modal-body my_background">

                <div class="d-flex  rtl form-group login_register">
                    <input type="text" class="form-control my_background"  id="et_title"
                              placeholder="عنوان پاسخ">
                </div>
                <div class="d-flex  rtl form-group login_register">
                    <textarea class="form-control my_background" rows="3" id="et_replay"
                              placeholder="متن پاسخ"></textarea>
                </div>

                <span class="text-center d-none " id="replay_message">پیام شما ارسال گردید </span>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer rtl  d-flex justify-content-start comment-body">

                <a class="text-success pointer mr-3 text-bold small-font" id="tv_send_message">ارسال کردن </a>
                <span class="spinner-border text-success spinner-border-sm mr-2 d-none" id="loading_replay"></span>
                <div class="mr-3 " style="border-left: 1px solid #676767 ; height: 40px;"></div>
                <a class="text-danger pointer mr-3 text-bold small-font pr-2 pl-2" data-dismiss="modal"> لغو</a>

            </div>

        </div>
    </div>


</div> <!--replay_modal-->



<script src="dashbord/plugins/jquery/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
    <script src="dashbord/dist/js/exit.js"></script>

<script>

    $(document).ready(function () {

        var loading = false;
        var finished = false;
        var current_reciver ;
        var current_username ;
        var apikey =  "<?php echo $user["apikey"] ?>";

        var mycount = <?php echo count($messages) ?>;



        $(window).scroll(function () {


            if ($(window).scrollTop() + 1 >= $(document).height() - $(window).height()) {


                if (!loading && !finished && mycount>9) {
                    $("#loading_list").removeClass("d-none").addClass("d-flex");
                    var last_id = $('.my_card:last').attr("id")
                    loading=true;
                 console.log(apikey);

                    $.ajax({
                        type: "get",
                        url: baseURl + "getMessages",
                        headers: {
                            'Authorization': apikey

                        },
                        data: {
                            'last_id': last_id
                        },

                        complete: function () {
                            loading = false;
                            $("#loading_list").removeClass("d-flex").addClass("d-none");

                        },

                        success: function (result, status, xhr) {

                            $.each(result, function (index, element) {

                                var message_id = element.message_id;

                                if (element.active > 4) {
                                    var thumb = "assets/profile.svg";
                                    var username = "تیم مدیریت پوشکا";
                                    var replay_calss = "d-none"
                                } else {
                                    var thumb = element.thumb==null ? "assets/profile.svg" : IMAGE_URL+ "profile/" + element.thumb;
                                    var username = element.username==null ? element.mobile : element.username;
                                    var replay_calss = "text-left text-primary pointer mb-1 ml-1 tv_replay"
                                }





                           var div = "<div class='d-flex my_card flex-column mt-2' id='"+message_id+"'>" +
                               "<div class='d-flex align-items-center position-relative'>" +
                               "<img class='rounded-circle mt-2 mr-1' width='60px' height='60px' src='"+thumb+"'>"+
                               "<p class='mb-0 mr-2 ltr'>"+username+"</p>" +
                               "<p class='ltr created_at'>"+element.created_at+"</p>"+
                               "</div>" +
                               "<span class='text-bold mt-2 mr-2'>"+element.title+"</span>" +
                                "<p class='text-justify text-gray m-2'>"+element.message+"</p>"+
                               "<p data-reciver='"+element.sender_id+"' data-username='"+username+"' " +
                               " class='"+replay_calss+"'> پاسخ دادن <i class='fa fa-reply'></i></p>"+
                               "</div>"

                            $('#div_messages').append(div);


                            });


                        },


                        error: function (xhr, status, error) {
                            console.log(xhr.responseText);

                            var json = JSON.parse(xhr.responseText);
                            if (xhr.status === 404) {
                                finished = true;
                            }


                        }


                    });
                }


            }
        });




        $('#div_messages').on('click', '.tv_replay', function () {
          current_reciver = $(this).attr("data-reciver");
          current_username = $(this).attr("data-username");

            $('#replay_modal').modal('show');
            $('#tv_title').html(  current_username + " ارسال پاسخ به به " );
        })





        $('#tv_send_message').click(function () {
         var title =  $('#et_title').val();
         var message = $('#et_replay').val();
         if (title.length==0) {
             showMessage(true,true,"عنوان پیام خالی میباشد !")
         } else if (message.length==0) {
                 showMessage(true,true,"متن پیام خالی میباشد")
         } else {
          showLoading(true)
             showMessage(false,false,"")
             $.ajax({
                 type: "POST",
                 url: baseURl + "sendMessage",
                 headers: {
                     'Authorization': apikey

                 },
                 data: {
                     'reciver_id': current_reciver ,
                      'title' : title ,
                      'message' : message
                 },

                 complete: function () {
                     loading = false;
                     showLoading(false)

                 },

                 success: function (result, status, xhr) {
                     $('#et_title').val("");
                     $('#et_replay').val("");
                     showMessage(true,false,result["message"])
                 },


                 error: function (xhr, status, error) {

                     var json = JSON.parse(xhr.responseText);

                     console.log(json);

                     showMessage(true,true,json["message"])



                 }


             });

         }

        })


        function showMessage(show , is_error, message ) {

            if (show) {
                $('#replay_message').removeClass('d-none').addClass('d-block').html(message);
                if (is_error) {
                    $('#replay_message').css('color','#cf000f')
                } else {
                    $('#replay_message').css('color','#00b16a')
                }


            } else {
                $('#replay_message').addClass('d-none').removeClass('d-block')
            }


        }

        function showLoading(show) {
            if (show) {
                $('#loading_replay').removeClass('d-none');
                $('#tv_send_message').html("در حال ارسال")
            } else {
                $('#loading_replay').addClass('d-none');
                $('#tv_send_message').html("ارسال کردن")
            }
        }

        $('#replay_modal').on('hidden.bs.modal', function () {
            $('#et_title').val("");
            $('#et_replay').val("");
            $('#replay_message').addClass('d-none').removeClass('d-block')
            showMessage(false,false,"")
        })

    })

</script>

<!- AdminLTE App -->
<script src="dashbord/dist/js/adminlte.js"></script>


</body>
</html>
