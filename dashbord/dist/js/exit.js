$('.nav-item').click(function () {
    var attr = $(this).attr('data-function')

    if (attr === "exit") {
        $.ajax({
            type: "GET",
            url: baseURl+ "logout",
            success: function (result, status, xhr) {
                window.location = web_url;
            },
            error: function (xhr, status, error) {
                var json = JSON.parse(xhr.responseText);
                alert(json["message"]);
            }

        });
    }

})