<?php
include 'header.php'
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <div class="container-fluid d-flex justify-content-between  align-items-center p-xl-3 p-2">
        <h1 class="my-h1 mr-2">داشبورد</h1>
    </div>

    <div class="container-fluid  p-2 " style="min-height: 66vh">

        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6 p-1 pr-3">
                <!-- small box -->
                <div class="small-box bg-gold ">
                    <div class="inner">
                        <h3><?php echo $dashbord["inventory"] ?></h3>
                        <p class="text-bold">موجودی سکه </p>
                    </div>

                    <div class="icon">
                        <i class="fa fa-coins p-2 mt-2"></i>
                    </div>
                    <a href="dashbord/coin" class="small-box-footer">افزایش موجودی <i class="fas fa-arrow-left"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6 p-1 pl-3 pl-lg-1">
                <!-- small box -->
                <div class="small-box bg-socials ">
                    <div class="inner">
                        <h3><?php echo $dashbord["pages"] ?></h3>
                        <p class="text-bold">صفحات و کانال ها </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-chart-network p-1 mt-2 mr-2"></i>
                    </div>
                    <a href="dashbord/social.php" class="small-box-footer"> مدیریت <i class="fas fa-arrow-left"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6 p-1 pr-3 pr-lg-1">
                <!-- small box -->
                <div class="small-box bg_messsage">
                    <div class="inner">
                        <h3><?php echo $dashbord["messages"] ?></h3>
                        <p class="text-bold">پیام ها </p>
                    </div>
                    <div class="icon">
                        <i class="text-white fa fa-inbox p-2 mt-2"></i>
                    </div>
                    <a href="dashbord/inbox.php" class="small-box-footer "> صندوق پیام <i class="fas fa-arrow-left"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6 p-1 pl-3">
                <!-- small box -->
                <div class="small-box bg_usercode ">
                    <div class="inner">
                        <h3>آمار</h3>
                        <p class="text-bold">ویژه شده ها</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-star-shooting p-2 mt-2"></i>
                    </div>
                    <a href="dashbord/specials.php" class="small-box-footer text-dark">مشاهده <i class="fa fa-arrow-left mr-1"></i></a>
                </div>
            </div>

        </div>






        <div class="d-flex flex-column  align-items-center  justify-content-center"
             style="background: #4f4f4f; height: 120px; margin-right: 5px">

            <img class="img-circle " src=<?php echo $user["thumb"]==null? BASE_URL."assets/profile.svg" : IMG_URL. 'profile/'.$user["thumb"]?>
            width="120px" height="120px" style="margin-top: 120px"
            >
        </div>
        <div class="d-flex flex-column align-items-center " style="margin-top: 60px">
            <span class="mt-2 text-gray text-bold"><?php echo $user["username"]==null ? "بدون نام کاربری" : $user["username"]?> </span>
            <span class="text-gray"> <?php echo $user["name"]==null ? " نام " : $user["name"]?> </span>

            <ul class="socials-ul">
                <li ><a href="<?php echo $user["t_id"] ==null? '#' : 'https://telegram.me/'.$user['t_id'] ?>" target="_blank"><i class="fab fa-telegram telegram-color"></i></a></li>
                <li><a href="<?php echo $user["instagram"] ==null? '#' : 'https://instagram.com/'.$user['instagram'] ?>" target="_blank"><i class="fab fa-instagram insta-color"></i></a></li>
                <li><a href="<?php echo $user["email"] ==null? '#' : 'mailto:'.$user['email'] ?>" target="_blank"><i class="fa fa-envelope gmail-color"></i></a></li>

            </ul>

            <p class="mt-2 mr-xl-5 mr-1 ml-xl-5 ml-1 text-center">
                <?php echo $user["bio"] == null ? " بیوگرافی " : $user["bio"]?>
            </p>

            <a href="dashbord/profile" class="text-center text-white btn btn-primary mb-2"> به روز رسانی حساب کاربری</a>

        </div>

    </div>






    <div style="height: 160px; background: #e6e6e6;" class="mt-2 d-flex flex-column justify-content-center">

        <span class="text-center text-bold mb-4">ارتباط مستقیم با پشتیبانی</span>
        <ul class="text-center" id="ul-support">
            <i><a><i class="fab fa-instagram"></i></a></i>
            <i><a><i class="fab fa-telegram"></i></a></i>
            <i><a><i class="fa fa-envelope"></i></a></i>
        </ul>

    </div>


    <!-- /.row (main row) -->
</div><!-- /.container-fluid -->




<!-- /.content -->


</div>
<!-- /.content-wrapper -->


<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- jQuery -->
<script src="dashbord/plugins/jquery/jquery.min.js"></script>
<script src="dashbord/dist/js/exit.js"></script>


<!-- jQuery UI 1.11.4 -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!- AdminLTE App -->
<script src="dashbord/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

</body>
</html>
