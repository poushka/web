<?php
//todo footer !!!!
//todo fix make page
include "helper/init.php";

$description="بخش پرسش های متداول کاربران در پوشکا";

$faq_response = $client->request('GET', 'faq');
$faqs = json_decode($faq_response->getBody(), true);


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>پرسش های متداول در پوشکا</title>
    <base href=<?php echo BASE_URL ?>>
    <meta charset="utf-8">
    <title>پوشکا مرجع شبکه های اجتماعی</title>\
    <meta http-equiv="content-language" content="fa">
    <meta property="og:site_name" content="پوشکا - مرجع شبکه های اجتماعی">
    <meta name="description" content="<?php echo $description?>">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!--    <link rel="stylesheet" href="css/bootstrap.css">-->

    <link rel="stylesheet" href="awsome/css/all.min.css">
    <link rel="stylesheet" href="css/style.css">

    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
<!--    <script src="js/jquery.js"></script>-->
    <script src="js/config.js"></script>


</head>

<body>

<nav class="navbar sticky-top d-flex justify-content-between navbar-light bg-white pr-1 pl-1 pr-md-2 pl-md-2">
    <a href="<?php echo BASE_URL . 'new/Instagram' ?>" id="new_page"
       class="navbar-brand alertPulse-css bg_new_page brand-background ml-1 ml-md-2 ">
        ثبت صفحه یا کانال
    </a>
    <a href="<?php echo BASE_URL ?>" class="text-bold brand-color mr-2 text-bold brand-textsize">پوشکا</a>

</nav>  <!--nav bar-->


<div class="min-height-70 text-right container d-flex flex-column">
    <h1 class="main-h1 mt-3 align-self-center"> سوالات متداول در پوشکا </h1>

    <?php
      foreach ($faqs as $faq) {
          $des_id = "faq-des-".$faq["faq_id"];
          $img_id = "faq-img-".$faq["faq_id"];

          echo "    <div class=\"pt-3 pb-2 mt-2 pr-1 pr-sm-2 rounded d-flex flex-column\" style=\"background: #ededed\">
      <div class=\"d-flex align-items-center justify-content-between pointer faq-header\" data-open=\"0\" data-id='$faq[faq_id]'>
          <div class=\"d-flex align-items-center bg-hr justify-content-center ml-2\" style=\"border-radius: 50px; width: 40px; height: 40px\">
              <img id='$img_id' src=\"assets/bottom_black_arrow.svg\" width=\"25px\" height=\"25px\">
          </div>
          <h4 class=\"myh4\">$faq[title]</h4>
      </div>

        <span class=\"mt-2 mr-sm-3 mr-2 d-none\" id='$des_id'>$faq[description]</span>

    </div>";

      }

    ?>




</div>


<?php include 'footer.php' ?>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<!--<script src="js/bootstrap.min.js"></script>-->

<script>

    $('.faq-header').click(function () {
        var data_open = $(this).attr("data-open");
        var data_id = $(this).attr("data-id");

        if (data_open=="0") {
            $(this).attr("data-open",1)
            $('#faq-des-'+data_id).removeClass('d-none');
            $('#faq-img-'+data_id).attr('src',web_url+"assets/top_black_arrow.svg")
        } else {
            $(this).attr("data-open",0)
            $('#faq-des-'+data_id).addClass('d-none');
            $('#faq-img-'+data_id).attr('src',web_url+"assets/bottom_black_arrow.svg")
        }


    })

</script>


</body>
</html>