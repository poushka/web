<?php
header("Content-type: application/xml; charset=utf-8");
echo '<?xml version="1.0" encoding="UTF-8" ?>'.PHP_EOL;
include 'helper/DbConnection.php';
include 'helper/config.php';
$socials=array();
?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    <sitemap>
        <loc><?php echo BASE_URL."sitemaps/main.xml" ?></loc>
    </sitemap>

    <sitemap>
        <loc><?php echo BASE_URL."sitemaps/all.xml" ?></loc>
    </sitemap>


    <?php
    $stmt=$conn->prepare("select * from sociall");
    $stmt->execute();
    $result = $stmt->get_result();

    while ($single_social = $result->fetch_assoc()) {
        array_push($socials,$single_social);
    }

    foreach ($socials as $social) {
        $social_loc = BASE_URL."sitemaps/".$social["e_name"].".xml";
        echo "<sitemap>
             <loc>$social_loc</loc>
             </sitemap>";
    }


    ?>



</sitemapindex>