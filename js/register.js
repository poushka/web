var where_to_go = 0; //define where to go after login succes ; 0 refresh current; 1=new page ; 2=panel
var login_location = 1;  // 1 define we are in login // 2 define wer are in register // 3 define we are in forget
var forget_level = 1;
var forget_mobile;
var forget_otp;

var register_level = 1;
var phone_stored = "";
var otp_stored = "";


$('#user_panel').click(function () {
    where_to_go = 1;
})

$('#user_panel_footer').click(function () {
    where_to_go = 1;
})



$('.btn_login').click(function () {
    var phone = $('#et_login_mobile').val();
    var password = $('#et_login_password').val();
    if (phone.length == 0) {
        setErrorForLogin(true, "لطفا شماره موبایل خود را وارد نمایید")
    } else if (password.length == 0) {
        setErrorForLogin(true, "لطفا رمز عبور خود را وارد نمایید")
    } else {

        $('#loading_login').show();
        setErrorForLogin(false, "")
        $.ajax({
            type: "POST",
            url: baseURl + "login"
            ,

            data: {
                'mobile': phone,
                'password': password

            },

            complete: function () {
                $('#loading_login').hide();
            },

            success: function (result, status, xhr) {


                if (where_to_go == 0) {
                    location.reload();
                } else if (where_to_go == 1) {
                    window.location = web_url + "dashbord";
                    }

            },

            error: function (xhr, status, error) {
                var json = JSON.parse(xhr.responseText);
                setErrorForLogin(true, json["message"])
            }
        });
    }

    function setErrorForLogin(show, message) {
        if (show) {
            $('#login_error').html(message);
            $('#login_error').slideDown(350);

        } else {
            $('#login_error').slideUp(350);
            $('#login_error').html("");
        }

    }


})  //login fuctio on btn

$('#tv_register').click(function () {


    if (login_location==1) {
        login_location=2;

        $(".login_main").slideUp("medium", function () {
            $(".login_main").removeClass("d-flex").addClass("d-none");
            $(".register_main").removeClass("d-none").addClass("d-flex").hide().slideDown("medium");
            $("#tv_login_header").html("ثبت نام در پوشکا");
            $("#lbl_login_register").html("حساب کاربری دارید ؟ ");
            $("#tv_register").html("ورود به حساب");


        });
    } else if (login_location==2) {
       login_location=1;
        $(".register_main").slideUp("medium", function () {
            $(".register_main").removeClass("d-flex").addClass("d-none");
            $(".login_main").removeClass("d-none").addClass("d-flex").hide().slideDown("medium");
            resetRegister();

        });
    }else if (login_location==3) {
        login_location=1;
        $(".forget_main").slideUp("medium", function () {
            $(".forget_main").removeClass("d-flex").addClass("d-none");
            $(".login_main").removeClass("d-none").addClass("d-flex").hide().slideDown("medium");
            resetRegister();

        });
    }


    // $('.register_main').removeClass("d-none").addClass("d-flex");


}) //hiding loading ad showing register

$('#btn_register').click(function () {


    var phone = $('#et_register_mobile').val();

    if (register_level == 1) {
        if (phone.length == 0) {
            setErrorRegister(true, "لطفا شماره موبایل خود را وارد نمایید")
        } else {
            setErrorRegister(false, "");
            setRegisterLoading(true);
            $.ajax({
                type: "POST",
                url: baseURl + "register"
                ,

                data: {
                    'mobile': phone,

                },

                complete: function () {
                    setRegisterLoading(false);
                },

                success: function (result, status, xhr) {
                    register_level = 2;
                    phone_stored = $("#et_register_mobile").val();
                    $("#register_message").html(result["message"]);
                    $("#et_register_mobile").val("").attr({
                        placeholder: "کد دریافت شده",
                        maxLength: 6
                    });
                    $("#et_register_icon").removeClass("fa-phone").addClass("fa-id-card");


                },


                error: function (xhr, status, error) {
                    var json = JSON.parse(xhr.responseText);
                    setErrorRegister(true, json["message"])
                }
            });
        }

    } else if (register_level == 2) {
        if (phone.length == 0) {
            setErrorRegister(true, "لطفا کد دریافت شده را وارد نمایید")
        } else {
            setErrorRegister(false, "");
            setRegisterLoading(true);


            $.ajax({
                type: "POST",
                url: baseURl + "verify"
                ,

                data: {
                    'mobile': phone_stored,
                    'otp': phone,
                    'password': null,
                    'moaref': null,

                },

                complete: function () {
                    setRegisterLoading(false);
                },

                success: function (result, status, xhr) {
                    register_level = 3;
                    otp_stored = $("#et_register_mobile").val();

                    $("#register_message").html(result["message"]);

                    $("#register_mobile_div").removeClass("d-flex").addClass("d-none");

                    $("#register_final_div").removeClass("d-none").addClass("d-flex").hide().slideDown("down");


                    $('#btn_register').html("  <span class=\"spinner-border spinner-border-sm mr-2\" id=\"loading_register\"></span>\n" +
                        "                        <i  class=\"fa fa-arrow-circle-left mr-2\" id=\"icon_register\"></i>\n" +
                        " تکمیل ثبت نام");

                    $('#icon_register').removeClass("fa-arrow-circle-left").addClass("fa-check-circle");


                },


                error: function (xhr, status, error) {

                    var json = JSON.parse(xhr.responseText);
                    setErrorRegister(true, json["message"])
                }
            });
        }


    } else if (register_level === 3) {
        var password_register = $("#et_register_password").val();
        var re_password_register = $("#et_re_register_password").val();
        var moaref = $("#et_moaref_password").val();
        console.log(password_register + " : " + re_password_register);


        if (password_register.length < 6) {
            setErrorRegister(true, "رمز عبور باید دارای حداقل 6 کاراکتر باشد")
        } else if (String(password_register) !== String(re_password_register)) {
            setErrorRegister(true, "رمز عبور و تکرار آن یکسان نمیباشد")
        } else {
            setRegisterLoading(true);
            $.ajax({
                type: "POST",
                url: baseURl + "verify",
                data: {
                    'mobile': phone_stored,
                    'otp': otp_stored,
                    'password': password_register,
                    'moaref': moaref,

                },
                complete: function () {
                    setRegisterLoading(false);
                },
                success: function (result, status, xhr) {
                    $("#register_message").html(result["message"]).addClass('text-bold text-green') +  " "+  "<i class='fa fa-badge-check'></i>"  ;

                    $("#icon_top_forget").removeClass("fa-phone").addClass("fa-redo")


                    setTimeout(function () {
                        if (where_to_go == 0) {
                            location.reload();
                        } else if (where_to_go == 1) {
                            window.location = web_url + "dashbord";
                        }

                    },1500)


                },
                error: function (xhr, status, error) {
                    var json = JSON.parse(xhr.responseText);
                    setErrorRegister(true, json["message"])
                }

            });
        }


    }


    function setErrorRegister(show, message) {
        if (show) {
            $('#register_error').html(message).slideDown(350);

        } else {
            $('#register_error').slideUp(350).html("");
        }

    }

    function setRegisterLoading(show) {
        if (show) {
            $('#icon_register').hide()
            $('#loading_register').show()

        } else {
            $('#icon_register').show()
            $('#loading_register').hide()
        }
    }


})  //register all levels on btn

$('#forget_div').click(function () {
    $(".login_main").slideUp("medium", function () {
        login_location=3;
        $(".login_main").removeClass("d-flex").addClass("d-none");
        $(".forget_main").removeClass("d-none").addClass("d-flex").hide().slideDown("medium");
        $("#tv_login_header").html("فراموشی رمز عبور");
        $("#tv_register").html("<i class='fa fa-arrow-left'></i> بازگشت به صفحه ورود")
        $("#lbl_login_register").hide();

    });
})

$('#btn_forget').click(function () {
    if (forget_level == 1) {
        forget_mobile = $('#et_top_forget').val();
        if (forget_mobile.length < 11) {
            showMessage(true, "شماره موبایل وارد شده صحیح نمیباشد", false, $('#forget_error'));
        } else {
            showMessage(false, "", false, $('#forget_error'));
            setForgetLoading(true)
            $.ajax({
                type: "POST",
                url: baseURl + "reqReset",
                data: {
                    'mobile': forget_mobile
                },
                complete: function () {
                    setForgetLoading(false)
                },
                success: function (result, status, xhr) {
                    forget_level = 2;
                    $('#message_forget').text("کد ارسال شده به شماره" + forget_mobile + " را وارد نمایید")
                    $('#et_top_forget').val("").attr("placeholder", "کد دریافت شده")
                    $('#icon_top_forget').removeClass('fa-phone').addClass('fa-barcode')
                    $('#btn_forget').html("     <span class=\"spinner-border spinner-border-sm mr-2\" id=\"loading_forget\"></span>\n" +
                        "                        <i  class=\"fa fa-arrow-left mr-2\" id=\"icon_forget\"></i>\n" +
                        "ارسال کد")
                },
                error: function (xhr, status, error) {
                    var json = JSON.parse(xhr.responseText);
                    showMessage(false, json["message"], false, $('#forget_error'));
                }


            })
        }
    } else if (forget_level == 2) {
        forget_otp = $('#et_top_forget').val();
        setForgetLoading(true)
        showMessage(false, "", false, $('#forget_error'));
        $.ajax({
            type: "POST",
            url: baseURl + "resetPassword",
            data: {
                'mobile': forget_mobile,
                'otp': forget_otp,
                'password': null
            },
            complete: function () {
                setForgetLoading(false)
            },
            success: function (result, status, xhr) {
                forget_level = 3;
                $('#message_forget').text("رمز عبور جدید خود را وارد نمایید")
                $('#et_top_forget').val("").attr({"placeholder": "رمز عبور", "type": "password"})
                $('#div_bottom_forget').removeClass('d-none').addClass('d-flex')
                $('#icon_top_forget').removeClass('fa-barcode').addClass('fa-lock')
                $('#btn_forget').html("     <span class=\"spinner-border spinner-border-sm mr-2\" id=\"loading_forget\"></span>\n" +
                    "                        <i  class=\"fal fa-check mr-2\" id=\"icon_forget\"></i>\n" +
                    "به روز رسانی ")


            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
                var json = JSON.parse(xhr.responseText);
                showMessage(true, json["message"], false, $('#forget_error'));
            }


        })
    } else if (forget_level == 3) {
        var password = $('#et_top_forget').val();
        var re_password = $('#et_bottom_forget').val();
        if (password.length < 6) {
            showMessage(true, "رمز عبور باید دارای حداقل 6 حرف باشد", false, $('#forget_error'));
        } else if (password !== re_password) {
            showMessage(true, "رمز عبور و تکرار آن یکسان نمیباشد", false, $('#forget_error'));
        } else {
            setForgetLoading(true)
            $.ajax({
                type: "POST",
                url: baseURl + "resetPassword",
                data: {
                    'mobile': forget_mobile,
                    'otp': forget_otp,
                    'password': password
                },
                complete: function () {
                    setForgetLoading(false)
                },
                success: function (result, status, xhr) {
                    showMessage(true, result["message"] , true, $('#forget_error'));
                    setTimeout(function () {
                        if (where_to_go == 0) {
                            location.reload();
                        } else if (where_to_go == 1) {
                            window.location = web_url + "dashbord";
                        }

                    },1500)

                },
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                    var json = JSON.parse(xhr.responseText);
                    showMessage(true, json["message"], false, $('#forget_error'));
                }
            })

        }
    }
})

function setForgetLoading(show) {
    if (show) {
        $('#icon_forget').hide()
        $('#loading_forget').show()

    } else {
        $('#icon_forget').show()
        $('#loading_forget').hide()
    }
}


$("#login_modal").on("hidden.bs.modal", function () {
    $('#et_login_mobile').val("");
    $('#et_login_password').val("");
    $(".login_main").removeClass("d-none").addClass("d-flex");
    $(".register_main").removeClass("d-flex").addClass("d-none");
    $('#login_error').html("").hide();
    $("#tv_login_header").html("ورود به حساب کاربری");
    $("#lbl_login_register").html("حساب کاربری ندارید؟ ");
    $("#tv_register").html(" ثبت نام");
    resetRegister();

}) // call back for function on closing login_modal

function resetRegister() {
    register_level = 1;
    inLogin = 1;
    phone_stored = "";
    otp_stored = "";
    $("#lbl_login_register").show();
    $("#tv_login_header").html("ورود به حساب کاربری");
    $("#lbl_login_register").html("حساب کاربری ندارید؟ ");
    $("#tv_register").html(" ثبت نام");

    $("#et_register_mobile").val("").attr({
        placeholder: "شماره موبایل",
        maxLength: 11
    });

    $("#register_message").html("شماره خود را برای دریافت پیامک ورود وارد نمایید");

    $("#et_register_icon").removeClass().addClass("fa fa-phone text-gray");

    $("#register_mobile_div").removeClass().addClass("d-flex align-items-center rtl form-group mr-3 ml-3 mr-lg-5 ml-lg-5");
    $("#register_final_div").removeClass().addClass("flex-column d-none");
    $('#register_error').hide();

    $('#btn_register').html("<span class=\"spinner-border spinner-border-sm mr-2\" id=\"loading_register\"></span>" +
        "<i  class=\"fa fa-arrow-circle-left mr-2\" id=\"icon_register\"></i>" +
        "مرحله بعد");

    $('#icon_register').removeClass().addClass("fa fa-arrow-circle-left mr-2");


    forget_level = 1;
    forget_mobile = "";
    forget_otp = "";
    where_to_go=0;

    $('.forget_main').removeClass("d-flex").addClass("d-none");
    $('#div_bottom_forget').removeClass('d-flex').addClass("d-none");
    $('#btn_forget').html(" <span class=\"spinner-border spinner-border-sm mr-2\" id=\"loading_forget\"></span>\n" +
        "                        <i  class=\"fa fa-redo mr-2\" id=\"icon_forget\"></i>\n" +
        "دریافت کد بازیابی");
    $('#message_forget').html("شماره خود را برای بازیابی رمز عبور وارد نمایید");
    $('#et_top_forget').attr({"type":"tel","placeholder":"شماره موبایل"})
    $('#icon_top_forget').removeClass().addClass("fa  fa-phone text-gray");


}
