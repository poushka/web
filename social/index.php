<?php

use GuzzleHttp\Exception\ClientException;
use Src\helper\Practical as MyPractical;
include '../helper/init.php';

$social_id = 1;
$cat_id = 0;
$main = 1;
$social_position = 0;
$current_social = null;

$response = $client->request('GET', 'getSC');
$socials_categories = json_decode($response->getBody(), true);
$socials = $socials_categories["socials"];
$categories= $socials_categories["categories"];

$new_page_modal = false ;  // it define that modal is opening from new page or user panel
$category_en = null;
$category_fa = null;


$has_screen = false; // to initilazie gallery if there is any picture
if (isset($_GET["social"]) && isset($_GET["id"])) {

    $social = $_GET["social"];
    $stmt = $conn->prepare("select social_id from sociall where e_name like ?");
    $stmt->bind_param("s", $social);
    $stmt->execute();
    $result_social = $stmt->get_result();
    if ($result_social->num_rows==0) {
        header('Location:'.BASE_URL."404");
    }
    $social_db = $result_social->fetch_assoc();
    $social_id = $social_db["social_id"];

    for ($i = 0; $i < count($socials); $i++) {
        if ($social_id == $socials[$i][SOCIAL_ID_SOCIAL]) {
            $social_position = $i;
            setcookie(SOCIAL_POSITION, $social_position, 2147483647, "/");
        }
    }
    $current_social = $socials[$social_position];


    $id = $_GET["id"];
    $user_id = 0;
    $comments = null;
    if (isset($_SESSION["user_id"])) {
        $user_id = $_SESSION["user_id"];
    }
    $st = $conn->prepare("select page_id from page where social_id = (select social_id from sociall where e_name like ?) and id = ? and active > 0");
    $st->bind_param("ss", $social, $id);
    $st->execute();
    $page_result = $st->get_result()->fetch_assoc();
    if ($page_result==null) {
        header('Location:'.BASE_URL."404");
    }


    //todo inja bayad bejaye 0 user id ro bezarim age logine ke user id age na 0
    $response = $client->request('GET', 'PDetail/' . $page_result["page_id"], [
        'query' => ["user_id" => $user_id]
    ]);
    $page_detail = json_decode($response->getBody(), true);




    $client->request('PUT', 'compress/' . $page_result["page_id"]);

    try {
        $response_comment = $client->request('GET', 'getComments/' . $page_result["page_id"], [
            'query' => ["last_id" => 0]
        ]);
        $comments = json_decode($response_comment->getBody(), true);
    } catch (GuzzleHttp\Exception\ClientException $e) {
        $response_tttt = $e->getResponse();
        $responseBodyAsString = $response_tttt->getBody()->getContents();
    }

    $rating = $page_detail["rating"];

    $myrate=$rating["myrate"];

    $og_image = IMG_URL.'chanel_pic/'.$page_detail["pic"];
    $og_url = BASE_URL ."social/".$social."/".$id;
    $title = $page_detail["prefix"] . " " . $page_detail["p_name"] . " ".$page_detail["name"] . " - " . "پوشکا";
    $defined_prefix = $page_detail["prefix"] == "صفحه" ? "پیج "  : $page_detail["prefix"] ." ";
    $pre_description = $defined_prefix  . $page_detail["p_name"] . " " . $page_detail["name"].  "|" . $page_detail["short_des"];

    $lenth_remaining = 156-mb_strlen($pre_description);
    $final_desc = mb_substr($page_detail["des"],0,$lenth_remaining);

    $description = $pre_description."|".$final_desc;

    $keywords = $defined_prefix  . $page_detail["p_name"] . " " . $page_detail["name"];
}else {
    header('Location:'.BASE_URL."404");
}
include '../m/header.php';



?>

<script src="js/rater.min.js"></script>

<!--top pictures and titles -->
<div class="container mt-3 rtl d-flex flex-column min-height-70 "
     xmlns:-webkit-box-shadow="http://www.w3.org/1999/xhtml"
     xmlns:box-shadow="http://www.w3.org/1999/xhtml">


    <div class="d-flex mr-3  text-success mt-lg-3  mb-1" >
        <a href="<?php echo BASE_URL?>"> <i class="fa fa-home navigation_color"></i></a>
        <span><i class="fa  fa fa-angle-left mr-1 ml-1 mt-1 my-black-color"></i> </span>
        <a class="navigation_color" href=<?php echo BASE_URL_M . $current_social[E_NAME_SOCIAL] ?>> <?php echo $current_social[P_NAME_SOCIAL] ?> </a>
        <span><i class="fa  fa fa-angle-left mr-1 ml-1 mt-1 my-black-color"></i> </span>
        <a class="navigation_color" href=<?php echo BASE_URL_M . $current_social[E_NAME_SOCIAL] . '/' . htmlspecialchars($page_detail['en_name'],ENT_QUOTES,"UTF-8")  ?>> <?php echo $page_detail["cat_name"] ?></a>
    </div>

    <!--row of top pictures and titles-->
    <div class="row mt-1 m-0 myltr" >

        <!--top image div-->
        <div class="col-md-6 col-lg-6 col-xl-6 col-sm-12 col-12 mt-2 p-0">

            <img class="img-page rounded"
                 src="<?php echo IMG_URL . "chanel_pic/" . $page_detail["pic"] ?>" alt="<?php echo 'تصویر ' . $page_detail['prefix'] . ' ' . $current_social[P_NAME_SOCIAL]
              . ' ' .  htmlspecialchars($page_detail['name'],ENT_QUOTES,'UTF-8') ?>"
            >

        </div>

        <div class="col-md-6 col-lg-6 col-xl-6 col-sm-12 col-12 mt-2 d-flex flex-column p-0 pl-md-2 pr-md-2 rtl ">





            <!--title and shor des-->
            <div class="">
                <div class="d-flex align-items-center justify-content-between flex-nowrap">
                    <h1 class="text-right mb-0 mt-2 w-100 myh1 mt-lg-3 mr-2 ml-2 text-bold"> <?php echo $current_social[PREFIX_SOCIAL] . " " . $current_social[P_NAME_SOCIAL] . " " .
                            htmlspecialchars($page_detail["name"],ENT_QUOTES,"UTF-8")
                        ?> </h1>


                    <div class="d-flex align-items-center mt-1">
                        <div class="d-flex flex-column justify-content-center align-items-center pl-2 mt-1">
                            <div class="text-bold">
                                <span id="rate_span" class="d-inline" style="font-size: 24px"><?php echo $rating["rate"]==null ? "0.0" :
                                        $rating["rate"] ?></span><span style="font-size: 12px; color: #d1d1d1" class="d-inline">/10
                                </span></div>
                            <hr class="bg-dark w-100 bg-hr mt-0 mb-0"/>
                            <span id="voters_span"><?php echo $rating["voters"]?></span>
                        </div> <!--rate div-->

                        <div class="ml-1 ml-xl-2" style="width: 1px ; height: 50px; background: #9c9c9c;"></div>

                        <div class="d-flex  align-items-center pl-1" id="star-div">

                        <i id="rate-img" class="<?php echo $myrate==null? "far":"star-color fa"?> fa-star text-gray fa-2x"></i>
                        <span class=" text-center" >
                            <span id="myrate-span"  class="<?php echo $rating["myrate"]==null ? "small-font" : "rate-font" ?>"><?php echo $myrate==null ? "رای" : $rating["myrate"]?></span>
                            <span id="myrate-label" class="small-font"><?php echo $rating["myrate"]==null ? "دهید" : "شما"?></span>
                        </span>
                        </div>  <!--star div-->
                    </div>
                </div>


                            <div class="d-none rating-bar align-items-center" data-visible="0" style="direction: ltr; font-size: 32px">
                            <div class="rate ltr"></div>
                                <div id="spinner-rate" class="d-none spinner-border spinner-border-sm star-color ml-2"></div>
                            </div>


                <h2 class="mr-2 mt-lg-2 mt-1 myh2 text-right text-regular short_des  align-self-end">
                    <?php echo htmlspecialchars($page_detail["short_des"],ENT_QUOTES,"UTF-8") ?> </h2>



            </div><!--title and shor des-->


            <div class="d-flex mt-4 mr-2 ">


            <div class="enter_page d-flex ">

                <?php  $join_id = $page_detail["id_2"] == null ?   $page_detail["id"] : $page_detail["id_2"] ?>

                <a href=<?php echo $current_social[WEB] . $join_id ?> target="_blank" rel="nofollow" class="d-flex pr-4 pl-4  pl-md-5 pr-md-5 justify-content-center align-items-center
            flex-grow-1 "
                >
                    <?php echo "ورود به  " . $current_social[PREFIX_SOCIAL] ?>
                </a>




            </div>
                <a href="#share_modal" data-toggle="modal" class="share_page d-flex justify-content-center align-items-center pr-4 pl-4  pl-md-5 pr-md-5  mr-2" data-share=<?php echo $page_detail["short_url"]?>>
                    اشتراک گذاری
                </a>


            </div>



            <!-- infoes-->


            <div class="d-flex justify-content-around  text-left  mt-4 mt-md-auto info_test "
            >


                <div class="w-20 pb-1 info pb-xl-2  d-flex flex-column text-right align-items-center rtl ">
                    <img class="pointer mt-1 mt-xl-2 img-like" id="like_img" data-like=<?php echo $page_detail["user_like"] ?>
                    src="assets/<?php echo $page_detail["user_like"] == 0 ? "unlike.png" : "like.png"; ?>"
                         data-id="<?php echo $page_result["page_id"] ?>" width=35px height=35px>
                    <span class=""> پسند شده</span>
                    <span class="text-center " id="tv_like_count"><?php echo $page_detail["likes_cnt"] ?></span>
                </div>


                <div class="w-20  pb-1 pb-xl-2 info d-flex flex-column text-right align-items-center rtl ">
                    <img class="mt-1  mt-xl-2 img-like" src="assets/ic_eye.svg" width=35px
                         height=35px>
                    <span class="">بازدید</span>
                    <span class="text-center "><?php echo $page_detail["view"] ?></span>

                </div>


                <div class="w-20 pb-xl-2 pb-1 info d-flex flex-column text-right align-items-center rtl ">
                    <img class="mt-1 mt-xl-2 img-like" src="assets/comment.svg" width=35px
                         height=35px>
                    <span class=""> نظرات</span>
                    <span class="text-center "><?php echo $page_detail["comments_cnt"] ?></span>
                </div>


                <div class="w-20  pb-1 pb-xl-2 info  d-flex flex-column text-right align-items-center rtl ">
                    <img class="mt-1 mt-xl-2 img-like" src="assets/follower.svg" width=35px
                         height=35px>
                    <span class=""> <?php echo $current_social[MEMBER_PREFIX_SOCIAL] ?></span>
                    <span class="text-center text-bold"><?php echo MyPractical::thousandsCurrencyFormat($page_detail["member"]) ?> </span>
                </div>

            </div>
        </div>  <!--top titles and infos-->

        <!--top titles and infos-->


    </div> <!--row of top pictures and titles-->


    <div class="row m-0" >

        <div class="col-lg-6 mt-4 rtl text-center p-0 pl-md-2 pr-md-2"
        >
            <ul class="w-100 p-0 mb-0  tabs text-bold  d-inline-flex align-self-start ">
                <li class="tab-link   w-25 current" data-tab="tab-1"> توضیحات</li>
                <li class="tab-link   w-25" data-tab="tab-2"> تصاویر</li>
                <li class="tab-link   w-25" data-tab="tab-3">نظرات</li>
                <li class="tab-link   w-25" data-tab="tab-4"> ویدیو</li>

            </ul> <!--tabs ul and lis-->

        </div>
    </div>


    <div id="tab-1" class="tab-content col-lg-6  current p-0 w-100">

        <p style="white-space: pre-wrap; color: #000000" class="mr-md-2 ml-md-2 mt-2  my-black-color mb-2"><?php echo htmlspecialchars($page_detail["des"],ENT_QUOTES,"UTF-8")  ?></p>

    </div>  <!--tab 1 description-->


    <div id="tab-2" class="tab-content p-0 mt-0 w-100">


        <?php


        if (count($page_detail["screens"]) > 0) {
            echo " <div id=\"owl-demo\" class=\"owl-carousel  gallery mt-3 owl-theme p-0\">";

            $has_screen = true;
            $alt = $current_social[PREFIX_SOCIAL] . " " . $current_social[P_NAME_SOCIAL] . " " .
                htmlspecialchars($page_detail["name"],ENT_QUOTES,"UTF-8");

            foreach ($page_detail["screens"] as $screen) {
                $screen_address = IMG_URL . "chanel_screen/" . $screen["name"];
                $screen_address_thumb = IMG_URL . "chanel_screen/" . $screen["thumb_name"];
                echo " <div class='mr-1 pr-1 pl-1 w-100'>
                    <a href=$screen_address class='mr-1 pr-1 pl-1' rel=\"rel1\" >
                    
                    <img class='rounded img-screen fit-cover'   src=$screen_address_thumb alt=$alt  >
                     </a>
                    </div>
                   
                  ";

            }

            echo "</div>";

        } else {

            echo "<div class='d-flex flex-column w-100 justify-content-center align-items-center' > 
               <i class='fal mt-5 fa-3x fa-ban'></i>
               <span class='mt-3 mb-1 mt-md-2 mb-md-2 pl-2 pr-2 text-center'>هیچ تصویری اضافه نشده است</span>
                </div>";
        }


        ?>




    </div> <!-- tab 2 screens-->

    <div id="tab-3" class="tab-content p-0">  <!-- tab 3 comments -->

        <div id='comments_container'>
            <?php

            if ($comments == null) {
                echo "<div class='d-flex flex-column w-100 justify-content-center align-items-center' > 
               <i class='fa mt-4 fa-3x fa-comment-slash'></i>
               <span class='mt-1 mb-1 mt-md-2 mb-md-2 pl-2 pr-2 text-center' > هیچ نظری ثبت نشده است ! </span>
                </div>";
            }
            else {


                foreach ($comments as $single_comment) {
                    $profile = $single_comment["thumb"] == null ? BASE_URL . "assets/profile.svg" :
                        $profile = IMG_URL . "profile/" . $single_comment["thumb"];

                    $display = $single_comment["reply"] == null ? "d-none" : "d-flex";
                    $no_profile = "assets/comment.svg";
                    $comment_text = htmlspecialchars($single_comment["text"],ENT_QUOTES,"UTF-8");
                    $comment_reply = htmlspecialchars($single_comment["reply"],ENT_QUOTES,"UTF-8");
                    $username = $single_comment["username"] == null ? "بدون نام " : htmlspecialchars($single_comment["username"]);
                    echo "
            <div class='parent-comment w-100 mt-2'  data-id='$single_comment[comment_id]'>
            <div class='d-flex justify-content-start  rtl comment norma-line-height' >
                <a href='#'>
                    <img src='$profile' width='60px' height='60px' class='img-com' >
                </a>
                <div class=' w-100 d-flex flex-column'>
                    <div class='d-flex rtl'>
                        <span class='text-bold text-medium'>$username</span>
                        <span class='mr-auto ml-2'>$single_comment[persian_time]</span>
                    </div>
                    <span class='text-right mr-2 mt-2'> $comment_text </span>
                    <span   data-id='$single_comment[comment_id]' data-admin='0' class='ml-2 pointer align-self-end text-danger mt-2 report_cm'> 
                     <i class='fa fa-flag'></i> گزارش تخلف</span>
                </div>
            </div>
            <div class='$display justify-content-start rtl replay mr-5 mt-2  pr-lg-5  pr-xl-5 pr-md-3 pr-sm-2 pr-1 norma-line-height' >
                <a href='#'>
                    <img src='assets/pic_63587577.jpg' width='60px' height='60px' class='img-com' >
                </a>
                <div class=' w-100 d-flex flex-column'>
                    <div class='d-flex  rtl'>
                        <span class='text-bold text-medium'>مدیر $current_social[prefix] </span>
                        <span class='ml-2 mr-auto'>$single_comment[persian_time_reply]</span>
                    </div>
                    <span class='text-right mr-2 mt-2'> $comment_reply </span>
                    <span  data-id='$single_comment[comment_id]' data-admin='1' class='pointer ml-2 align-self-end text-danger mt-2 '> 
                     <i class='fa fa-flag'></i> گزارش تخلف</span>
                </div>
            </div>
        </div> ";
    

                }

                //show load more if there are 5 comment


            }

            ?>
        </div>

        <?php


            if (count($comments)==15) {
                echo " <div  class='d-flex justify-content-center mt-3 '>
                    <span id='btn_more' class='load-more d-flex align-items-center'>
                    نمایش بیشتر نظرات
                    </span>
  
                      </div>";
            }

        ?>



        <?php
        if (isset($_SESSION[LOGIN]) && $_SESSION[LOGIN] === true) {
            ?>

            <a href="#coment_modal" data-toggle="modal" id="lilili"
               class='btn btn-success mt-3 mr-3 comment_btn mr-auto d-flex align-items-center justify-content-center'>
                <img src='assets/pen.svg' width='28' height='28'> </a>


        <?php } else { ?>
            <a href="#login_modal" data-toggle="modal"
               class='btn btn-success mt-3 mr-3 comment_btn mr-auto d-flex align-items-center
               justify-content-center'>
                <img src='assets/pen.svg' width='28' height='28'> </a>

        <?php }

        ?>


    </div>   <!--tab 3 comments-->


    <div id="tab-4" class="tab-content p-0">  <!-- tab 4 video -->

        <?php
        if ($page_detail["aparat"]==null) {
            echo "<div class='d-flex flex-column w-100 justify-content-center align-items-center' > 
               <i class='fa mt-4 fa-3x fa-video-slash'></i>
               <span class='mt-1 mb-1 mt-md-2 mb-md-2 pl-2 pr-2 text-center' >  ویدیو ثبت نشده است! </span>
                </div>";
        }else {
            echo "<div class=\"h_iframe-aparat_embed_frame\"><span class='video'></span><iframe
      src=$page_detail[aparat] allowFullScreen=\"true\" webkitallowfullscreen=\"true\" 
       mozallowfullscreen=\"true\"></iframe></div>";
        }

        ?>



    </div>   <!--tab 4 video-->

    <div class="d-flex flex-wrap align-items-center mt-4">
        <span class="bg_tag mt-2"> <i class="fa fa-tags "></i> برچسب ها </span>
        <?php
        foreach ($page_detail["tags"] as $tag) {
            echo "<span class='mr-2 bg_tags mt-2'> $current_social[prefix] $tag[name]</span>";
        }
        ?>
    </div> <!--tags-->




    <?php
    foreach ($page_detail["reserved"] as $reserved) {

        echo "<main class=\"grid-item container main mt-4 \"  >
    <div class=\"d-flex list-top justify-content-between align-items-center rtl\" >
        <h2 class=\"mr-3 section_title\">$reserved[title]</h2>    
   
    </div>
    
    <div  class=\" mt-2 text-right owl-carousel pt-1 myowl\"  >
    ";

        $pages = $reserved["pages"];

        foreach ($pages as $single_page) {
            $name = htmlspecialchars($single_page["name"],ENT_QUOTES,"UTF-8");
            $member = MyPractical::thousandsCurrencyFormat($single_page["member"]);
            $member_prefix = $single_page["member_prefix"];
            $page_icon = IMG_URL . "chanel_pic/" . $single_page["thumb"];
            $page_alt = $current_social[PREFIX_SOCIAL] . " " . $current_social[P_NAME_SOCIAL] . " " . htmlspecialchars($single_page["name"],ENT_QUOTES,"UTF-8")  ;
            $social_address = BASE_URL . "social/" . $current_social[E_NAME_SOCIAL] . "/" . $single_page["id"];
            $short_des = htmlspecialchars($single_page["short_des"],ENT_QUOTES,"UTF-8");

            echo "
        <div  class=\"item \">
                <a href=$social_address>
               <img  src=$page_icon class=\"item-img fit-cover\" ' alt=\"$page_alt\">
               </a> " ;

            echo '<img src="assets/special.png" alt="" class="special_img">';


            echo "  
               <div class='d-flex flex-column'>
               <a href=$social_address 
               class=\"item-title mt-1\" >$name</a>
                <span>$short_des</span>
               <p class=\"item-member align-self-end mb-0\">$member  $member_prefix</p>
               </div>
                       </div>
        ";

        }
        echo "
            </div>
            
        
         
              <hr class='hr-bg'>
              </main>
    ";



    }
    ?>






    <div class="tab-content d-flex flex-column rtl justify-content-start">
        <span> <i class="fa fa-info "> </i> کاربر گرامی پوشکا مسئولیتی در قبال محتوای قرار گرفته شده در این صفحه ندارد و صرفا بستری برای معرفی کانال ها و صفحات میباشد </span>
        <div class="mt-2">
            <span  class="pointer text-danger mt-3" id="tv_report_page" ><i class="fa fa-flag "></i> <?php echo "گزارش " . $current_social[PREFIX_SOCIAL] ?> </span>

            <span id="tv_send_message" class="mr-3 pointer text-dark mt-3"><i class="fa fa-envelope"></i> ارسال پیام </span>
        </div>

    </div>

</div> <!--top pictures and titles -->


<div class="modal fade" id="coment_modal">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header d-flex  justify-content-center align-items-center">
                <h5 class="text-medium""><?php echo "نظر شما در مورد " . $current_social[PREFIX_SOCIAL] . " " . htmlspecialchars($page_detail["name"],ENT_QUOTES,"UTF-8") ?></h5>
            </div>

            <!-- Modal body -->
            <div class="modal-body d-flex flex-column">
                <div class="d-flex  rtl form-group login_register">
                    <textarea class="form-control " rows="3" id="et_comment"
                              placeholder="نظر شما"></textarea>
                </div>
                <span class="text-center d-none align-self-center" id="comment_message"> نظر شما ثبت شد</span>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer my_background d-flex comment-body">

                <a class="text-danger mr-3 text-bold small-font pr-2 pl-2" data-dismiss="modal"> لغو</a>
                <div class="mr-2 comment-footer" ></div>
                <div class="spinner-border spinner-border-sm myspinner d-none" id="comment_spiiner"></div>
                <a class="text-success mr-3 text-bold small-font" id="tv_send_comment">ارسال کردن </a>


            </div>

        </div>
    </div>


</div> <!--comment_modal-->



<div class="modal fade" id="report_cm_modal">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">

            <div class="modal-header d-flex my_background justify-content-center align-items-center">
                <h5 class="small-font">آیا از گزارش تخلف این نظر مطمعن میباشید ؟</h5>
            </div>

            <!-- Modal body -->
            <div class="modal-body my_background d-flex flex-column">
                <div class="d-flex  rtl form-group align-items-center">
                    <button id="btn_report_cm" class="btn btn-success">گزارش نظر</button>
                    <button id="btn_cancel_report" class="btn btn-danger mr-2 pr-3 pl-3">لفو کردن </button>
                    <span id="spinner_report" class="spinner-border spinner-border-sm mr-2 d-none"></span>
                </div>
                <span id="tv_report_cm_message" class="mt-2 d-none text-center">ارسال گزارش انجام شد</span>

            </div>
        </div>
    </div>
</div> <!--report_cm_modal-->
<div class="modal fade" id="report_page_modal">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">

            <div class="modal-header d-flex bg-white justify-content-center align-items-center">
                <h5 class=""><?php echo "گزارش تخلف " . $current_social[PREFIX_SOCIAL] . " " .htmlspecialchars($page_detail["name"],ENT_QUOTES,"UTF-8")?></h5>
            </div>

            <!-- Modal body -->
            <div class="modal-body bg-white  d-flex flex-column p-0 report_body">
                <?php
                foreach ($page_detail["reports"] as $report) {
                    echo  "<div data-id='$report[rt_id]' class='d-flex report_page pointer pt-2 pb-2 rtl align-items-center'>
                    <i class='far fa-circle mr-2' data-id='1'></i>
                    <span class='text-right mr-2' id='$report[rt_id]'>$report[text]</span>
                    </div>";
                }

                ?>

                <span id="tv_report_page_message" class="mt-2 d-none text-center">ارسال گزارش انجام شد</span>
                <div class="d-flex rtl mt-4 my_background pt-2 pb-2">
                    <button id="btn_submit_report_page" class="btn btn-success d-flex align-items-center mr-2">  ارسال گزارش
                    </button>

                    <button id="btn_cancel_report_page" class="btn btn-danger mr-2 ">لغو کردن</button>
                </div>

            </div>
        </div>
    </div>
</div> <!--report_page_modal-->
<div class="modal fade" id="replay_modal" >
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">

            <div class="modal-header d-flex  justify-content-center align-items-center">
                <h5 class="ltr" id="tv_title">ارسال پیام به مدیر <?php  echo $current_social[PREFIX_SOCIAL] ." " .htmlspecialchars($page_detail["name"],ENT_QUOTES,"UTF-8") ?></h5>

            </div>

            <!-- Modal body -->
            <div class="modal-body d-flex flex-column">

                <div class="d-flex  rtl form-group login_register">
                    <input type="text" class="form-control"  id="et_title"
                           placeholder="عنوان پیام">
                </div>
                <div class="d-flex  rtl form-group login_register">
                    <textarea class="form-control " rows="3" id="et_replay"
                              placeholder="متن پیام"></textarea>
                </div>

                <span class="text-center d-none" id="replay_message">پیام شما ارسال گردید </span>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer rtl my_background low_gray d-flex justify-content-start comment-body">

                <a class="text-success pointer mr-3 text-bold small-font" id="tv_send">ارسال کردن </a>
                <span class="spinner-border text-success spinner-border-sm mr-2 d-none" id="loading_replay"></span>
                <div class="mr-2 comment-footer" ></div>
                <a class="text-danger pointer mr-3 text-bold small-font pr-2 pl-2" data-dismiss="modal"> لغو</a>

            </div>

        </div>
    </div>


</div> <!--replay_modal-->

<div class="modal fade" id="share_modal" >
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body d-flex flex-column">
                <span class="text-center mt-2 text-bold my-black-color">اشتراک گذاری</span>
             <a href="<?php echo BASE_URL . "me/". $page_detail["short_url"]?>"
                class="mt-4 d-flex share_page pl-2 align-items-center"><?php echo BASE_URL . "me/". $page_detail["short_url"]?></a>
             <span data-share="<?php echo BASE_URL . "me/". $page_detail["short_url"] ?>" class="enter_page mt-2 d-flex align-items-center justify-content-center pointer" id="copy_share">کپی کردن لینک</span>
             <span class="pointer close_dialog d-flex justify-content-center align-items-center mt-2"
                   data-dismiss="modal">بستن </span>

            </div>

        </div>
    </div>


</div> <!--replay_modal-->

<?php include "../footer.php"; ?>
</body>

<script src="js/owl.carousel.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<!--<script src="js/bootstrap.min.js"></script>-->

<script src="js/script.js"></script>
<script src="js/register.js"></script>
<script src="js/simple-lightbox.jquery.min.js"></script>

<script>
    $(document).ready(function () {
        //to initilaze screen if there is any screen !!!
        var hsa_screen = "<?php echo $has_screen?>";
        var is_login = "<?php echo isset($_SESSION[LOGIN]) ?>";
        var apikey = "<?php echo isset($_SESSION['apikey']) ? $_SESSION['apikey'] : ''; ?>";
        var comment_id ;
        var admin ;
        var report_page_id = 0 ;
        var page_id = <?php echo $page_result["page_id"] ?>;
        var prefix = "<?php echo $current_social[PREFIX_SOCIAL] ?>";
        var admin_id = <?php echo $page_detail["user_id"] ?>;


        if (hsa_screen) {
            $('.gallery a').simpleLightbox({});

        }

        var on_comments = false;
        var more_comments = <?php echo $comments == null ? 'false' : 'true'?> ;
        var loading_comments = false;

        $('#copy_share').click(function () {
            var short_url = $(this).attr('data-share')
            var dummy = $('<input>').val(short_url).appendTo('body').select()
            document.execCommand('copy')
            dummy.remove();
            $('#share_modal').modal('hide');

        })

        //handlle tab clicls
        $('ul.tabs li').click(function () {
            var tab_id = $(this).attr('data-tab');
            if (tab_id === "tab-2") {
                on_comments = true;
            } else {
                on_comments = false;
            }

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');
            $(this).addClass('current');
            $("#" + tab_id).addClass('current');

        })

        //handle screens with owl

        $("#owl-demo").owlCarousel({
            loop: false,
            rtl: true,
            margin: 0,
            nav: true,
            navText: [
                '<div class="d-flex align-items-center justify-content-center navigation"> <i class="fa fa-angle-right " ></i></div>',
                '<div class="d-flex align-items-center justify-content-center navigation"> <i class="fa fa-angle-left " ></i></div>'
            ],
            responsive: {
                0: {
                    items: 2
                },
                550: {
                    items: 3
                },
                768: {
                    items: 4
                },
                1000: {
                    items: 5
                }
            }
        });
        $(".myowl").owlCarousel({

            loop: false,
            rtl: true,
            /* margin: 0,*/
            nav: true,
            navText: [
                '<div class="d-flex align-items-center justify-content-center navigation"> <i class="fa fa-angle-right " ></i></div>',
                '<div class="d-flex align-items-center justify-content-center navigation"> <i class="fa fa-angle-left " ></i></div>'
            ],

            dots: false,
            margin: 10,
            stagePadding: 10,
            autoPlay: 1000,
            responsive: {
                0: {
                    items: 2
                },
                450: {
                    items: 3
                },

                768: {
                    items: 4
                },
                992: {
                    items: 5
                },

                1000: {
                    items: 6
                }

            }
        });



        //handle like function
        $('#like_img').click(function () {
            var login =  <?php  echo isset($_SESSION[LOGIN]) && $_SESSION[LOGIN] ? 'true' : 'false'?>;
            if (login) {
                var like_state = $(this).attr("data-like");
                var page_id = $(this).attr("data-id");

                var user_id = <?php echo isset($_SESSION['user_id']) ? $_SESSION["user_id"] : "''";?>

                if (like_state == 0) {
                    $.ajax({
                        type: "POST",
                        url: baseURl + "likePage/" + page_id,

                        headers: {
                            "Authorization": apikey
                        }
                        ,
                        success: function (result, status, xhr) {
                            var like_count = $('#tv_like_count').html();
                            like_count++;
                            $('#tv_like_count').html(like_count);

                            $('#like_img').attr("src", "assets/like.png").attr("data-like", 1);
                        },
                        error: function (xhr, status, error) {
                            var json = JSON.parse(xhr.responseText);
                            alert(json["message"]);

                        }


                    });


                } else {

                    $.ajax({
                        type: "DELETE",
                        url: baseURl + "unlikePage/" + page_id,

                        headers: {

                            "Authorization": apikey
                        }
                        ,
                        success: function (result, status, xhr) {
                            var like_count = $('#tv_like_count').html();
                            like_count--;
                            $('#tv_like_count').html(like_count);
                            $('#like_img').attr("src", "assets/unlike.png").attr("data-like", 0);

                        },
                        error: function (xhr, status, error) {
                            var json = JSON.parse(xhr.responseText);
                            alert(json["message"]);

                        }


                    });


                }
            } else {
                where_to_go=0;
                $('#login_modal').modal('show');

            }
        })

        $("#tv_send_comment").click(function () {
            if (!$.trim($("#et_comment").val())) {
                commentMessage(true, false, "متن نظر نمیتواند خالی بماند")
            } else {
                var page_id = <?php echo $page_result["page_id"] ?> ;
                var text = $("#et_comment").val();
                var apikey = "<?php echo isset($_SESSION['apikey']) ? $_SESSION['apikey'] : ''; ?>";
               commentMessage(false, false, '');
                $('#comment_spiiner').removeClass('d-none').addClass('d-inline-block');
                $.ajax({
                    type: "post",
                    url: baseURl + "commentPage/" + page_id,
                    headers: {
                        'Authorization': apikey
                    },
                    data: {
                        'text': text
                    },
                    complete: function () {
                        $('#comment_spiiner').removeClass('d-inline-block').addClass('d-none');
                    },
                    success: function (result, status, xhr) {
                        commentMessage(true, true, result["message"])
                        setTimeout(function () {
                            $('#coment_modal').modal('hide');
                        }, 2500);
                    },
                    error: function (xhr, status, error) {
                        var json = JSON.parse(xhr.responseText);
                        commentMessage(true, json["message"])
                    }


                })
            }
        })

        $("#coment_modal").on("hidden.bs.modal", function () {
            commentMessage(false, false, false);
            $('#comment_spiiner').removeClass('d-inline-block').addClass('d-none');
            $('#et_comment').val('');

        })


        function commentMessage(show,success,message) {
            if (show) {
                $('#comment_message').removeClass('d-none').text(message)
                if (success) {
                    $('#comment_message').css('color','#66ff2b')
                } else {
                    $('#comment_message').css('color','#ff1e1a')
                }

            } else {
                $('#comment_message').addClass('d-none')
            }

        }

        //load more comments function
        $('#btn_more').click(function () {

            if (more_comments && !loading_comments) {

                var last_id = $('.parent-comment:last').attr('data-id');
                commentsLoading(true);
                $.ajax({
                    type: "GET",
                    url: baseURl + "getComments/" +<?php echo $page_result["page_id"] ?> ,

                    data: {
                        "last_id": last_id
                    }
                    ,
                    complete: function () {
                        commentsLoading(false);
                    },
                    success: function (result, status, xhr) {

                        $.each(result, function (index, element) {
                            var profile = element["thumb"] == null ? "<?php echo BASE_URL . "assets/profile.svg"?>" :
                                "<?php echo IMG_URL . "profile/" ?>" + element["thumb"];
                            var display = element["replay"] == null ? "d-none" : "d-flex";
                            var current_social = "<?php echo $current_social[PREFIX_SOCIAL] ?>";
                            var username = element["username"] == null ? "بدون نام " : escapeHtml(element["username"]);

                            var div = "<div class='parent-comment w-100 mt-2'  data-id='" + element['comment_id'] + "'>" +
                                "<div class='d-flex justify-content-start  rtl comment norma-line-height' >" +
                                "<a href='#'>" +
                                "<img src='" + profile + "' width='60px' height='60px' class='img-com' >" +
                                "</a>" +
                                "<div class=' w-100 d-flex flex-column'>" +
                                "<div class='d-flex rtl'>" +
                                "<span class='text-bold text-medium'>" + username + "</span>" +
                                "<span class='mr-auto ml-2'>" + element.persian_time + "</span>" +
                                "</div>" +
                                "<span class='text-right mr-2 mt-2'> " + escapeHtml(element['text'])  + "</span>" +
                                "<a href='' data-id='" + element['comment_id'] + "' class=' ml-2 align-self-end text-danger mt-2'>  <i class='fa fa-flag'></i> گزارش تخلف</a>" +
                                "</div>" +
                                "</div >" +
                                "<div class='" + display + " justify-content-start rtl replay mr-5 mt-2  pr-lg-5  pr-xl-5 pr-md-3 pr-sm-2 pr-1 norma-line-height' >" +
                                "<a href='#'>" +
                                "<img src='assets/profile.svg' width='60px' height='60px' class='img-com' " +
                                "</a>" +
                                "<div class=' w-100 d-flex flex-column'>" +
                                "<div class='d-flex  rtl'>" +
                                " <span class='text-bold text-medium'>مدیر " + current_social + " </span>" +
                                "<span class='ml-2 mr-auto'>"+element.persian_time_reply+"</spa" +
                                " </div>" +
                                "<span class='text-right mr-2 mt-2'> $single_comment[reply] </span>" +
                                "<a href='#' class=' ml-2 align-self-end text-danger mt-2'>  <i class='fa fa-flag'></i> گزارش تخلف</a>" +
                                "  </div>" +
                                "</div>" +
                                "</div>";

                            $("#comments_container").append(div);

                        });


                    },
                    error: function (xhr, status, error) {

                        var json = JSON.parse(xhr.responseText);
                        if (xhr.status === 404) {
                            $('#btn_more').removeClass('d-flex').addClass('d-none')
                            more_comments = false;
                        }

                    }

                })


            }


        })

        $('.report_cm').click(function () {

            if (is_login) {
                comment_id = $(this).attr('data-id');
                admin = $(this).attr('data-admin');

                $('#report_cm_modal').modal('show');

            } else {
                where_to_go =0; // flat for refresh the page after login defined in register
                $('#login_modal').modal('show');
            }
        })



        //submit cm report
        $('#btn_report_cm').click(function () {

            $('#spinner_report').removeClass('d-none');
            $.ajax({
                type: "POST",
                url: baseURl + "reportCm/" + comment_id,
                data : {
                    "admin":admin
                },
                headers: {
                    "Authorization": apikey
                },


                complete : function() {
                    $('#spinner_report').addClass('d-none');
                },

                success: function (result, status, xhr) {

                    showMessage(true,result["message"],true,$('#tv_report_cm_message'))
                },
                error: function (xhr, status, error) {
                    var json = JSON.parse(xhr.responseText);
                    showMessage(true,json["message"],false,$('#tv_report_cm_message'))

                }


            });

        })
        //cancel cm report
        $('#btn_cancel_report').click(function () {
            $('#report_cm_modal').modal('hide');
        })

        //call back for comment modal close to hide message if exists
        $('#report_cm_modal').on("hidden.bs.modal",function () {
            showCmReportMessage(false,"",false);
        })

        //open page report modal
        $('#tv_report_page').click(function () {
            if (is_login) {

                $('#report_page_modal').modal('show');
                var report_page_id = 0 ;

            } else {
                where_to_go =0; // flat for refresh the page after login defined in register
                $('#login_modal').modal('show');
            }
        })

        //handle report page click item
        $('.report_page').click(function () {

            $('.report_page i').each(function(){

                $(this).removeClass('fa text-green').addClass('far');
            });
            report_page_id = $(this).attr('data-id');
            $(this).find("i").removeClass('far').addClass('fa text-green')

        })

        //cancel report
        $('#btn_cancel_report_page').click(function () {
            $('#report_page_modal').modal('hide');
        })

        //submit page report
        $('#btn_submit_report_page').click(function () {
            if (report_page_id==0) {
                showMessage(true,"هیچ گزینه ای انتخاب نشده است !",false,$('#tv_report_page_message'))
            } else {
                console.log(report_page_id);
                $.ajax({
                    type: "POST",
                    url: baseURl + "reportPage/" + page_id,
                    data : {

                        "rt_id":report_page_id,
                        "prefix":prefix

                    },
                    headers: {
                        "Authorization": apikey
                    },

                    complete : function() {
                        $('#spinner_report').addClass('d-none');
                    },

                    success: function (result, status, xhr) {

                        showMessage(true,result["message"],true,$('#tv_report_page_message'))
                    },
                    error: function (xhr, status, error) {
                        console.log(xhr.responseText)
                        var json = JSON.parse(xhr.responseText);
                        showMessage(true,json["message"],false,$('#tv_report_page_message'))

                    }


                });
            }
        })

        //on report page  modal close litner
        $('#report_page_modal').on("hidden.bs.modal",function () {
            showPageReportMessage(false,"",false);
        })


        //open page report modal
        $('#tv_send_message').click(function () {
            if (is_login) {
                $('#replay_modal').modal('show');
                var report_page_id = 0 ;
            } else {
                where_to_go =0; // flat for refresh the page after login defined in register
                $('#login_modal').modal('show');
            }
        })

        $('#tv_send').click(function () {
            var title =  $('#et_title').val();
            var message = $('#et_replay').val();
            if (title.length==0) {

                showMessage(true,"عنوان پیام خالی میباشد",false,$('#replay_message'))
            } else if (message.length==0) {
                showMessage(true,"متن پیام خالی میباشد",false,$('#replay_message'))
            } else {

                showMessage(false,false,"",$('#replay_message'))
                $('#loading_replay').removeClass("d-none")
                $.ajax({
                    type: "POST",
                    url: baseURl + "sendMessage",
                    headers: {
                        'Authorization': apikey
                    },
                    data: {
                        'reciver_id': admin_id ,
                        'title' : title ,
                        'message' : message
                    },

                    complete: function () {
                        loading = false;
                        $('#loading_replay').addClass("d-none")
                    },

                    success: function (result, status, xhr) {
                        $('#et_title').val("");
                        $('#et_replay').val("");
                        showMessage(true,result["message"],true, $('#replay_message'))
                    },


                    error: function (xhr, status, error) {

                        var json = JSON.parse(xhr.responseText);
                        showMessage(true,json["message"],false, $('#replay_message'))

                    }


                });

            }
        })


        var myrate = <?php echo  $rating["myrate"]==null ? 0 : $rating["myrate"]?>;

        var options_rate =  {
            max_value:10,
            step_size:1.0,
            initial_value: myrate
        }

        $(".rate").rate(options_rate);
        $("#star-div").click(function () {
            var rating_visible = $('.rating-bar').attr("data-visible")
            if (rating_visible==0) {
                $('.rating-bar').removeClass('d-none').addClass('d-flex').hide().slideDown('medium', function () {
                    $('.rating-bar').attr("data-visible",1)
                })

            } else {

               $('.rating-bar').slideUp('medium',function () {
                    $(this).removeClass('d-flex').addClass('d-none')
                   $('.rating-bar').attr("data-visible",0)
               })
            }
        })


        $(".rate").on("change", function(ev, data){

            var login =  <?php  echo isset($_SESSION[LOGIN]) && $_SESSION[LOGIN] ? 'true' : 'false'?>;
            if (login) {
                var rate = data.to;
                $('#spinner-rate').removeClass('d-none');
                $.ajax({
                    type: "POST",
                    url: baseURl + "ratePage/"+page_id,
                    headers: {
                        'Authorization': apikey
                    },
                    data: {
                        'rate': rate

                    },

                    complete: function () {
                        $('#spinner-rate').addClass('d-none');
                    },

                    success: function (result, status, xhr) {
                         var rating_result = result["rating"];
                         $('#myrate-span').removeClass('small-font').addClass('rate-font').text(rating_result["myrate"])
                         $('#myrate-label').text("شما")
                         $('#rate-img').removeClass('far').addClass('fa star-color');
                         $('#rate_span').text(rating_result["rate"]);
                         $('#voters_span').text(rating_result["voters"]);
                         $('.rating-bar').slideUp('medium',function () {
                            $(this).removeClass('d-flex').addClass('d-none')
                            $('.rating-bar').attr("data-visible",0)
                        })

                    },


                    error: function (xhr, status, error) {
                        var json = JSON.parse(xhr.responseText);
                        alert(json["message"])

                    }
                })

            } else {
                where_to_go=0;
                $('.rating-bar').slideUp('medium',function () {
                    $(this).removeClass('d-flex').addClass('d-none')
                    $('.rating-bar').attr("data-visible",0)
                })
                $('#login_modal').modal('show');


            }


        });

        function commentsLoading(show) {
            if (show) {
                loading_comments = true;
                $('#btn_more').html(' نمایش بیشتر نظرات\ <span class=\'spinner-border spinner-border-sm mr-2\'></span>');
            } else {
                loading_comments = false;
                $('#btn_more').html('نمایش بیشتر نظرات');
            }
        }




    })
</script>


</html>