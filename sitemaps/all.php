<?php
header("Content-type: application/xml; charset=utf-8");
echo '<?xml version="1.0" encoding="UTF-8" ?>'.PHP_EOL;
include '../helper/DbConnection.php';
include '../helper/config.php';
$socials=array();
$categories = array();

$stmt = $conn->prepare("select e_name from sociall where active=1");
$stmt->execute();
$result = $stmt->get_result();
while ($single_social = $result->fetch_assoc()) {
    array_push($socials,$single_social);
}


$categories = array();
$stmt = $conn->prepare("select en_name from category where active=1");
$stmt->execute();
$result = $stmt->get_result();
while ($single_cat = $result->fetch_assoc()) {
    array_push($categories,$single_cat);
}



?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    <?php
    foreach ($socials as $social) {
        $maxMember = BASE_URL."all/".$social["e_name"]."/maxMember";
        $newst = BASE_URL."all/".$social["e_name"]."/newest";

        echo "<url>
        <changefreq>weekly</changefreq>
        <loc>
        $maxMember
        </loc>
        <priority>1.0</priority>
    </url>
    <url>
        <changefreq>weekly</changefreq>
        <loc>
        $newst
        </loc>
        <priority>1.0</priority>
    </url>
    
    ";


     foreach ($categories as $category) {
         $maxMember_category = BASE_URL."all/".$social["e_name"]."/". $category["en_name"]. "/maxMember";
         $newst_category = BASE_URL."all/".$social["e_name"]."/". $category["en_name"].  "/newest";
         echo "<url>
        <changefreq>weekly</changefreq>
        <loc>
        $maxMember_category
        </loc>
        <priority>1.0</priority>
    </url>
    <url>
        <changefreq>weekly</changefreq>
        <loc>
        $newst_category
        </loc>
        <priority>1.0</priority>
    </url>";

     }


    }


    ?>




</urlset>