<?php
header("Content-type: application/xml; charset=utf-8");
echo '<?xml version="1.0" encoding="UTF-8" ?>' . PHP_EOL;
include '../helper/DbConnection.php';
include '../helper/config.php';
$socials = array();
$social_name = basename(__FILE__, '.php');

$categories = array();
$stmt = $conn->prepare("select en_name from category where active=1");
$stmt->execute();
$result = $stmt->get_result();
while ($single_cat = $result->fetch_assoc()) {
    array_push($categories, $single_cat);
}


$categories = array();
$stmt = $conn->prepare("select en_name from category where active=1");
$stmt->execute();
$result = $stmt->get_result();
while ($single_cat = $result->fetch_assoc()) {
    array_push($categories, $single_cat);
}

$pages = array();
$stmt = $conn->prepare("select id,date(lastmod) lastmod from page where social_id = (select social_id from sociall where e_name like ?) and active > 0");
$stmt->bind_param("s", $social_name);
$stmt->execute();
$result_pages = $stmt->get_result();
while ($single_page = $result_pages->fetch_assoc()) {
    array_push($pages, $single_page);
}


$current_date = date("Y-m-d");

?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <lastmod><?php echo $current_date ?></lastmod>
        <changefreq>daily</changefreq>
        <loc>
            <?php echo BASE_URL . "m/" . $social_name ?>
        </loc>
        <priority>1.0</priority>
    </url>
    <?php
    foreach ($categories as $category) {
        $cat_url = BASE_URL . "m/" . $social_name . "/" . $category["en_name"];
        echo "<url>
        <lastmod>$current_date</lastmod>
        <changefreq>daily</changefreq>
        <loc>
          $cat_url
        </loc>
        <priority>1.0</priority>
    </url>";
    }

    foreach ($pages as $page) {
        $cat_url = BASE_URL . "social/" . $social_name . "/" . $page["id"];
        $lastmod = $page["lastmod"];
        echo "<url>
        <lastmod>$lastmod</lastmod>
        <changefreq>weekly</changefreq>
        <loc>
          $cat_url
        </loc>
        <priority>0.9</priority>
    </url>";
    }
    ?>


</urlset>