<?php
//todo footer !!!!
//todo fix make page
include "helper/init.php";

$description="بخش قوانین قبت کانال و صفحه در پوشکا - قوانین ثبت تصاویر در پوشکا ";




?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>قوانین ثبت صفحه و کانال در پوشکا</title>
    <base href=<?php echo BASE_URL ?>>
    <meta charset="utf-8">
    <title>پوشکا مرجع شبکه های اجتماعی</title>

    <meta http-equiv="content-language" content="fa">
    <meta property="og:site_name" content="پوشکا - مرجع شبکه های اجتماعی">

    <meta name="description" content="<?php echo $description?>">



    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="awsome/css/all.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/config.js"></script>


</head>

<body>

<nav class="navbar sticky-top d-flex justify-content-between navbar-light bg-white pr-1 pl-1 pr-md-2 pl-md-2">
    <a href="<?php echo BASE_URL . 'new/Instagram' ?>" id="new_page"
       class="navbar-brand alertPulse-css bg_new_page brand-background ml-1 ml-md-2 ">
        ثبت صفحه یا کانال
    </a>
    <a href="<?php echo BASE_URL ?>" class="text-bold brand-color mr-2 text-bold brand-textsize">پوشکا</a>




</nav>  <!--nav bar-->


<div class="min-height-70 d-flex flex-column">
    <h1 class="main-h1 mt-3 align-self-center"> قوانین ثبت صفحه و کانال در پوشکا</h1>

    <div class="rtl container text-justify" >

        <h2 class="myh2 text-bold">قوانین ثبت صفحه یا کانال&nbsp;</h2>
        <p>1- محتوای منتشر شده توسط کانال یا صفحه نباید مقایر با قوانین جاری جمهوری اسلامی ایران باشد&nbsp;</p>
        <p>2- انتشار هرگونه محتوایی که جزو&nbsp;<a href="https://internet.ir/crime_index.html">فهرست مصاديق&nbsp;مجرمانه</a> میباشد&nbsp;</p>
        <p>3- پوشکا مجاز به ویرایش اطلاعات وارد شده در صورت صلاح دید میباشد</p>
        <p>4- به کار بردن از عبارات یا کلمات ضد عفت واخلاق عمومی در عنوان یا توضیحات صفحه یا کانال ثبت شده ممنوع میباشد&nbsp;</p>
        <p>5- انتشار مطالبی که باعث توهین به ادیان و قومیت ها میگردد&nbsp;&nbsp;</p>
        <p>6- در صورت اثبات اینکه شما مالک صفحه یا کانال ثبت شده نمیباشید پوشکا مجاز است که مالکیت صفحه یا کانال را به صاحب اصلی آن بازگرداند&nbsp;</p>
        <p>7- انشتار اخبار کذب و ضد مصالح عمومی کشور و خلاف قوانین نشر اطلاعات توسط صفحه یا کانال ثبت شده ممنوع میباشد&nbsp;</p>
        <p>8- کانال ها یا صفحاتی که محتوای آنها در زمینه های شرط بندی ، مطالب مستحجن ، ترویج خشونت، ترویج هرگونه سوء رفتار میباشد مورد تایید پوشکا قرار نخواهند گرفت&nbsp;</p>
        <p>9- استفاده کردن از هرگونه ربات برای افزایش تعداد بازدید صفحه یا کانال&nbsp; ممنوع بوده&nbsp; و در صورت مشاهده صفحه یا کانال مذکور به حالت تعلیق در آمده یا مسدود میگردد&nbsp;</p>
        <p>10- فروش اقلام ممنوعه (<strong>تجهیزات ماهواره،عرضه غیر مجاز دارو،تجهیزات نظامی،لوازم جنسی،تجهیزات جاسوسی،اقلام مستهجن</strong>) توسط صفحات یا کانال های فروشگاهی&nbsp; ممنوع میباشد&nbsp;</p>
        <p>11-در صورت گزارش صفحه یا کانالی توسط کاربران بیش از 8 بار پوشکا مجاز به تصمیم گیری برای تعلیق یا مسدود سازی صفحه یا کانال مورد نظر میباشد&nbsp;</p>
        <p>&nbsp;</p>
        <h2 class="myh2 text-bold">قوانین ثبت تصویر کانال&nbsp;</h2>
        <ul class="pr-1">
            <li class="mt-2">1- کیفت تصاویر ثبت شده باید مطلوب باشد </li>
            <li class="mt-2">2- اندازه تصاویر باید حداقل 300*300 پیکسل باشد</li>
            <li class="mt-2">3- تصاویر ثبت شده نبايد مقاير با قوانين جمهوري اسلامي ايران باشد</li>
            <li class="mt-2">4- تصاویر نبايد حاوي محتواي توهين آميز براي اديان ، قوميت ها  و اقليت هاي مذهبي باشد</li>
            <li class="mt-2">5- تصاویر ثبت شده باید مرتبط با محتوای صفحه یا کانال باشد</li>
            <li class="mt-2">6- با توجه به اینکه بنر شما در صفحات اصلی پوشکا و بالاترین قسمت هر بخش نمایش داده میشود، به منظور حفظ کیفیت بصری سایت و اپلیکیشن تصویر حتما باید به صورت افقی بوده و از کیفیت مطلوبی برخوردار باشد پیشنهاد پوشکا برای ثبت بنر انتخاب تصویری با ارتفاع 600 پیکسل و عرض 900 پپیکسل میباشد </li>
            <li class="mt-2">7- ممکن است صفحه یا کانال مورد تایید پوشکا قرار بگیرد اما اسکرین شات های انتخاب شده تایید نشوند</li>
        </ul>


    </div>

</div>


<?php include 'footer.php' ?>


<script src="js/bootstrap.min.js"></script>



</body>
</html>